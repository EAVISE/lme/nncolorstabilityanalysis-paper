dill
# emonet port
EmoNet@git+https://gitlab.com/EAVISE/lme/emonet.git
matplotlib
nltk
pandas
Pillow>=8.3.1
seaborn
termcolor
# torch will also install numpy
torch>=1.9.0
torchvision>=0.11.0
