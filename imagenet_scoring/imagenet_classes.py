"""
Container for Imagenet target classes

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import json

from config import Config


class ImageNetClasses:
    def __init__(self, in_file: str=None):
        if in_file is None:
            in_file = Config.FILE_IMGNET_CLASSES

        with open(in_file, 'r') as fin:
            classes = json.load(fin)

        self.classes = classes

    def get_class_name(self, idx: int):
        if idx < 0 or idx > 999:
            raise ValueError(f"Provided index should be 0 <= index <= 999, got {idx} instead.")

        res = self.classes[str(idx)]
        return res[1:]

    def get_class_wordnet_id(self, idx: int):
        if idx < 0 or idx > 999:
            raise ValueError(f"Provided index should be 0 <= index <= 999, got {idx} instead.")

        res = self.classes[str(idx)]
        return res[0]


if __name__ == '__main__':
    inc = ImageNetClasses()
