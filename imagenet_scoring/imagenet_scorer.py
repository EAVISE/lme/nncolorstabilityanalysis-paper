"""

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os
from collections import Counter

from config import Config
import pandas as pd

from imagenet_scoring.imagenet_classes import ImageNetClasses


class ImageNetScorer:
    """
    Compute Top1/Top5 score on ILSVRC2012 validation set.
    """
    def __init__(self, file_val_sol=None):
        if file_val_sol is None:
            file_val_sol = Config.FILE_IMGNET_VAL_SOL
        # Load validation solution
        csv_val_sol = pd.read_csv(file_val_sol)
        val_sol = {}
        for row_idx, row in csv_val_sol.iterrows():
            k = row[0]
            parts = row[1].split()
            # When there is more than one "label" in the ref file, it is always the same label,
            # just with a different bbox, it appears.
            val_sol[k] = parts[0]

        self.val_sol = val_sol  # validation set solution
        self.in_classes = ImageNetClasses()

    def compute_score(self, model_preds: dict):
        """

        :param model_preds: {k: v}: k = image filename, v = top 5 predicted labels, as synset 'n...' ID
        :return:
        """
        raise NotImplementedError()

    def compute_top1_score_for_csv(self, csv_file: str, b_silent=False):
        """
        Compute top 1 score for CSV containing label ID predictions.

        :param csv_file:
        :param b_silent: do not print results to console
        :return: dictionary containing the top1 score per hue/saturation shift
        """
        csv = pd.read_csv(csv_file)
        score_per_col = Counter()
        for row_idx, row in csv.iterrows():
            # Filename in CSV contains extension ".JPEG", so remove.
            ref_label = self.val_sol[row[0][:-5]]
            for col_idx in range(1, csv.shape[1]):
                # Convert label index to WordNet 'n'-ID
                try:
                    wordnet_id = self.in_classes.get_class_wordnet_id(int(row[col_idx]))
                except ValueError:
                    raise ValueError(f"Error in file: {csv_file}\nrow_idx: {row_idx}\ncol_idx: {col_idx}")

                score_per_col[float(csv.columns[col_idx])] += 1 if wordnet_id == ref_label else 0
        for k, v in score_per_col.items():
            score_per_col[k] = v/csv.shape[0]

        if not b_silent:
            for k in sorted(score_per_col.keys()):
                if k % 1. == 0:
                    print(f"{int(k):>3d}: {score_per_col[k]}")
                else:
                    print(f"{k:.2f}: {score_per_col[k]}")

        return score_per_col

    def compute_top5_score_for_csv(self, csv_file: str, b_silent=False):
        """
        Compute top 5 score for CSV containing label ID predictions.

        :param csv_file:
        :param b_silent: do not print results to console
        :return: dictionary containing the top5 score per hue/saturation shift
        """
        csv = pd.read_csv(csv_file)
        score_per_col = Counter()
        for row_idx, row in csv.iterrows():
            # Filename in CSV contains extension ".JPEG", so remove.
            ref_label = self.val_sol[row[0][:-5]]
            for col_idx in range(1, csv.shape[1], 5):
                # Convert label index to WordNet 'n'-ID
                wordnet_ids = set([self.in_classes.get_class_wordnet_id(row[col_idx+k]) for k in range(5)])
                score_per_col[float(csv.columns[col_idx].split('/')[0])] += 1 if ref_label in wordnet_ids else 0
        for k, v in score_per_col.items():
            score_per_col[k] = v/csv.shape[0]

        if not b_silent:
            for k in sorted(score_per_col.keys()):
                if k % 1. == 0:
                    print(f"{int(k):>3d}: {score_per_col[k]}")
                else:
                    print(f"{k:.2f}: {score_per_col[k]}")

        return score_per_col

    def compare_corr_vs_wrong_for_csv(self, csv_file: str, b_silent=False):
        """
        Question that this method answers: for those images that were classified differently due to a change in hue,\
        how many were originally correctly predicted?\

        :param csv_file:
        :param b_silent: do not print results to console
        :return: 3 dictionaries; score_per_col, overlap_ref_corr_per_col, overlap_ref_wrong_per_col:\
         -first contains the stats per hue/saturation shift\
         -second contains % of originally correctly predicted images whose predicted label did not change
         -third contains % of originally wrongly predicted images whose predicted label did not change
        """
        csv = pd.read_csv(csv_file)
        score_per_col = Counter()  # Accuracy per column
        overlap_ref_corr_per_col = Counter()  # Are images originally correctly predicted still predicted the same?
        overlap_ref_wrong_per_col = Counter()  # Are images originally wrongly predicted still predicted the same?
        for row_idx, row in csv.iterrows():
            # Get correct label for image; filename in CSV contains extension ".JPEG", so remove.
            ref_label = self.val_sol[row[0][:-5]]
            orig_pred = None
            b_orig_corr = False  # Original prediction was correct?
            for col_idx in range(1, csv.shape[1]):
                # Convert label index to WordNet 'n'-ID
                wordnet_id = self.in_classes.get_class_wordnet_id(row[col_idx])
                score_per_col[float(csv.columns[col_idx])] += 1 if wordnet_id == ref_label else 0

                if col_idx == 1:
                    orig_pred = wordnet_id
                    b_orig_corr = (wordnet_id == ref_label)
                else:
                    if b_orig_corr:
                        overlap_ref_corr_per_col[float(csv.columns[col_idx])] += 1 if wordnet_id == orig_pred else 0
                    else:
                        overlap_ref_wrong_per_col[float(csv.columns[col_idx])] += 1 if wordnet_id == orig_pred else 0

        for k, v in score_per_col.items():
            score_per_col[k] = v/csv.shape[0]
        for k, v in overlap_ref_corr_per_col.items():
            overlap_ref_corr_per_col[k] /= csv.shape[0]*score_per_col[0]
            overlap_ref_wrong_per_col[k] /= csv.shape[0]*(1. - score_per_col[0])

        if not b_silent:
            for idx_k, k in enumerate(sorted(score_per_col.keys())):
                if k % 1. == 0:
                    print(f"{int(k):>4d}: {score_per_col[k]:.4f}", end='')
                else:
                    print(f"{k:.2f}: {score_per_col[k]:.4f}", end='')

                if idx_k == 0:
                    print()
                else:
                    print(f"\t{overlap_ref_corr_per_col[k]}\t{overlap_ref_wrong_per_col[k]}")

        return score_per_col, overlap_ref_corr_per_col, overlap_ref_wrong_per_col

if __name__ == '__main__':
    # val_csv = os.path.join(Config.DIR_LOGS, 'alexnet_val_stability_hue.csv')
    # val_csv = os.path.join(Config.DIR_LOGS, 'alexnet_classifier_tl_cc_val_stability_hue.csv')
    # val_top5_csv = os.path.join(Config.DIR_LOGS, 'alexnet_classifier_tl_cc_val_stability_hue_top5.csv')
    val_csv = os.path.join(Config.DIR_LOGS, 'alexnet_val_stability_saturation.csv')
    # val_csv = os.path.join(Config.DIR_LOGS, 'alexnet_full_tl_val_stability_hue.csv')
    # val_csv = os.path.join(Config.DIR_LOGS, 'alexnet_full_tl_hue+sat_val_stability_hue.csv')

    # val_csv = os.path.join(Config.DIR_LOGS, 'vgg16_val_stability_hue.csv')
    # val_csv = os.path.join(Config.DIR_LOGS, 'vgg16_full_tl_cc_val_stability_hue.csv')
    # val_top5_csv = os.path.join(Config.DIR_LOGS, 'vgg16_full_tl_cc_val_stability_hue_top5.csv')
    # val_csv = os.path.join(Config.DIR_LOGS, 'vgg16_val_stability_saturation.csv')

    # val_csv = os.path.join(Config.DIR_LOGS, 'vgg19_val_stability_saturation.csv')

    # val_csv = os.path.join(Config.DIR_LOGS, 'resnet18_val_stability_hue.csv')
    # val_csv = os.path.join(Config.DIR_LOGS, 'resnet18_full_hue_tl_val_stability_hue.csv')
    # val_top5_csv = os.path.join(Config.DIR_LOGS, 'resnet18_full_hue_tl_val_stability_hue_top5.csv')

    print(f"Using file {val_csv}")
    in_scorer = ImageNetScorer()
    # in_scorer.compute_top1_score_for_csv(csv_file=val_csv)
    # in_scorer.compute_top5_score_for_csv(csv_file=val_top5_csv)
    in_scorer.compare_corr_vs_wrong_for_csv(csv_file=val_csv)
