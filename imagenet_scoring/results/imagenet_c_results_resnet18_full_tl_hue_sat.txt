/home/lmertens/Work/Projects/NeuroANN/Code/NNColorStabilityAnalysis/env/bin/python /home/lmertens/Work/Projects/NeuroANN/Code/NNColorStabilityAnalysis/imagenet_scoring/imagenet_c_test.py
Namespace(model_name='resnet18_hue_sat', ngpu=1)
Model Loaded

Using ImageNet data
Distortion gaussian_noise, severity 1
At batch 195/195...
Error: 0.5269800424575806
Distortion gaussian_noise, severity 2
At batch 195/195...
Error: 0.6540800333023071
Distortion gaussian_noise, severity 3
At batch 195/195...
Error: 0.8101999759674072
Distortion gaussian_noise, severity 4
At batch 195/195...
Error: 0.9269599914550781
Distortion gaussian_noise, severity 5
At batch 195/195...
Error: 0.9844599962234497
Average: 0.7805359959602356

Distortion: gaussian_noise   | CE (unnormalized) (%): 78.05

Distortion shot_noise, severity 1
At batch 195/195...
Error: 0.5508600473403931
Distortion shot_noise, severity 2
At batch 195/195...
Error: 0.6878000497817993
Distortion shot_noise, severity 3
At batch 195/195...
Error: 0.8194999694824219
Distortion shot_noise, severity 4
At batch 195/195...
Error: 0.9383599758148193
Distortion shot_noise, severity 5
At batch 195/195...
Error: 0.9758599996566772
Average: 0.7944760322570801

Distortion: shot_noise       | CE (unnormalized) (%): 79.45

Distortion impulse_noise, severity 1
At batch 195/195...
Error: 0.6853799819946289
Distortion impulse_noise, severity 2
At batch 195/195...
Error: 0.7772600054740906
Distortion impulse_noise, severity 3
At batch 195/195...
Error: 0.8429000377655029
Distortion impulse_noise, severity 4
At batch 195/195...
Error: 0.9411200284957886
Distortion impulse_noise, severity 5
At batch 195/195...
Error: 0.9856399893760681
Average: 0.8464600443840027

Distortion: impulse_noise    | CE (unnormalized) (%): 84.65

Distortion defocus_blur, severity 1
At batch 195/195...
Error: 0.48787999153137207
Distortion defocus_blur, severity 2
At batch 195/195...
Error: 0.5727599859237671
Distortion defocus_blur, severity 3
At batch 195/195...
Error: 0.7366200089454651
Distortion defocus_blur, severity 4
At batch 195/195...
Error: 0.8510800004005432
Distortion defocus_blur, severity 5
At batch 195/195...
Error: 0.9176599979400635
Average: 0.7131999731063843

Distortion: defocus_blur     | CE (unnormalized) (%): 71.32

Distortion glass_blur, severity 1
At batch 195/195...
Error: 0.540340006351471
Distortion glass_blur, severity 2
At batch 195/195...
Error: 0.668940007686615
Distortion glass_blur, severity 3
At batch 195/195...
Error: 0.8762199878692627
Distortion glass_blur, severity 4
At batch 195/195...
Error: 0.9099400043487549
Distortion glass_blur, severity 5
At batch 195/195...
Error: 0.9380800127983093
Average: 0.7867040038108826

Distortion: glass_blur       | CE (unnormalized) (%): 78.67

Distortion motion_blur, severity 1
At batch 195/195...
Error: 0.435200035572052
Distortion motion_blur, severity 2
At batch 195/195...
Error: 0.5636199712753296
Distortion motion_blur, severity 3
At batch 195/195...
Error: 0.7437199950218201
Distortion motion_blur, severity 4
At batch 195/195...
Error: 0.8685399889945984
Distortion motion_blur, severity 5
At batch 195/195...
Error: 0.9155399799346924
Average: 0.7053239941596985

Distortion: motion_blur      | CE (unnormalized) (%): 70.53

Distortion zoom_blur, severity 1
At batch 195/195...
Error: 0.5856000185012817
Distortion zoom_blur, severity 2
At batch 195/195...
Error: 0.6906800270080566
Distortion zoom_blur, severity 3
At batch 195/195...
Error: 0.752780020236969
Distortion zoom_blur, severity 4
At batch 195/195...
Error: 0.8094000220298767
Distortion zoom_blur, severity 5
At batch 195/195...
Error: 0.8524600267410278
Average: 0.7381840944290161

Distortion: zoom_blur        | CE (unnormalized) (%): 73.82

Distortion snow, severity 1
At batch 195/195...
Error: 0.5354800224304199
Distortion snow, severity 2
At batch 195/195...
Error: 0.7581999897956848
Distortion snow, severity 3
At batch 195/195...
Error: 0.7172999978065491
Distortion snow, severity 4
At batch 195/195...
Error: 0.8174999952316284
Distortion snow, severity 5
At batch 195/195...
Error: 0.8746600151062012
Average: 0.7406279444694519

Distortion: snow             | CE (unnormalized) (%): 74.06

Distortion frost, severity 1
At batch 195/195...
Error: 0.4754199981689453
Distortion frost, severity 2
At batch 195/195...
Error: 0.653659999370575
Distortion frost, severity 3
At batch 195/195...
Error: 0.7613599896430969
Distortion frost, severity 4
At batch 195/195...
Error: 0.777899980545044
Distortion frost, severity 5
At batch 195/195...
Error: 0.8337000012397766
Average: 0.7004079818725586

Distortion: frost            | CE (unnormalized) (%): 70.04

Distortion fog, severity 1
At batch 195/195...
Error: 0.4591200351715088
Distortion fog, severity 2
At batch 195/195...
Error: 0.526960015296936
Distortion fog, severity 3
At batch 195/195...
Error: 0.6117199659347534
Distortion fog, severity 4
At batch 195/195...
Error: 0.655019998550415
Distortion fog, severity 5
At batch 195/195...
Error: 0.7870200276374817
Average: 0.6079679727554321

Distortion: fog              | CE (unnormalized) (%): 60.80

Distortion brightness, severity 1
At batch 195/195...
Error: 0.32528001070022583
Distortion brightness, severity 2
At batch 195/195...
Error: 0.34310001134872437
Distortion brightness, severity 3
At batch 195/195...
Error: 0.3698199987411499
Distortion brightness, severity 4
At batch 195/195...
Error: 0.414900004863739
Distortion brightness, severity 5
At batch 195/195...
Error: 0.4744200110435486
Average: 0.38550400733947754

Distortion: brightness       | CE (unnormalized) (%): 38.55

Distortion contrast, severity 1
At batch 195/195...
Error: 0.41940003633499146
Distortion contrast, severity 2
At batch 195/195...
Error: 0.488319993019104
Distortion contrast, severity 3
At batch 195/195...
Error: 0.6168799996376038
Distortion contrast, severity 4
At batch 195/195...
Error: 0.8543599843978882
Distortion contrast, severity 5
At batch 195/195...
Error: 0.9711599946022034
Average: 0.670024037361145

Distortion: contrast         | CE (unnormalized) (%): 67.00

Distortion elastic_transform, severity 1
At batch 195/195...
Error: 0.40248000621795654
Distortion elastic_transform, severity 2
At batch 195/195...
Error: 0.6327999830245972
Distortion elastic_transform, severity 3
At batch 195/195...
Error: 0.5073000192642212
Distortion elastic_transform, severity 4
At batch 195/195...
Error: 0.6468200087547302
Distortion elastic_transform, severity 5
At batch 195/195...
Error: 0.8687599897384644
Average: 0.6116319894790649

Distortion: elastic_transform  | CE (unnormalized) (%): 61.16

Distortion pixelate, severity 1
At batch 195/195...
Error: 0.4312800168991089
Distortion pixelate, severity 2
At batch 195/195...
Error: 0.42809998989105225
Distortion pixelate, severity 3
At batch 195/195...
Error: 0.6762599945068359
Distortion pixelate, severity 4
At batch 195/195...
Error: 0.8263000249862671
Distortion pixelate, severity 5
At batch 195/195...
Error: 0.8539999723434448
Average: 0.6431879997253418

Distortion: pixelate         | CE (unnormalized) (%): 64.32

Distortion jpeg_compression, severity 1
At batch 195/195...
Error: 0.42834001779556274
Distortion jpeg_compression, severity 2
At batch 195/195...
Error: 0.4709799885749817
Distortion jpeg_compression, severity 3
At batch 195/195...
Error: 0.5065599679946899
Distortion jpeg_compression, severity 4
At batch 195/195...
Error: 0.6221799850463867
Distortion jpeg_compression, severity 5
At batch 195/195...
Error: 0.770300030708313
Average: 0.5596719980239868

Distortion: jpeg_compression  | CE (unnormalized) (%): 55.97

Distortion speckle_noise, severity 1
At batch 195/195...
Error: 0.5109000205993652
Distortion speckle_noise, severity 2
At batch 195/195...
Error: 0.5886800289154053
Distortion speckle_noise, severity 3
At batch 195/195...
Error: 0.7686799764633179
Distortion speckle_noise, severity 4
At batch 195/195...
Error: 0.8450400233268738
Distortion speckle_noise, severity 5
At batch 195/195...
Error: 0.9088600277900696
Average: 0.7244319915771484

Distortion: speckle_noise    | CE (unnormalized) (%): 72.44

Distortion gaussian_blur, severity 1
At batch 195/195...
Error: 0.39197999238967896
Distortion gaussian_blur, severity 2
At batch 195/195...
Error: 0.5293999910354614
Distortion gaussian_blur, severity 3
At batch 195/195...
Error: 0.6850399971008301
Distortion gaussian_blur, severity 4
At batch 195/195...
Error: 0.8121799826622009
Distortion gaussian_blur, severity 5
At batch 195/195...
Error: 0.9414399862289429
Average: 0.6720080375671387

Distortion: gaussian_blur    | CE (unnormalized) (%): 67.20

Distortion spatter, severity 1
At batch 195/195...
Error: 0.3459799885749817
Distortion spatter, severity 2
At batch 195/195...
Error: 0.4863399863243103
Distortion spatter, severity 3
At batch 195/195...
Error: 0.6095800399780273
Distortion spatter, severity 4
At batch 195/195...
Error: 0.7161400318145752
Distortion spatter, severity 5
At batch 195/195...
Error: 0.821340024471283
Average: 0.5958760380744934

Distortion: spatter          | CE (unnormalized) (%): 59.59

Distortion saturate, severity 1
At batch 195/195...
Error: 0.33737999200820923
Distortion saturate, severity 2
At batch 195/195...
Error: 0.365339994430542
Distortion saturate, severity 3
At batch 195/195...
Error: 0.33145999908447266
Distortion saturate, severity 4
At batch 195/195...
Error: 0.4045799970626831
Distortion saturate, severity 5
At batch 195/195...
Error: 0.5180200338363647
Average: 0.3913559913635254

Distortion: saturate         | CE (unnormalized) (%): 39.14

mCE (unnormalized by AlexNet errors) (%): 66.67

Process finished with exit code 0
