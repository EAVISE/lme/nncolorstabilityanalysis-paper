/home/lmertens/Work/Projects/NeuroANN/Code/NNColorStabilityAnalysis/env/bin/python /home/lmertens/Work/Projects/NeuroANN/Code/NNColorStabilityAnalysis/imagenet_scoring/imagenet_c_test.py
Namespace(model_name='resnet18_hue_sat_blur', ngpu=1)
Model Loaded

Using ImageNet data
Distortion gaussian_noise, severity 1
At batch 195/195...
Error: 0.5370000004768372
Distortion gaussian_noise, severity 2
At batch 195/195...
Error: 0.6723799705505371
Distortion gaussian_noise, severity 3
At batch 195/195...
Error: 0.8402600288391113
Distortion gaussian_noise, severity 4
At batch 195/195...
Error: 0.9553999900817871
Distortion gaussian_noise, severity 5
At batch 195/195...
Error: 0.9933800101280212
Average: 0.7996839880943298

Distortion: gaussian_noise   | CE (unnormalized) (%): 79.97

Distortion shot_noise, severity 1
At batch 195/195...
Error: 0.5600800514221191
Distortion shot_noise, severity 2
At batch 195/195...
Error: 0.7049199938774109
Distortion shot_noise, severity 3
At batch 195/195...
Error: 0.8476799726486206
Distortion shot_noise, severity 4
At batch 195/195...
Error: 0.9646999835968018
Distortion shot_noise, severity 5
At batch 195/195...
Error: 0.9869199991226196
Average: 0.8128600120544434

Distortion: shot_noise       | CE (unnormalized) (%): 81.29

Distortion impulse_noise, severity 1
At batch 195/195...
Error: 0.7060400247573853
Distortion impulse_noise, severity 2
At batch 195/195...
Error: 0.8070800304412842
Distortion impulse_noise, severity 3
At batch 195/195...
Error: 0.881380021572113
Distortion impulse_noise, severity 4
At batch 195/195...
Error: 0.9708999991416931
Distortion impulse_noise, severity 5
At batch 195/195...
Error: 0.9948599934577942
Average: 0.872052013874054

Distortion: impulse_noise    | CE (unnormalized) (%): 87.21

Distortion defocus_blur, severity 1
At batch 195/195...
Error: 0.3886600136756897
Distortion defocus_blur, severity 2
At batch 195/195...
Error: 0.4324800372123718
Distortion defocus_blur, severity 3
At batch 195/195...
Error: 0.6017600297927856
Distortion defocus_blur, severity 4
At batch 195/195...
Error: 0.7801799774169922
Distortion defocus_blur, severity 5
At batch 195/195...
Error: 0.8932200074195862
Average: 0.6192600131034851

Distortion: defocus_blur     | CE (unnormalized) (%): 61.93

Distortion glass_blur, severity 1
At batch 195/195...
Error: 0.5142999887466431
Distortion glass_blur, severity 2
At batch 195/195...
Error: 0.649940013885498
Distortion glass_blur, severity 3
At batch 195/195...
Error: 0.8748199939727783
Distortion glass_blur, severity 4
At batch 195/195...
Error: 0.9120200276374817
Distortion glass_blur, severity 5
At batch 195/195...
Error: 0.949180006980896
Average: 0.7800520062446594

Distortion: glass_blur       | CE (unnormalized) (%): 78.01

Distortion motion_blur, severity 1
At batch 195/195...
Error: 0.410800039768219
Distortion motion_blur, severity 2
At batch 195/195...
Error: 0.5420399904251099
Distortion motion_blur, severity 3
At batch 195/195...
Error: 0.7421799898147583
Distortion motion_blur, severity 4
At batch 195/195...
Error: 0.8780999779701233
Distortion motion_blur, severity 5
At batch 195/195...
Error: 0.923799991607666
Average: 0.6993839740753174

Distortion: motion_blur      | CE (unnormalized) (%): 69.94

Distortion zoom_blur, severity 1
At batch 195/195...
Error: 0.5794800519943237
Distortion zoom_blur, severity 2
At batch 195/195...
Error: 0.6906800270080566
Distortion zoom_blur, severity 3
At batch 195/195...
Error: 0.751960039138794
Distortion zoom_blur, severity 4
At batch 195/195...
Error: 0.8130999803543091
Distortion zoom_blur, severity 5
At batch 195/195...
Error: 0.8565999865531921
Average: 0.7383639812469482

Distortion: zoom_blur        | CE (unnormalized) (%): 73.84

Distortion snow, severity 1
At batch 195/195...
Error: 0.5625600218772888
Distortion snow, severity 2
At batch 195/195...
Error: 0.7897400259971619
Distortion snow, severity 3
At batch 195/195...
Error: 0.7451200485229492
Distortion snow, severity 4
At batch 195/195...
Error: 0.8428800106048584
Distortion snow, severity 5
At batch 195/195...
Error: 0.892300009727478
Average: 0.7665200233459473

Distortion: snow             | CE (unnormalized) (%): 76.65

Distortion frost, severity 1
At batch 195/195...
Error: 0.49187999963760376
Distortion frost, severity 2
At batch 195/195...
Error: 0.6662200093269348
Distortion frost, severity 3
At batch 195/195...
Error: 0.7726399898529053
Distortion frost, severity 4
At batch 195/195...
Error: 0.7879400253295898
Distortion frost, severity 5
At batch 195/195...
Error: 0.8425999879837036
Average: 0.7122559547424316

Distortion: frost            | CE (unnormalized) (%): 71.23

Distortion fog, severity 1
At batch 195/195...
Error: 0.4799399971961975
Distortion fog, severity 2
At batch 195/195...
Error: 0.543660044670105
Distortion fog, severity 3
At batch 195/195...
Error: 0.6195600032806396
Distortion fog, severity 4
At batch 195/195...
Error: 0.6591399908065796
Distortion fog, severity 5
At batch 195/195...
Error: 0.7861199975013733
Average: 0.617684006690979

Distortion: fog              | CE (unnormalized) (%): 61.77

Distortion brightness, severity 1
At batch 195/195...
Error: 0.34328001737594604
Distortion brightness, severity 2
At batch 195/195...
Error: 0.36309999227523804
Distortion brightness, severity 3
At batch 195/195...
Error: 0.3909200429916382
Distortion brightness, severity 4
At batch 195/195...
Error: 0.43588000535964966
Distortion brightness, severity 5
At batch 195/195...
Error: 0.49814003705978394
Average: 0.4062640070915222

Distortion: brightness       | CE (unnormalized) (%): 40.63

Distortion contrast, severity 1
At batch 195/195...
Error: 0.4547399878501892
Distortion contrast, severity 2
At batch 195/195...
Error: 0.533840000629425
Distortion contrast, severity 3
At batch 195/195...
Error: 0.6717399954795837
Distortion contrast, severity 4
At batch 195/195...
Error: 0.8809199929237366
Distortion contrast, severity 5
At batch 195/195...
Error: 0.9721199870109558
Average: 0.702672004699707

Distortion: contrast         | CE (unnormalized) (%): 70.27

Distortion elastic_transform, severity 1
At batch 195/195...
Error: 0.38982003927230835
Distortion elastic_transform, severity 2
At batch 195/195...
Error: 0.6205800175666809
Distortion elastic_transform, severity 3
At batch 195/195...
Error: 0.49893999099731445
Distortion elastic_transform, severity 4
At batch 195/195...
Error: 0.634880006313324
Distortion elastic_transform, severity 5
At batch 195/195...
Error: 0.8642600178718567
Average: 0.6016960144042969

Distortion: elastic_transform  | CE (unnormalized) (%): 60.17

Distortion pixelate, severity 1
At batch 195/195...
Error: 0.4432600140571594
Distortion pixelate, severity 2
At batch 195/195...
Error: 0.45249998569488525
Distortion pixelate, severity 3
At batch 195/195...
Error: 0.6505199670791626
Distortion pixelate, severity 4
At batch 195/195...
Error: 0.791159987449646
Distortion pixelate, severity 5
At batch 195/195...
Error: 0.8075399994850159
Average: 0.6289960145950317

Distortion: pixelate         | CE (unnormalized) (%): 62.90

Distortion jpeg_compression, severity 1
At batch 195/195...
Error: 0.4832000136375427
Distortion jpeg_compression, severity 2
At batch 195/195...
Error: 0.538040041923523
Distortion jpeg_compression, severity 3
At batch 195/195...
Error: 0.5751800537109375
Distortion jpeg_compression, severity 4
At batch 195/195...
Error: 0.682919979095459
Distortion jpeg_compression, severity 5
At batch 195/195...
Error: 0.7921199798583984
Average: 0.6142920255661011

Distortion: jpeg_compression  | CE (unnormalized) (%): 61.43

Distortion speckle_noise, severity 1
At batch 195/195...
Error: 0.5159400105476379
Distortion speckle_noise, severity 2
At batch 195/195...
Error: 0.5938400030136108
Distortion speckle_noise, severity 3
At batch 195/195...
Error: 0.785099983215332
Distortion speckle_noise, severity 4
At batch 195/195...
Error: 0.8694000244140625
Distortion speckle_noise, severity 5
At batch 195/195...
Error: 0.934719979763031
Average: 0.7398000359535217

Distortion: speckle_noise    | CE (unnormalized) (%): 73.98

Distortion gaussian_blur, severity 1
At batch 195/195...
Error: 0.3436400294303894
Distortion gaussian_blur, severity 2
At batch 195/195...
Error: 0.3922399878501892
Distortion gaussian_blur, severity 3
At batch 195/195...
Error: 0.48260003328323364
Distortion gaussian_blur, severity 4
At batch 195/195...
Error: 0.6331200003623962
Distortion gaussian_blur, severity 5
At batch 195/195...
Error: 0.9167600274085999
Average: 0.5536720156669617

Distortion: gaussian_blur    | CE (unnormalized) (%): 55.37

Distortion spatter, severity 1
At batch 195/195...
Error: 0.3611600399017334
Distortion spatter, severity 2
At batch 195/195...
Error: 0.5044599771499634
Distortion spatter, severity 3
At batch 195/195...
Error: 0.633080005645752
Distortion spatter, severity 4
At batch 195/195...
Error: 0.7522599697113037
Distortion spatter, severity 5
At batch 195/195...
Error: 0.8427599668502808
Average: 0.6187440156936646

Distortion: spatter          | CE (unnormalized) (%): 61.87

Distortion saturate, severity 1
At batch 195/195...
Error: 0.3662000298500061
Distortion saturate, severity 2
At batch 195/195...
Error: 0.4009400010108948
Distortion saturate, severity 3
At batch 195/195...
Error: 0.34902000427246094
Distortion saturate, severity 4
At batch 195/195...
Error: 0.42368000745773315
Distortion saturate, severity 5
At batch 195/195...
Error: 0.5297200083732605
Average: 0.41391199827194214

Distortion: saturate         | CE (unnormalized) (%): 41.39

mCE (unnormalized by AlexNet errors) (%): 66.83

Process finished with exit code 0
