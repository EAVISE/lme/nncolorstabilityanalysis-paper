/home/lmertens/Work/Projects/NeuroANN/Code/NNColorStabilityAnalysis/env/bin/python /home/lmertens/Work/Projects/NeuroANN/Code/NNColorStabilityAnalysis/imagenet_scoring/imagenet_c_test.py
Namespace(model_name='resnet18_hue', ngpu=1)
Model Loaded

Using ImageNet data
Distortion gaussian_noise, severity 1
At batch 195/195...
Error: 0.5201799869537354
Distortion gaussian_noise, severity 2
At batch 195/195...
Error: 0.6437599658966064
Distortion gaussian_noise, severity 3
At batch 195/195...
Error: 0.8085399866104126
Distortion gaussian_noise, severity 4
At batch 195/195...
Error: 0.9335799813270569
Distortion gaussian_noise, severity 5
At batch 195/195...
Error: 0.9865800142288208
Average: 0.7785280346870422

Distortion: gaussian_noise   | CE (unnormalized) (%): 77.85

Distortion shot_noise, severity 1
At batch 195/195...
Error: 0.5454200506210327
Distortion shot_noise, severity 2
At batch 195/195...
Error: 0.6829800009727478
Distortion shot_noise, severity 3
At batch 195/195...
Error: 0.8234599828720093
Distortion shot_noise, severity 4
At batch 195/195...
Error: 0.949679970741272
Distortion shot_noise, severity 5
At batch 195/195...
Error: 0.9810400009155273
Average: 0.796515941619873

Distortion: shot_noise       | CE (unnormalized) (%): 79.65

Distortion impulse_noise, severity 1
At batch 195/195...
Error: 0.6857399940490723
Distortion impulse_noise, severity 2
At batch 195/195...
Error: 0.7778400182723999
Distortion impulse_noise, severity 3
At batch 195/195...
Error: 0.8460999727249146
Distortion impulse_noise, severity 4
At batch 195/195...
Error: 0.9487400054931641
Distortion impulse_noise, severity 5
At batch 195/195...
Error: 0.9886199831962585
Average: 0.849407970905304

Distortion: impulse_noise    | CE (unnormalized) (%): 84.94

Distortion defocus_blur, severity 1
At batch 195/195...
Error: 0.4884200096130371
Distortion defocus_blur, severity 2
At batch 195/195...
Error: 0.5731199979782104
Distortion defocus_blur, severity 3
At batch 195/195...
Error: 0.7366600036621094
Distortion defocus_blur, severity 4
At batch 195/195...
Error: 0.8485000133514404
Distortion defocus_blur, severity 5
At batch 195/195...
Error: 0.9144799709320068
Average: 0.7122359871864319

Distortion: defocus_blur     | CE (unnormalized) (%): 71.22

Distortion glass_blur, severity 1
At batch 195/195...
Error: 0.5424200296401978
Distortion glass_blur, severity 2
At batch 195/195...
Error: 0.6708600521087646
Distortion glass_blur, severity 3
At batch 195/195...
Error: 0.8739200234413147
Distortion glass_blur, severity 4
At batch 195/195...
Error: 0.9081599712371826
Distortion glass_blur, severity 5
At batch 195/195...
Error: 0.9348999857902527
Average: 0.7860520482063293

Distortion: glass_blur       | CE (unnormalized) (%): 78.61

Distortion motion_blur, severity 1
At batch 195/195...
Error: 0.43474000692367554
Distortion motion_blur, severity 2
At batch 195/195...
Error: 0.5645800232887268
Distortion motion_blur, severity 3
At batch 195/195...
Error: 0.7418799996376038
Distortion motion_blur, severity 4
At batch 195/195...
Error: 0.8632199764251709
Distortion motion_blur, severity 5
At batch 195/195...
Error: 0.908079981803894
Average: 0.7024999856948853

Distortion: motion_blur      | CE (unnormalized) (%): 70.25

Distortion zoom_blur, severity 1
At batch 195/195...
Error: 0.5840200185775757
Distortion zoom_blur, severity 2
At batch 195/195...
Error: 0.6863600015640259
Distortion zoom_blur, severity 3
At batch 195/195...
Error: 0.7487000226974487
Distortion zoom_blur, severity 4
At batch 195/195...
Error: 0.8037400245666504
Distortion zoom_blur, severity 5
At batch 195/195...
Error: 0.8468599915504456
Average: 0.7339360117912292

Distortion: zoom_blur        | CE (unnormalized) (%): 73.39

Distortion snow, severity 1
At batch 195/195...
Error: 0.5486600399017334
Distortion snow, severity 2
At batch 195/195...
Error: 0.7803199887275696
Distortion snow, severity 3
At batch 195/195...
Error: 0.7457799911499023
Distortion snow, severity 4
At batch 195/195...
Error: 0.8440999984741211
Distortion snow, severity 5
At batch 195/195...
Error: 0.8985400199890137
Average: 0.763480007648468

Distortion: snow             | CE (unnormalized) (%): 76.35

Distortion frost, severity 1
At batch 195/195...
Error: 0.47732001543045044
Distortion frost, severity 2
At batch 195/195...
Error: 0.6569000482559204
Distortion frost, severity 3
At batch 195/195...
Error: 0.7672799825668335
Distortion frost, severity 4
At batch 195/195...
Error: 0.7866600155830383
Distortion frost, severity 5
At batch 195/195...
Error: 0.8416600227355957
Average: 0.7059640288352966

Distortion: frost            | CE (unnormalized) (%): 70.60

Distortion fog, severity 1
At batch 195/195...
Error: 0.47798001766204834
Distortion fog, severity 2
At batch 195/195...
Error: 0.5505599975585938
Distortion fog, severity 3
At batch 195/195...
Error: 0.644320011138916
Distortion fog, severity 4
At batch 195/195...
Error: 0.6980400085449219
Distortion fog, severity 5
At batch 195/195...
Error: 0.8315200209617615
Average: 0.6404840350151062

Distortion: fog              | CE (unnormalized) (%): 64.05

Distortion brightness, severity 1
At batch 195/195...
Error: 0.32462000846862793
Distortion brightness, severity 2
At batch 195/195...
Error: 0.34654003381729126
Distortion brightness, severity 3
At batch 195/195...
Error: 0.3801000118255615
Distortion brightness, severity 4
At batch 195/195...
Error: 0.43296003341674805
Distortion brightness, severity 5
At batch 195/195...
Error: 0.5004600286483765
Average: 0.39693599939346313

Distortion: brightness       | CE (unnormalized) (%): 39.69

Distortion contrast, severity 1
At batch 195/195...
Error: 0.42778003215789795
Distortion contrast, severity 2
At batch 195/195...
Error: 0.5000799894332886
Distortion contrast, severity 3
At batch 195/195...
Error: 0.6358599662780762
Distortion contrast, severity 4
At batch 195/195...
Error: 0.8676000237464905
Distortion contrast, severity 5
At batch 195/195...
Error: 0.9743800163269043
Average: 0.6811400055885315

Distortion: contrast         | CE (unnormalized) (%): 68.11

Distortion elastic_transform, severity 1
At batch 195/195...
Error: 0.40296000242233276
Distortion elastic_transform, severity 2
At batch 195/195...
Error: 0.6331599950790405
Distortion elastic_transform, severity 3
At batch 195/195...
Error: 0.505079984664917
Distortion elastic_transform, severity 4
At batch 195/195...
Error: 0.641700029373169
Distortion elastic_transform, severity 5
At batch 195/195...
Error: 0.8659600019454956
Average: 0.6097720265388489

Distortion: elastic_transform  | CE (unnormalized) (%): 60.98

Distortion pixelate, severity 1
At batch 195/195...
Error: 0.4267600178718567
Distortion pixelate, severity 2
At batch 195/195...
Error: 0.42625999450683594
Distortion pixelate, severity 3
At batch 195/195...
Error: 0.6587600111961365
Distortion pixelate, severity 4
At batch 195/195...
Error: 0.8158999681472778
Distortion pixelate, severity 5
At batch 195/195...
Error: 0.8471800088882446
Average: 0.6349719762802124

Distortion: pixelate         | CE (unnormalized) (%): 63.50

Distortion jpeg_compression, severity 1
At batch 195/195...
Error: 0.4239400029182434
Distortion jpeg_compression, severity 2
At batch 195/195...
Error: 0.4652000069618225
Distortion jpeg_compression, severity 3
At batch 195/195...
Error: 0.4973599910736084
Distortion jpeg_compression, severity 4
At batch 195/195...
Error: 0.6132799983024597
Distortion jpeg_compression, severity 5
At batch 195/195...
Error: 0.767520010471344
Average: 0.5534600019454956

Distortion: jpeg_compression  | CE (unnormalized) (%): 55.35

Distortion speckle_noise, severity 1
At batch 195/195...
Error: 0.5093400478363037
Distortion speckle_noise, severity 2
At batch 195/195...
Error: 0.5857599973678589
Distortion speckle_noise, severity 3
At batch 195/195...
Error: 0.7714599967002869
Distortion speckle_noise, severity 4
At batch 195/195...
Error: 0.8531399965286255
Distortion speckle_noise, severity 5
At batch 195/195...
Error: 0.9200599789619446
Average: 0.7279519438743591

Distortion: speckle_noise    | CE (unnormalized) (%): 72.80

Distortion gaussian_blur, severity 1
At batch 195/195...
Error: 0.3899199962615967
Distortion gaussian_blur, severity 2
At batch 195/195...
Error: 0.5305200219154358
Distortion gaussian_blur, severity 3
At batch 195/195...
Error: 0.6855599880218506
Distortion gaussian_blur, severity 4
At batch 195/195...
Error: 0.8102200031280518
Distortion gaussian_blur, severity 5
At batch 195/195...
Error: 0.936959981918335
Average: 0.670635998249054

Distortion: gaussian_blur    | CE (unnormalized) (%): 67.06

Distortion spatter, severity 1
At batch 195/195...
Error: 0.34700000286102295
Distortion spatter, severity 2
At batch 195/195...
Error: 0.47633999586105347
Distortion spatter, severity 3
At batch 195/195...
Error: 0.5888000130653381
Distortion spatter, severity 4
At batch 195/195...
Error: 0.7091000080108643
Distortion spatter, severity 5
At batch 195/195...
Error: 0.8119400143623352
Average: 0.5866360068321228

Distortion: spatter          | CE (unnormalized) (%): 58.66

Distortion saturate, severity 1
At batch 195/195...
Error: 0.4025599956512451
Distortion saturate, severity 2
At batch 195/195...
Error: 0.43518000841140747
Distortion saturate, severity 3
At batch 195/195...
Error: 0.348300039768219
Distortion saturate, severity 4
At batch 195/195...
Error: 0.4619799852371216
Distortion saturate, severity 5
At batch 195/195...
Error: 0.5856200456619263
Average: 0.44672805070877075

Distortion: saturate         | CE (unnormalized) (%): 44.67

mCE (unnormalized by AlexNet errors) (%): 67.25

Process finished with exit code 0
