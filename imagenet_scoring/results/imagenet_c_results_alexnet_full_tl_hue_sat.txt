/home/lmertens/Work/Projects/NeuroANN/Code/NNColorStabilityAnalysis/env/bin/python /home/lmertens/Work/Projects/NeuroANN/Code/NNColorStabilityAnalysis/imagenet_scoring/imagenet_c_test.py 
Namespace(model_name='alexnet_hue_sat', ngpu=1)
Model Loaded

Using ImageNet data
Distortion gaussian_noise, severity 1
At batch 195/195...
Error: 0.7257200479507446
Distortion gaussian_noise, severity 2
At batch 195/195...
Error: 0.844480037689209
Distortion gaussian_noise, severity 3
At batch 195/195...
Error: 0.9378399848937988
Distortion gaussian_noise, severity 4
At batch 195/195...
Error: 0.9803199768066406
Distortion gaussian_noise, severity 5
At batch 195/195...
Error: 0.994979977607727
Average: 0.8966679573059082

Distortion: gaussian_noise   | CE (unnormalized) (%): 89.67

Distortion shot_noise, severity 1
At batch 195/195...
Error: 0.7387000322341919
Distortion shot_noise, severity 2
At batch 195/195...
Error: 0.8605999946594238
Distortion shot_noise, severity 3
At batch 195/195...
Error: 0.9365600347518921
Distortion shot_noise, severity 4
At batch 195/195...
Error: 0.9818199872970581
Distortion shot_noise, severity 5
At batch 195/195...
Error: 0.9915000200271606
Average: 0.9018360376358032

Distortion: shot_noise       | CE (unnormalized) (%): 90.18

Distortion impulse_noise, severity 1
At batch 195/195...
Error: 0.8073800206184387
Distortion impulse_noise, severity 2
At batch 195/195...
Error: 0.9070199728012085
Distortion impulse_noise, severity 3
At batch 195/195...
Error: 0.9499800205230713
Distortion impulse_noise, severity 4
At batch 195/195...
Error: 0.9864799976348877
Distortion impulse_noise, severity 5
At batch 195/195...
Error: 0.9955599904060364
Average: 0.9292839765548706

Distortion: impulse_noise    | CE (unnormalized) (%): 92.93

Distortion defocus_blur, severity 1
At batch 195/195...
Error: 0.689520001411438
Distortion defocus_blur, severity 2
At batch 195/195...
Error: 0.7719799876213074
Distortion defocus_blur, severity 3
At batch 195/195...
Error: 0.8902599811553955
Distortion defocus_blur, severity 4
At batch 195/195...
Error: 0.947920024394989
Distortion defocus_blur, severity 5
At batch 195/195...
Error: 0.9726999998092651
Average: 0.8544759750366211

Distortion: defocus_blur     | CE (unnormalized) (%): 85.45

Distortion glass_blur, severity 1
At batch 195/195...
Error: 0.6774200201034546
Distortion glass_blur, severity 2
At batch 195/195...
Error: 0.789080023765564
Distortion glass_blur, severity 3
At batch 195/195...
Error: 0.9215599894523621
Distortion glass_blur, severity 4
At batch 195/195...
Error: 0.9457399845123291
Distortion glass_blur, severity 5
At batch 195/195...
Error: 0.9595199823379517
Average: 0.8586640357971191

Distortion: glass_blur       | CE (unnormalized) (%): 85.87

Distortion motion_blur, severity 1
At batch 195/195...
Error: 0.6113200187683105
Distortion motion_blur, severity 2
At batch 195/195...
Error: 0.7373600006103516
Distortion motion_blur, severity 3
At batch 195/195...
Error: 0.858680009841919
Distortion motion_blur, severity 4
At batch 195/195...
Error: 0.9279599785804749
Distortion motion_blur, severity 5
At batch 195/195...
Error: 0.9515200257301331
Average: 0.8173680305480957

Distortion: motion_blur      | CE (unnormalized) (%): 81.74

Distortion zoom_blur, severity 1
At batch 195/195...
Error: 0.7307800054550171
Distortion zoom_blur, severity 2
At batch 195/195...
Error: 0.8019599914550781
Distortion zoom_blur, severity 3
At batch 195/195...
Error: 0.8398000001907349
Distortion zoom_blur, severity 4
At batch 195/195...
Error: 0.872979998588562
Distortion zoom_blur, severity 5
At batch 195/195...
Error: 0.8997399806976318
Average: 0.8290519714355469

Distortion: zoom_blur        | CE (unnormalized) (%): 82.91

Distortion snow, severity 1
At batch 195/195...
Error: 0.7190799713134766
Distortion snow, severity 2
At batch 195/195...
Error: 0.8801400065422058
Distortion snow, severity 3
At batch 195/195...
Error: 0.8606200218200684
Distortion snow, severity 4
At batch 195/195...
Error: 0.913640022277832
Distortion snow, severity 5
At batch 195/195...
Error: 0.9433199763298035
Average: 0.8633600473403931

Distortion: snow             | CE (unnormalized) (%): 86.34

Distortion frost, severity 1
At batch 195/195...
Error: 0.6222599744796753
Distortion frost, severity 2
At batch 195/195...
Error: 0.8030800223350525
Distortion frost, severity 3
At batch 195/195...
Error: 0.8895000219345093
Distortion frost, severity 4
At batch 195/195...
Error: 0.9006800055503845
Distortion frost, severity 5
At batch 195/195...
Error: 0.9343400001525879
Average: 0.829971969127655

Distortion: frost            | CE (unnormalized) (%): 83.00

Distortion fog, severity 1
At batch 195/195...
Error: 0.663320004940033
Distortion fog, severity 2
At batch 195/195...
Error: 0.74617999792099
Distortion fog, severity 3
At batch 195/195...
Error: 0.8282999992370605
Distortion fog, severity 4
At batch 195/195...
Error: 0.853659987449646
Distortion fog, severity 5
At batch 195/195...
Error: 0.9285200238227844
Average: 0.8039960861206055

Distortion: fog              | CE (unnormalized) (%): 80.40

Distortion brightness, severity 1
At batch 195/195...
Error: 0.4552600383758545
Distortion brightness, severity 2
At batch 195/195...
Error: 0.48142004013061523
Distortion brightness, severity 3
At batch 195/195...
Error: 0.5250200033187866
Distortion brightness, severity 4
At batch 195/195...
Error: 0.5931199789047241
Distortion brightness, severity 5
At batch 195/195...
Error: 0.6836999654769897
Average: 0.5477040410041809

Distortion: brightness       | CE (unnormalized) (%): 54.77

Distortion contrast, severity 1
At batch 195/195...
Error: 0.6446599960327148
Distortion contrast, severity 2
At batch 195/195...
Error: 0.7567600011825562
Distortion contrast, severity 3
At batch 195/195...
Error: 0.8848999738693237
Distortion contrast, severity 4
At batch 195/195...
Error: 0.9768999814987183
Distortion contrast, severity 5
At batch 195/195...
Error: 0.9941200017929077
Average: 0.8514679670333862

Distortion: contrast         | CE (unnormalized) (%): 85.15

Distortion elastic_transform, severity 1
At batch 195/195...
Error: 0.5430799722671509
Distortion elastic_transform, severity 2
At batch 195/195...
Error: 0.7239800095558167
Distortion elastic_transform, severity 3
At batch 195/195...
Error: 0.5709600448608398
Distortion elastic_transform, severity 4
At batch 195/195...
Error: 0.6555200219154358
Distortion elastic_transform, severity 5
At batch 195/195...
Error: 0.8264800310134888
Average: 0.6640040278434753

Distortion: elastic_transform  | CE (unnormalized) (%): 66.40

Distortion pixelate, severity 1
At batch 195/195...
Error: 0.5460799932479858
Distortion pixelate, severity 2
At batch 195/195...
Error: 0.5762200355529785
Distortion pixelate, severity 3
At batch 195/195...
Error: 0.7837799787521362
Distortion pixelate, severity 4
At batch 195/195...
Error: 0.902180016040802
Distortion pixelate, severity 5
At batch 195/195...
Error: 0.9355800151824951
Average: 0.7487679719924927

Distortion: pixelate         | CE (unnormalized) (%): 74.88

Distortion jpeg_compression, severity 1
At batch 195/195...
Error: 0.5230200290679932
Distortion jpeg_compression, severity 2
At batch 195/195...
Error: 0.5603600144386292
Distortion jpeg_compression, severity 3
At batch 195/195...
Error: 0.586080014705658
Distortion jpeg_compression, severity 4
At batch 195/195...
Error: 0.66184002161026
Distortion jpeg_compression, severity 5
At batch 195/195...
Error: 0.7502599954605103
Average: 0.6163120269775391

Distortion: jpeg_compression  | CE (unnormalized) (%): 61.63

Distortion speckle_noise, severity 1
At batch 195/195...
Error: 0.6856399774551392
Distortion speckle_noise, severity 2
At batch 195/195...
Error: 0.7642199993133545
Distortion speckle_noise, severity 3
At batch 195/195...
Error: 0.9047800302505493
Distortion speckle_noise, severity 4
At batch 195/195...
Error: 0.9436399936676025
Distortion speckle_noise, severity 5
At batch 195/195...
Error: 0.9716200232505798
Average: 0.8539799451828003

Distortion: speckle_noise    | CE (unnormalized) (%): 85.40

Distortion gaussian_blur, severity 1
At batch 195/195...
Error: 0.576200008392334
Distortion gaussian_blur, severity 2
At batch 195/195...
Error: 0.7442200183868408
Distortion gaussian_blur, severity 3
At batch 195/195...
Error: 0.8660200238227844
Distortion gaussian_blur, severity 4
At batch 195/195...
Error: 0.9363399744033813
Distortion gaussian_blur, severity 5
At batch 195/195...
Error: 0.9804400205612183
Average: 0.8206440210342407

Distortion: gaussian_blur    | CE (unnormalized) (%): 82.06

Distortion spatter, severity 1
At batch 195/195...
Error: 0.47901999950408936
Distortion spatter, severity 2
At batch 195/195...
Error: 0.6380599737167358
Distortion spatter, severity 3
At batch 195/195...
Error: 0.7627000212669373
Distortion spatter, severity 4
At batch 195/195...
Error: 0.8505399823188782
Distortion spatter, severity 5
At batch 195/195...
Error: 0.9061800241470337
Average: 0.7272999882698059

Distortion: spatter          | CE (unnormalized) (%): 72.73

Distortion saturate, severity 1
At batch 195/195...
Error: 0.49856001138687134
Distortion saturate, severity 2
At batch 195/195...
Error: 0.5440400242805481
Distortion saturate, severity 3
At batch 195/195...
Error: 0.4752799868583679
Distortion saturate, severity 4
At batch 195/195...
Error: 0.5952399969100952
Distortion saturate, severity 5
At batch 195/195...
Error: 0.7237000465393066
Average: 0.5673640370368958

Distortion: saturate         | CE (unnormalized) (%): 56.74

mCE (unnormalized by AlexNet errors) (%): 78.85

Process finished with exit code 0
