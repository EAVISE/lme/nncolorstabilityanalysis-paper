"""
Check stability of ImagNet-trained network (AlexNet, VGG, etc.) to changes in color palette and brightness/contrast.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import torch
import torchvision

from config import Config
from datasets.imagenet_test_dataset import ImageNetTestDatasetFactory
from datasets.imagenet_preproc import ImageNetPreProcess
from experiments.experiment_global import ColorStabilityExperiment

if __name__ == '__main__':
    device = torch.device('cpu')
    # imagenet_pp = ImageNetPreProcess(chain_type=ImageNetPreProcess.FULL)
    # imagenet_name = 'resnet18'
    # imagenet = torchvision.models.alexnet(weights=torchvision.models.AlexNet_Weights.DEFAULT)
    # imagenet = torchvision.models.vgg16(weights=torchvision.models.VGG16_Weights.DEFAULT)
    # imagenet = torchvision.models.resnet18(weights=torchvision.models.ResNet18_Weights.DEFAULT)
    imagenet = torchvision.models.resnet50(weights=torchvision.models.ResNet50_Weights.DEFAULT)
    # imagenet = torchvision.models.densenet161(weights=torchvision.models.DenseNet161_Weights.DEFAULT)
    # file_state_dict = os.path.join(Config.DIR_IMGNET, 'Models', 'alexnet_full_tl_hue+sat_cuda:1.pth')
    # file_state_dict = os.path.join(Config.DIR_IMGNET, 'Models', 'alexnet_classifier_tl_hue_epoch66_cuda:0.pth')
    # file_state_dict = os.path.join(Config.DIR_IMGNET, 'Models', 'resnet18_full_tl_hue_cuda:1.pth')
    # file_state_dict = os.path.join(Config.DIR_IMGNET, 'Models', 'resnet18_full_tl_hue+sat_cuda:0.pth')
    # file_state_dict = os.path.join(Config.DIR_IMGNET, 'Models', 'vgg16_full_tl_hue_cuda:1.pth')
    # state_dict = torch.load(open(file_state_dict, 'rb'), map_location=device)
    # imagenet.load_state_dict(state_dict)

    imagenet_pp = ImageNetPreProcess(chain_type=ImageNetPreProcess.SMALL)
    imagenet.to(device)
    imagenet_name = 'resnet50'

    cse = ColorStabilityExperiment(model=imagenet, preproc=imagenet_pp, crop_size=224, device=device)

    data = ImageNetTestDatasetFactory.get_dataset(base_dir=Config.DIR_IMGNET_VAL)

    b_overwrite_csv = True

    b_process_hue = False
    b_process_saturation = True

    if b_process_hue:
        results_file = os.path.join(Config.DIR_LOGS, f'{imagenet_name}_val_stability_hue.csv')
        cse.process_dataset_for_hue(dataset=data, log_file=results_file, b_overwrite=b_overwrite_csv)
    if b_process_saturation:
        results_file = os.path.join(Config.DIR_LOGS, f'{imagenet_name}_val_stability_saturation.csv')
        cse.process_dataset_for_saturation(dataset=data, log_file=results_file, b_overwrite=b_overwrite_csv)
