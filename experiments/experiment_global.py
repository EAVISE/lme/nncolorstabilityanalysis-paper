"""
Shared "global" experiment code between different types of networks.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import datetime
import os
from copy import copy

import PIL
import numpy as np
import torch
from torchvision import transforms

from tools.pil_hue_changer import PILHueShifter


class ColorStabilityExperiment:
    def __init__(self, model: torch.nn.Module, resize_and_crop=None, preproc=None, crop_size=224, device=torch.device('cuda')):
        """

        :param model: model to be tested.
        :param preproc: preprocessing pipeline.
        :param crop_size: crop size in pixels to be used for centercrop; default=224 is setting for typical ImageNet\
        network; for EmoNet, use 227
        :param device: what device to use
        """
        self.device = device

        self.model = model
        self.model.to(device)
        self.model.eval()
        self.preproc = preproc

        if resize_and_crop is None:
            self.resize_and_crop = transforms.Compose([
                transforms.Resize(256),
                transforms.CenterCrop(crop_size)
            ])
        else:
            self.resize_and_crop = resize_and_crop

        self.saturation_range = np.arange(0., 2.01, 0.05)
        self.hue_range = list(range(0, 360, 10))

    def set_hue_range(self, hue_range):
        self.hue_range = hue_range

    def set_saturation_range(self, saturation_range):
        self.saturation_range = saturation_range

    def process_dataset_for_hue(self, dataset, log_file, b_overwrite=False):
        """
        Perform experiment with images with altered hue levels.

        :param dataset: the torch.utils.data.Dataset to use.
        :param log_file: the file to write the results to.
        :param b_overwrite: overwrite the log file if it already exists? Else, append to it, and don't reprocess files\
        that were already processed.
        :return:
        """
        seen_files = None
        log_file_top5 = log_file[:-4] + "_top5.csv"
        log_file_dist = log_file[:-4] + "_pred_dist.csv"
        if os.path.exists(log_file) and not b_overwrite:
            seen_files = set()
            with open(log_file, 'r') as fin:
                b_first = True
                for line in fin:
                    if b_first:
                        b_first = False
                        continue
                    parts = line.split(',')
                    seen_files.add(parts[0])
            # raise FileExistsError(f"Log file [{log_file}] already exists.")
        else:
            with open(log_file, 'w') as fout:
                fout.write("file," + ','.join([f'{x}' for x in self.hue_range]) + '\n')
            with open(log_file_top5, 'w') as fout:
                fout.write("file," + ','.join([f'{x}/{k+1}' for x in self.hue_range for k in range(5)]) + '\n')
            with open(log_file_dist, 'w') as fout:
                fout.write("file," + ','.join([f'{x}' for x in self.hue_range]) + '\n')

        print(f"Writing to (main) output file: {log_file}")

        files_to_process = copy(dataset.xs)
        if seen_files:
            to_remove = []
            for f in files_to_process:
                if f.replace(',', '-') in seen_files:
                    to_remove.append(f)
            for f in to_remove:
                files_to_process.remove(f)
        files_to_process = sorted(files_to_process)

        time_start = datetime.datetime.now()
        for i in range(len(files_to_process)):
            if (i + 1) % 5 == 0:
                time_now = datetime.datetime.now()
                time_delta = time_now - time_start
                time_remaining = (((len(files_to_process)/(i + 1))-1) * time_delta).total_seconds()
                minutes = time_remaining // 60
                print(f"\rAt image {i + 1} of {len(files_to_process)}... "
                      f"{minutes//60:.0f}h{minutes%60:.0f}m{time_remaining % 60:.0f}s remaining",
                      end='', flush=True)

            preds_per_hue = \
                self._process_img_hue(os.path.join(dataset.base_dir, *files_to_process[i].split('/')))

            # Update index distances
            # pred_distance_from_original

            csv_line = files_to_process[i].replace(',', '-')
            csv_line_top5 = csv_line
            csv_line_pred_dist = csv_line
            orig_pred = torch.argsort(preds_per_hue[self.hue_range[0]], descending=True)

            for idx_h, h in enumerate(sorted(preds_per_hue.keys())):
                top5_preds = torch.topk(preds_per_hue[h], 5)[1]  # Get indices of top 5 predictions
                csv_line += f',{top5_preds[0].item()}'
                for e in top5_preds:
                    csv_line_top5 += f',{e.item()}'
                # Update prediction distances
                if idx_h > 0:
                    pred_dist = (orig_pred == torch.argmax(preds_per_hue[h])).nonzero().item()
                else:
                    pred_dist = 0
                csv_line_pred_dist += f',{pred_dist}'
            with open(log_file, 'a') as fout:
                fout.write(csv_line + '\n')
            with open(log_file_top5, 'a') as fout:
                fout.write(csv_line_top5 + '\n')
            with open(log_file_dist, 'a') as fout:
                fout.write(csv_line_pred_dist + '\n')

        total_time = (datetime.datetime.now() - time_start).total_seconds()
        minutes = total_time // 60
        print(f"\rAt image {len(files_to_process)} of {len(files_to_process)}; total time: "
              f"{minutes//60:.0f}h{minutes%60:.0f}m{total_time%60:.0f}s")

    def process_dataset_for_saturation(self, dataset, log_file, b_overwrite=False):
        """
        Perform experiment with images with altered saturation levels.

        :param dataset: the torch.utils.data.Dataset to use.
        :param log_file: the file to write the results to.
        :param b_overwrite: overwrite the log file if it already exists? Else, append to it, and don't reprocess files\
        that were already processed.
        :return:
        """
        seen_files = None
        log_file_top5 = log_file[:-4] + "_top5.csv"
        log_file_dist = log_file[:-4] + "_pred_dist.csv"
        buffer = []
        if os.path.exists(log_file) and not b_overwrite:
            seen_files = set()
            with open(log_file, 'r') as fin:
                b_first = True
                for line in fin:
                    buffer.append(line)
                    if b_first:
                        b_first = False
                        continue
                    parts = line.split(',')
                    seen_files.add(parts[0])
            # raise FileExistsError(f"Log file [{log_file}] already exists.")
        else:
            with open(log_file, 'w') as fout:
                fout.write("file," + ','.join([f'{x:4.2f}' for x in self.saturation_range]) + '\n')
            with open(log_file_top5, 'w') as fout:
                fout.write("file," + ','.join([f'{x:4.2f}/{k+1}' for x in self.saturation_range for k in range(5)]) + '\n')
            with open(log_file_dist, 'w') as fout:
                fout.write("file," + ','.join([f'{x:4.2f}' for x in self.saturation_range]) + '\n')

        print(f"Writing to (main) output file: {log_file}")

        files_to_process = copy(dataset.xs)
        if seen_files:
            to_remove = []
            for f in files_to_process:
                if f.replace(',', '-') in seen_files:
                    to_remove.append(f)
            for f in to_remove:
                files_to_process.remove(f)
        files_to_process = sorted(files_to_process)

        time_start = datetime.datetime.now()
        for i in range(len(files_to_process)):
            if (i + 1) % 5 == 0:
                time_now = datetime.datetime.now()
                time_delta = time_now - time_start
                time_remaining = (((len(files_to_process)/(i + 1))-1) * time_delta).total_seconds()
                minutes = time_remaining // 60
                print(f"\rAt image {i + 1} of {len(files_to_process)}... "
                      f"{minutes//60:.0f}h{minutes%60:.0f}m{time_remaining % 60:.0f}s remaining",
                      end='', flush=True)

            preds_per_saturation = \
                self._process_img_saturation(os.path.join(dataset.base_dir, *files_to_process[i].split('/')))

            csv_line = files_to_process[i].replace(',', '-')
            csv_line_top5 = csv_line
            csv_line_pred_dist = csv_line
            orig_pred = torch.argsort(preds_per_saturation[1.0], descending=True)

            for idx_g, g in enumerate(sorted(preds_per_saturation.keys())):
                top5_preds = torch.topk(preds_per_saturation[g], 5)[1]  # Get indices of top 5 predictions
                csv_line += f',{top5_preds[0].item()}'
                for e in top5_preds:
                    csv_line_top5 += f',{e.item()}'
                # Update prediction distances
                if g != 1.0:
                    pred_dist = (orig_pred == torch.argmax(preds_per_saturation[g])).nonzero().item()
                else:
                    pred_dist = 0
                csv_line_pred_dist += f',{pred_dist}'
            with open(log_file, 'a') as fout:
                fout.write(csv_line + '\n')
            with open(log_file_top5, 'a') as fout:
                fout.write(csv_line_top5 + '\n')
            with open(log_file_dist, 'a') as fout:
                fout.write(csv_line_pred_dist + '\n')

        total_time = (datetime.datetime.now() - time_start).total_seconds()
        minutes = total_time // 60
        print(f"\rAt image {len(files_to_process)} of {len(files_to_process)}; total time: "
              f"{minutes//60:.0f}h{minutes%60:.0f}m{total_time%60:.0f}s")

    def _process_img_saturation(self, f):
        """
        Given an image at path f, load the image and vary saturation from 0 to 2, in steps of 0.05.\
        For each step, compute model prediction and store.

        :param f: path to the image to be loaded.
        :return:
        """
        self.model.eval()

        with open(f, 'rb') as f:
            img = PIL.Image.open(f).convert('RGB')
        # First, resize and centercrop
        img = self.resize_and_crop(img)

        converter = PIL.ImageEnhance.Color(img)

        pred_per_saturation = {}

        # First, compute all image variations, than bundle them so as to be able to parse the lot as a single batch.
        imgs = []

        # (De-)Saturate
        for g in self.saturation_range:
            # A factor of 1.0 gives the original image.
            img_alt = self.preproc(converter.enhance(g))
            imgs.append(img_alt)

        with torch.no_grad():
            imgs = torch.stack(imgs).to(self.device)
            preds = self.model(imgs)

        for idx, g in enumerate(self.saturation_range):
            pred_per_saturation[g] = preds[idx]

        return pred_per_saturation

    def _process_img_hue(self, f):
        """
        Given an image at path f, load the image and vary hue by adding x degrees, with x from 10 to 350 in steps of 10.\
        For each step, compute EmoNet prediction and store.

        :param f: path to the image to be loaded.
        :return:
        """
        self.model.eval()

        with open(f, 'rb') as f:
            img = PIL.Image.open(f).convert('RGB')

        pred_per_hue = {}

        # First, compute all image variations, than bundle them so as to be able to parse the lot as a single batch.
        # Vary hue
        # First, resize and centercrop
        img = self.resize_and_crop(img)
        imgs = PILHueShifter.shift_hue_8bit_tensor(image=img, hue_shift=self.hue_range, device=self.device)
        imgs = [self.preproc(x) for x in imgs]

        # imgs = []
        # for h in self.hue_range:
        #     new_img = PILHueShifter.shift_hue_8bit(image=img, hue_shift=h)
        #     imgs.append(self.preproc(new_img))

        with torch.no_grad():
            imgs = torch.stack(imgs).to(self.device)
            preds = self.model(imgs)

        for idx, h in enumerate(self.hue_range):
            pred_per_hue[h] = preds[idx]

        return pred_per_hue
