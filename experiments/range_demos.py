"""
Script to demo the effect of the saturation and hue shifts.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""

import os

import PIL
import matplotlib.pyplot as plt
import numpy as np
import torch
import torchvision.models
from torchvision import transforms

from config import Config
from datasets.ilsvrc_labels_map import ILSVRCLabelsMap
from datasets.imagenet_preproc import ImageNetPreProcess
from tools.pil_hue_changer import PILHueShifter


class RangeDemo:
    DEMO_IMG = os.path.join(Config.DIR_IMGNET_TEST, 'ILSVRC2012_test_00000002.JPEG')

    def __init__(self, crop_size=224, b_use_cuda=True, model=None):
        """
        :param crop_size: crop size in pixels to be used for centercrop; default=224 is setting for typical ImageNet\
        network; for EmoNet, use 227
        :param b_use_cuda: use cuda...?
        """
        self.b_use_cuda = b_use_cuda

        self.resize_and_crop = transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(crop_size)
        ])

        self.model = model
        self.model.eval()
        self.imagenet_small = ImageNetPreProcess(chain_type=ImageNetPreProcess.SMALL)
        self.ilsvrc_labels = ILSVRCLabelsMap()

        self.saturation_range = np.arange(0., 2.01, 0.05)
        self.hue_range = list(range(0, 360, 10))

    def demo_img_saturation(self):
        """
        Given an image, load the image and vary saturation from 0 to 2, in steps of 0.05.\
        """
        with open(self.DEMO_IMG, 'rb') as f:
            img = PIL.Image.open(f).convert('RGB')
        # First, resize and centercrop
        img = self.resize_and_crop(img)

        converter = PIL.ImageEnhance.Color(img)

        # (De-)Saturate
        for g in self.saturation_range:
            # A factor of 1.0 gives the original image.
            img_alt = converter.enhance(g)
            plt.title(f"Saturation coefficient: {g}")
            plt.imshow(img_alt)
            plt.show()
            # plt.savefig(os.path.join(Config.DIR_LOGS, f'demo_saturation_{g:.2f}.png'))

    def demo_img_hue(self):
        """
        Given an image, load the image and vary hue by adding x degrees, with x from 10 to 350 in steps of 10.\
        For each step, compute EmoNet prediction and store.
        """
        with open(self.DEMO_IMG, 'rb') as f:
            img = PIL.Image.open(f).convert('RGB')

        img = self.resize_and_crop(img)
        imgs = PILHueShifter.shift_hue_8bit_tensor(image=img, hue_shift=self.hue_range)

        for i, img in enumerate(imgs):
            pred = None
            if self.model is not None:
                with torch.no_grad():
                    pred = self.model(self.imagenet_small(img).unsqueeze(0))
                pred = torch.argmax(pred).item()
                pred = self.ilsvrc_labels.idx_to_label[str(pred)]

            if pred is not None:
                plt.title(f'Hue shift = {self.hue_range[i]} --> {pred}')
            else:
                plt.title(f'Hue shift = {self.hue_range[i]}')
            plt.imshow(img)
            plt.show()
            # plt.savefig(os.path.join(Config.DIR_LOGS, f'demo_hue_{self.hue_range[i]}.png'))

    def create_demo_plot(self, hue_shifts=None):
        if self.model is None:
            raise ValueError("No model specified. You should initialize this class with a model to use this method.")

        # demo_img = os.path.join(Config.DIR_IMGNET_TEST, 'ILSVRC2012_test_00000065.JPEG')
        demo_img = self.DEMO_IMG

        if hue_shifts is None:
            hue_shifts = [0, 10, 20, 30]

        with open(demo_img, 'rb') as f:
            img = PIL.Image.open(f).convert('RGB')

        img = self.resize_and_crop(img)
        # For negative shifts, use '%360'; e.g., '-10' becomes '350'.
        # Using negative numbers can result in some artefacts.
        imgs = PILHueShifter.shift_hue_8bit_tensor(image=img, hue_shift=hue_shifts)

        preds = []
        for i, img in enumerate(imgs):
            with torch.no_grad():
                pred = self.model(self.imagenet_small(img).unsqueeze(0))
            pred = torch.argmax(pred).item()
            preds.append(self.ilsvrc_labels.idx_to_label[str(pred)])

        title_size = 2.5
        fig, axes = plt.subplots(1, 4, sharex=True, sharey=True, figsize=(16, 4), dpi=1200)
        axes[0].imshow(imgs[0])
        axes[0].set_xticks(ticks=[])
        axes[0].set_yticks(ticks=[])
        axes[0].set_title(f'Hue shift={hue_shifts[0]},\nlabel={preds[0][1]}', fontsize=title_size)
        axes[1].imshow(imgs[1])
        axes[1].set_title(f'Hue shift={hue_shifts[1]},\nlabel={preds[1][1]}', fontsize=title_size)
        axes[2].imshow(imgs[2])
        axes[2].set_title(f'Hue shift={hue_shifts[2]},\nlabel={preds[2][1]}', fontsize=title_size)
        axes[3].imshow(imgs[3])
        axes[3].set_title(f'Hue shift={hue_shifts[3]},\nlabel={preds[3][1]}', fontsize=title_size)

        plt.tight_layout()
        plt.show()


if __name__ == '__main__':
    model = torchvision.models.alexnet(weights=torchvision.models.AlexNet_Weights.DEFAULT)

    rd = RangeDemo(model=model)
    # GOOD examples: 58, 59, 65 --> WINNER
    # for i in range(1, 5000):
    # rd.DEMO_IMG = os.path.join(Config.DIR_IMGNET_TEST, f'ILSVRC2012_test_00000{i:03d}.JPEG')
    rd.DEMO_IMG = os.path.join(Config.DIR_IMGNET_TEST, f'ILSVRC2012_test_00000065.JPEG')
    # rd.DEMO_IMG = os.path.join(Config.HOME_DIR, 'Work', 'Temp', 'stop_sign.jpeg')
    rd.create_demo_plot()
    # rd.saturation_range = np.arange(1.00, 25.01, 1.0)
    # rd.demo_img_saturation()
    # rd.demo_img_hue()
