"""
Check stability of networks trained on Places365 dataset networks to changes in color palette and brightness/contrast.
Official Places365 repo: https://github.com/CSAILVision/places365/

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import torch
from torchvision.transforms import transforms

from config import Config
from datasets.imagenet_test_dataset import ImageNetTestDatasetFactory
from datasets.imagenet_preproc import ImageNetPreProcess
from experiments.experiment_global import ColorStabilityExperiment
from experiments.places365_model import Places365Model

if __name__ == '__main__':
    device = torch.device('cuda')
    places_name = Places365Model.RESNET18
    places_net = Places365Model.load_model(places_name)

    resize_and_crop = transforms.Compose([
        transforms.CenterCrop(224)
    ])
    places_pp = ImageNetPreProcess(chain_type=ImageNetPreProcess.SMALL)

    cse = ColorStabilityExperiment(model=places_net, resize_and_crop=resize_and_crop,
                                   preproc=places_pp, crop_size=224, device=device)

    data = ImageNetTestDatasetFactory.get_dataset(base_dir=Config.DIR_IMGS_365)

    b_overwrite_csv = True

    b_process_hue = False
    b_process_saturation = True

    if b_process_hue:
        print("Performing hue experiment...")
        results_file = os.path.join(Config.DIR_LOGS, f'{places_name}_places365_stability_hue.csv')
        cse.process_dataset_for_hue(dataset=data, log_file=results_file, b_overwrite=b_overwrite_csv)

    if b_process_saturation:
        print("Performing saturation experiment...")
        results_file = os.path.join(Config.DIR_LOGS, f'{places_name}_places365_stability_saturation.csv')
        cse.process_dataset_for_saturation(dataset=data, log_file=results_file, b_overwrite=b_overwrite_csv)
