"""

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
from collections import Counter

import numpy as np
from matplotlib import pyplot as plt
import os
import pandas as pd

from config import Config
import seaborn as sb


class EmoNetStabilityAnalysis:
    @staticmethod
    def analyze_saturation(f: str):
        """
        Analyze results of saturation experiment, generated using emonet_stability.py.

        :param f: path to the CSV file containing the saturation experiment results.
        :return:
        """
        csv = pd.read_csv(f)

        nb_rows = csv.shape[0]

        ref = csv["1.00"]
        keys = list(csv.columns)[1:]

        # Number of classifications identical to ref, in pc., per hue shift
        scores = {}
        for k in keys:
            target = csv[k]
            equal = 0
            for t in zip(ref, target):
                if t[0] == t[1]:
                    equal += 1
            scores[k] = equal

        for k in sorted(scores.keys()):
            print(f"{k}: {scores[k]} --> {scores[k]/nb_rows:.2f}")

        plt_vals = [scores[k]/nb_rows for k in sorted(scores.keys())]

        fig = plt.figure()
        plt.title('Ratio of identical classification to ref (g=1.00) per color gain')
        plt.bar(keys, plt_vals)
        plt.xlabel('Color gain')
        plt.ylabel('Correct (ratio)')
        plt.show()

        # Confusion matrix over all color scales
        conf_matrix = np.zeros((20, 20))
        for row_idx, row in csv.iterrows():
            ref = row['1.00']
            for k in keys:
                if k == '1.00':
                    continue
                conf_matrix[ref, row[k]] += 1

        # Row-normalize
        conf_matrix_norm = conf_matrix.copy()
        for row_idx in range(20):
            row_sum = sum(conf_matrix_norm[row_idx, :])
            conf_matrix_norm[row_idx, :] /= row_sum

        fig = plt.figure()
        plt.title('Confusion matrix over all color gains')
        ax = sb.heatmap(conf_matrix_norm)
        plt.xlabel('Classified as')
        plt.ylabel('Reference')
        plt.tight_layout()
        plt.show()

        # Per color gain
        ref = csv['1.00']
        for k in keys:
            if k == '1.00':
                continue
            target = csv[k]

            conf_matrix = np.zeros((20, 20))
            for t in zip(ref, target):
                conf_matrix[t[0], t[1]] += 1

            # Row-normalize
            conf_matrix_norm = conf_matrix.copy()
            for row_idx in range(20):
                row_sum = sum(conf_matrix_norm[row_idx, :])
                conf_matrix_norm[row_idx, :] /= row_sum

            fig = plt.figure()
            plt.title(f'Color scale: {k}')
            ax = sb.heatmap(conf_matrix_norm)
            plt.xlabel('Classified as')
            plt.ylabel('Reference')
            plt.tight_layout()
            plt.show()


    @staticmethod
    def analyze_hue(f: str):
        """
        Analyze results of hue experiment, generated using emonet_stability.py.

        :param f: path to the CSV file containing the hue experiment results.
        :return:
        """
        csv = pd.read_csv(f)

        nb_rows = csv.shape[0]

        ref = csv["0"]
        keys = list(csv.columns)[1:]

        # Number of classifications identical to ref, in pc., per hue shift
        scores = {}
        for k in keys:
            target = csv[k]
            equal = 0
            for t in zip(ref, target):
                if t[0] == t[1]:
                    equal += 1
            scores[int(k)] = equal

        for k in sorted(scores.keys()):
            print(f"{k}: {scores[k]} --> {scores[k] / nb_rows:.2f}")

        plt_vals = [scores[k] / nb_rows for k in sorted(scores.keys())]

        fig = plt.figure()
        plt.title('Pc. of identical classification to reference (d=0) per hue shift')
        plt.bar(keys, plt_vals)
        plt.xlabel('Hue shift (degrees)')
        plt.ylabel('Correct (ratio)')
        plt.show()

        # Confusion matrix over all color scales
        conf_matrix = np.zeros((20, 20))
        for row_idx, row in csv.iterrows():
            ref = row['0']
            for k in keys:
                if k == '0':
                    continue
                conf_matrix[ref, row[k]] += 1

        # Row-normalize
        conf_matrix_norm = conf_matrix.copy()
        for row_idx in range(20):
            row_sum = sum(conf_matrix_norm[row_idx, :])
            conf_matrix_norm[row_idx, :] /= row_sum

        fig = plt.figure()
        plt.title('Confusion matrix over all hue shifts')
        ax = sb.heatmap(conf_matrix_norm)
        plt.xlabel('Classified as')
        plt.ylabel('Reference')
        plt.tight_layout()
        plt.show()

        # Per hue shift
        ref = csv['0']
        for k in keys:
            if k == '0':
                continue
            target = csv[k]

            conf_matrix = np.zeros((20, 20))
            for t in zip(ref, target):
                conf_matrix[t[0], t[1]] += 1

            # Row-normalize
            conf_matrix_norm = conf_matrix.copy()
            for row_idx in range(20):
                row_sum = sum(conf_matrix_norm[row_idx, :])
                conf_matrix_norm[row_idx, :] /= row_sum

            fig = plt.figure()
            plt.title(f'Hue shift: {k}')
            ax = sb.heatmap(conf_matrix_norm)
            plt.xlabel('Classified as')
            plt.ylabel('Reference')
            plt.tight_layout()
            plt.show()


if __name__ == '__main__':
    csv_sat = os.path.join(Config.DIR_LOGS, 'emonet_stability_saturation.csv')
    csv_hue = os.path.join(Config.DIR_LOGS, 'emonet_stability_hue.csv')

    EmoNetStabilityAnalysis.analyze_hue(csv_hue)
