"""
Check stability of EmoNet to changes in color palette and brightness/contrast.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import torch
from emonet import EmoNet, EmoNetPreProcess

from config import Config
from datasets.emonet_dataset import EmoNetDatasetFactory
from experiments.experiment_global import ColorStabilityExperiment

if __name__ == '__main__':
    device = torch.device('cpu')
    emonet = EmoNet.get_emonet(b_eval=True)
    emonet_pp = EmoNetPreProcess()
    cse = ColorStabilityExperiment(model=emonet, preproc=emonet_pp, crop_size=227, device=device)

    data = EmoNetDatasetFactory.get_dataset(base_dir=Config.DIR_IMGNET_VAL)

    b_overwrite_csv = True

    b_process_hue = False
    b_process_saturation = True

    if b_process_hue:
        results_file = os.path.join(Config.DIR_LOGS, 'emonet_val_stability_hue.csv')
        cse.process_dataset_for_hue(dataset=data, log_file=results_file, b_overwrite=b_overwrite_csv)

    if b_process_saturation:
        results_file = os.path.join(Config.DIR_LOGS, 'emonet_val_stability_saturation.csv')
        cse.process_dataset_for_saturation(dataset=data, log_file=results_file, b_overwrite=b_overwrite_csv)
