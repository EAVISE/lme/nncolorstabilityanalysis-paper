"""
Check stability of ImagNet-trained network (AlexNet, VGG, etc.) to changes in color palette and brightness/contrast.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import torch
import torchvision

from config import Config
from datasets.imagenet_test_dataset import ImageNetTestDatasetFactory
from datasets.imagenet_preproc import ImageNetPreProcess
from experiments.experiment_global import ColorStabilityExperiment


class StylizedModels:
    ALEXNET_SIN = 'alexnet_sin'
    VGG16 = 'vgg16_sin'
    RESNET50_SIN = 'resnet50_sin'


if __name__ == '__main__':
    device = torch.device('cuda')
    imagenet_name = StylizedModels.RESNET50_SIN

    if imagenet_name == StylizedModels.ALEXNET_SIN:
        imagenet = torchvision.models.alexnet(weights=None)
        statedict = torch.load(os.path.join(Config.DIR_PRETRAINED_MODELS, 'StylizedImageNet',
                                            'alexnet_train_60_epochs_lr0.001-b4aa5238.pth.tar'))['state_dict']
    elif imagenet_name == StylizedModels.VGG16:
        imagenet = torchvision.models.vgg16(weights=None)
        statedict = torch.load(os.path.join(Config.DIR_PRETRAINED_MODELS, 'StylizedImageNet',
                                            'vgg16_train_60_epochs_lr0.01-6c6fcc9f.pth.tar'))['state_dict']
    elif imagenet_name == StylizedModels.RESNET50_SIN:
        imagenet = torchvision.models.resnet50(weights=None)
        statedict = torch.load(os.path.join(Config.DIR_PRETRAINED_MODELS, 'StylizedImageNet',
                                            'resnet50_train_60_epochs-c8e5653e.pth.tar'))['state_dict']
    else:
        raise ValueError(f"Model {imagenet_name} is currently not supported.")

    orig_keys = list(statedict.keys())
    for k in orig_keys:
        alt_k = k.replace('module.', '')
        statedict[alt_k] = statedict[k]
        if alt_k != k:
            del statedict[k]

    imagenet.load_state_dict(statedict)
    imagenet_pp = ImageNetPreProcess(chain_type=ImageNetPreProcess.SMALL)

    cse = ColorStabilityExperiment(model=imagenet, preproc=imagenet_pp, crop_size=224, device=device)

    data = ImageNetTestDatasetFactory.get_dataset(base_dir=Config.DIR_IMGNET_VAL)

    b_overwrite_csv = True

    b_process_hue = False
    b_process_saturation = True

    if b_process_hue:
        results_file = os.path.join(Config.DIR_LOGS, f'{imagenet_name}_stability_hue.csv')
        cse.process_dataset_for_hue(dataset=data, log_file=results_file, b_overwrite=b_overwrite_csv)
    if b_process_saturation:
        results_file = os.path.join(Config.DIR_LOGS, f'{imagenet_name}_stability_saturation.csv')
        cse.process_dataset_for_saturation(dataset=data, log_file=results_file, b_overwrite=b_overwrite_csv)
