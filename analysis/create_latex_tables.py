"""
Code used to create the LaTeX tables used in the paper.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import numpy as np
import pandas as pd

from config import Config
from imagenet_scoring.imagenet_scorer import ImageNetScorer


class CreateLaTeXTables:
    def __init__(self):
        print("Initializing CreateLaTeXTables...", end='', flush=True)
        self.in_scorer = ImageNetScorer()
        print(" done!")

    # ============================================================
    # Create table methods
    # ============================================================
    def create_hue_results_table(self, models: [tuple], label: str):
        """

        :param models: list of tuples, where each tuple is of the format\
        (b_imgnet_1k, model_name, main_csv_file); b_imgnet_1k is a boolean indicating whether top1/top5 stats\
        should be computed for this model.
        :param label: text to insert into LaTeX table \label{} command
        :return:
        """
        if label is None:
            label = "tb:model_stats"
        nb_models = len(models)
        beg = "\\begin{landscape}\n" \
              "\\begin{table}[!htbp]\n" \
              "\t\\centering\n" \
              "\t\\caption{Model statistics}\n" \
              "\t\\label{" + label + "}\n" \
              "\t\\begin{tabular}{@{}l|" + 'l'*nb_models +"@{}}\n"
        end = "\t\\end{tabular}\n" \
              "\\end{table}\n" \
              "\\end{landscape}"

        header = ''
        # avg_equal_all_hues, avg_equal_max_30, avg_equal_not_max_30, \
        #     ref_top1, \
        #     avg_top1_all_hues, avg_overlap_ref_corr_all_hues, avg_overlap_ref_wrong_all_hues, \
        #     avg_top1_max_30, avg_overlap_ref_corr_max_30, avg_overlap_ref_wrong_max_30, \
        #     avg_top1_not_max_30, avg_overlap_ref_corr_not_max_30, avg_overlap_ref_wrong_not_max_30, \
        #     avg_pred_dist_all_hues, avg_pred_dist_max_30, avg_pred_dist_not_max_30, \
        #     ref_top5, \
        #     avg_top5_all_hues, avg_top5_max_30, avg_top5_not_max_30
        linebreaks = {3, 4, 7, 10, 13, 16, 17}  # Insert horizontal break after line...; counting from 1
        lines = []
        for idx, metric in enumerate([
            'Equal $d_{all}$', 'Equal $|d| \leq 30$', 'Equal $|d| > 30$',
            'Top1 $d_0$',
            'Top1 $d_{all}$', 'OL+ $d_{all}$', 'OL$-$ $d_{all}$',
            'Top1 $|d| \leq 30$', 'OL+ $|d| \leq 30$', 'OL$-$ $|d| \leq 30$',
            'Top1 $|d| > 30$', 'OL+ $|d| > 30$', 'OL$-$ $|d| > 30$',
            'O.P. $d_{all}$', 'O.P. $|d| \leq 30$', 'O.P. $|d| > 30$',
            'Top5 $d_0$',
            'Top5 $d_{all}$', 'Top5 $|d| \leq 30$', 'Top5 $|d| > 30$',
        ]):
            if idx in linebreaks:
                lines.append("\t\t\\midrule")
            lines.append("\t\t" + metric + " &")
        for model_idx, tpl in enumerate(models):
            print(f"\rAt model {model_idx+1} of {len(models)}...", end='', flush=True)

            b_imgnet_1k = tpl[0]
            model_name = tpl[1]
            header += ' & \\vtext{' + model_name.replace('_', '\\_') + '}'
            if model_idx+1 == len(models):
                header += ' \\\\\n'
            f = tpl[2]

            model_stats = self.analyze_hue_results(b_imgnet_1k, f)
            offset = 0
            for idx, res_tpl in enumerate(model_stats):
                if idx in linebreaks:
                    offset += 1
                line_idx = idx + offset
                if isinstance(res_tpl[0], float):
                    if res_tpl[0] < 1:
                        a = f"{res_tpl[0]:.3f}"[1:]
                        b = f"{res_tpl[1]:.2f}"[1:]
                        lines[line_idx] += f" ${a}^{{{b}}}$"
                    else:
                        lines[line_idx] += f" ${res_tpl[0]:.1f}^{{{res_tpl[1]:.0f}}}$"
                elif res_tpl[0] in {'ref_top1', 'ref_top5'}:
                    lines[line_idx] += " " + f"{res_tpl[1]:.3f}"[1:]
                else:
                    lines[line_idx] += f" {res_tpl[1]}"
                if model_idx+1 < len(models):
                    lines[line_idx] += " &"
                else:
                    lines[line_idx] += " \\\\"
        print(f"\rAt model {len(models)} of {len(models)}...", end='', flush=True)

        table = beg
        table += header + "\t\t\\midrule\n"
        for l in lines:
            table += l + '\n'
        table += end

        print('\n\n')
        print(table)

    def create_sat_results_table(self, models: [tuple], label: str):
        """

        :param models: list of tuples, where each tuple is of the format\
        (b_imgnet_1k, model_name, main_csv_file); b_imgnet_1k is a boolean indicating whether top1/top5 stats\
        should be computed for this model.
        :param label: text to insert into LaTeX table \label{} command
        :return:
        """
        if label is None:
            label = "tb:model_sat_stats"
        nb_models = len(models)
        beg = "\\begin{landscape}\n" \
              "\\begin{table}[!htbp]\n" \
              "\t\\centering\n" \
              "\t\\caption{Model statistics}\n" \
              "\t\\label{" + label + "}\n" \
              "\t\\begin{tabular}{@{}l|" + 'l'*nb_models +"@{}}\n"
        end = "\t\\end{tabular}\n" \
              "\\end{table}\n" \
              "\\end{landscape}"

        header = ''
        # avg_equal_all_gains, avg_equal_1std, avg_equal_gt_1st,\
        #             ref_top1,\
        #             avg_top1_all_gains, avg_overlap_ref_corr_all_gains, avg_overlap_ref_wrong_all_gains,\
        #             avg_top1_1std, avg_overlap_ref_corr_1std, avg_overlap_ref_wrong_1std,\
        #             avg_top1_gt_1std, avg_overlap_ref_corr_gt_1std, avg_overlap_ref_wrong_gt_1std,\
        #             avg_pred_dist_all_gains, avg_pred_dist_1std, avg_pred_dist_gt_1std, \
        #             ref_top5,\
        #             avg_top5_all_gains, avg_top5_1std, avg_top5_gt_1std
        linebreaks = {3, 4, 7, 10, 13, 16, 17}  # Insert horizontal break after line...; counting from 1
        lines = []
        for idx, metric in enumerate([
            'Equal $g_{all}$', 'Equal $g \in\ ]0.5, 1.5[\\backslash\\{1\\}$', 'Equal $g \in [0, 0.5]\\cup[1.5, 2]$',
            'Top1 $g_1$',
            'Top1 $g_{all}$', 'OL+ $g_{all}$', 'OL$-$ $g_{all}$',
            'Top1 $g \in\ ]0.5, 1.5[\\backslash\\{1\\}$', 'OL+ $g \in\ ]0.5, 1.5[\\backslash\\{1\\}$',
                'OL$-$ $g \in\ ]0.5, 1.5[\\backslash\\{1\\}$',
            'Top1 $g \in [0, 0.5]\\cup[1.5, 2]$', 'OL+ $g \in [0, 0.5]\\cup[1.5, 2]$', 'OL$-$ $g \in [0, 0.5]\\cup[1.5, 2]$',
            'O.P. $g_{all}$', 'O.P. $g \in [0, 0.5]\\cup[1.5, 2]$', 'O.P. $g \in\ ]0.5, 1.5[\\backslash\\{1\\}$',
            'Top5 $d_0$',
            'Top5 $g_{all}$', 'Top5 $g \in [0, 0.5]\\cup[1.5, 2]$', 'Top5 $g \in\ ]0.5, 1.5[\\backslash\\{1\\}$',
        ]):
            if idx in linebreaks:
                lines.append("\t\t\\midrule")
            lines.append("\t\t" + metric + " &")
        for model_idx, tpl in enumerate(models):
            print(f"\rAt model {model_idx+1} of {len(models)}...", end='', flush=True)

            b_imgnet_1k = tpl[0]
            model_name = tpl[1]
            if ' ' in model_name:
                header += ' & \\vtext{\\makecell{' + model_name.replace(' ', '\\\\') + '}}'
            else:
                header += ' & \\vtext{' + model_name.replace('_', '\\_') + '}'
            if model_idx+1 == len(models):
                header += ' \\\\\n'
            f = tpl[2]

            model_stats = self.analyze_saturation_results(b_imgnet_1k, f)
            offset = 0
            for idx, res_tpl in enumerate(model_stats):
                if idx in linebreaks:
                    offset += 1
                line_idx = idx + offset
                if isinstance(res_tpl[0], float):
                    if res_tpl[0] < 1:
                        a = f"{res_tpl[0]:.3f}"[1:]
                        b = f"{res_tpl[1]:.2f}"[1:]
                        lines[line_idx] += f" ${a}^{{{b}}}$"
                    else:
                        lines[line_idx] += f" ${res_tpl[0]:.1f}^{{{res_tpl[1]:.0f}}}$"
                elif res_tpl[0] in {'ref_top1', 'ref_top5'}:
                    lines[line_idx] += " " + f"{res_tpl[1]:.3f}"[1:]
                else:
                    lines[line_idx] += f" {res_tpl[1]}"
                if model_idx+1 < len(models):
                    lines[line_idx] += " &"
                else:
                    lines[line_idx] += " \\\\"
        print(f"\rAt model {len(models)} of {len(models)}...", end='', flush=True)

        table = beg
        table += header + "\t\t\\midrule\n"
        for l in lines:
            table += l + '\n'
        table += end

        print('\n\n')
        print(table)

    def create_hue_sat_results_table(self, models: [tuple], label: str):
        """

        :param models: list of tuples, where each tuple is of the format\
        (b_imgnet_1k, model_name, main_csv_file); b_imgnet_1k is a boolean indicating whether top1/top5 stats\
        should be computed for this model.
        :param label: text to insert into LaTeX table \label{} command
        :return:
        """
        if label is None:
            label = "tb:model_stats"
        nb_models = len(models)
        beg = "\\begin{landscape}\n" \
              "\\begin{table}[!htbp]\n" \
              "\t\\centering\n" \
              "\t\\caption{Model statistics}\n" \
              "\t\\label{" + label + "}\n" \
              "\t\\begin{tabular}{@{}l|" + 'l'*nb_models +"@{}}\n"
        end = "\t\\end{tabular}\n" \
              "\\end{table}\n" \
              "\\end{landscape}"

        header = ''
        # avg_equal_all_hues, avg_equal_max_30,\
        # avg_equal_all_gains, avg_equal_1std,\
        #             ref_top1,\
        #             avg_top1_all_hues, avg_overlap_ref_corr_all_hues, avg_overlap_ref_wrong_all_hues,\
        #             avg_top1_all_gains, avg_overlap_ref_corr_all_gains, avg_overlap_ref_wrong_all_gains,\
        #             avg_top1_max_30, avg_overlap_ref_corr_max_30, avg_overlap_ref_wrong_max_30,\
        #             avg_top1_1std, avg_overlap_ref_corr_1std, avg_overlap_ref_wrong_1std,\
        #             avg_pred_dist_all_hues, avg_pred_dist_max_30,\
        #             avg_pred_dist_all_gains, avg_pred_dist_1std
        linebreaks = {2, 4, 5, 8, 11, 14, 17, 19}  # Insert horizontal break after line...; counting from 1
        lines = []
        for idx, metric in enumerate([
            'Equal $d_{all}$', 'Equal $|d| \leq 30$',
            'Equal $g_{all}$', 'Equal $g \in\ ]0.5, 1.5[_{\\backslash\\{1\\}}$',
            'Top1 $d_0$,$g_1$',
            'Top1 $d_{all}$', 'OL+ $d_{all}$', 'OL$-$ $d_{all}$',
            'Top1 $g_{all}$', 'OL+ $g_{all}$', 'OL$-$ $g_{all}$',
            'Top1 $|d| \leq 30$', 'OL+ $|d| \leq 30$', 'OL$-$ $|d| \leq 30$',
            'Top1 $g \in\ ]0.5, 1.5[_{\\backslash\\{1\\}}$', 'OL+ $g \in\ ]0.5, 1.5[_{\\backslash\\{1\\}}$',
                'OL$-$ $g \in\ ]0.5, 1.5[_{\\backslash\\{1\\}}$',
            'O.P. $d_{all}$', 'O.P. $|d| \leq 30$',
            'O.P. $g_{all}$', 'O.P. $g \in\ ]0.5, 1.5[_{\\backslash\\{1\\}}$'
        ]):
            if idx in linebreaks:
                lines.append("\t\t\\midrule")
            lines.append("\t\t" + metric + " &")
        for model_idx, tpl in enumerate(models):
            print(f"\rAt model {model_idx+1} of {len(models)}...", end='', flush=True)

            b_imgnet_1k = tpl[0]
            model_name = tpl[1]
            if ' ' in model_name:
                header += ' & \\vtext{\\makecell[l]{' + model_name.replace('_', '\\_').replace(' ', '\\\\') + '}}'
            else:
                header += ' & \\vtext{' + model_name.replace('_', '\\_') + '}'
            if model_idx+1 == len(models):
                header += ' \\\\\n'
            f = tpl[2]

            model_stats = self.analyze_hue_sat_results(b_imgnet_1k, f)
            offset = 0
            for idx, res_tpl in enumerate(model_stats):
                if idx in linebreaks:
                    offset += 1
                line_idx = idx + offset
                if isinstance(res_tpl[0], float):
                    if res_tpl[0] < 1:
                        a = f"{res_tpl[0]:.3f}"[1:]
                        b = f"{res_tpl[1]:.2f}"[1:]
                        lines[line_idx] += f" ${a}^{{{b}}}$"
                    else:
                        lines[line_idx] += f" ${res_tpl[0]:.1f}^{{{res_tpl[1]:.0f}}}$"
                elif res_tpl[0] in {'ref_top1', 'ref_top5'}:
                    lines[line_idx] += " " + f"{res_tpl[1]:.3f}"[1:]
                else:
                    lines[line_idx] += f" {res_tpl[1]}"
                if model_idx+1 < len(models):
                    lines[line_idx] += " &"
                else:
                    lines[line_idx] += " \\\\"
        print(f"\rAt model {len(models)} of {len(models)}...", end='', flush=True)

        table = beg
        table += header + "\t\t\\midrule\n"
        for l in lines:
            table += l + '\n'
        table += end

        print('\n\n')
        print(table)

    # ============================================================
    # Analyze data methods
    # ============================================================
    def analyze_hue_results(self, b_imgnet_1k: bool, f: str):
        """
        Analyze results of hue experiment.

        :param b_imgnet_1k: compute Top1/Top5 stats for model, or not?
        :param f: path to the CSV file containing the hue experiment results.
        :return:
        """
        csv_top1 = pd.read_csv(f)
        f_top5 = f[:-4] + '_top5.csv'
        # csv_top5 = pd.read_csv(f_top5)
        f_pred_dist = f[:-4] + '_pred_dist.csv'

        nb_rows = csv_top1.shape[0]

        ref = csv_top1["0"]
        hue_shifts = list(csv_top1.columns)[1:]

        # Number of classifications identical to ref, in pc., per hue shift
        # At the same time:
        # -Compute average/std of identical predictions over all non-0 hue shifts
        # -Compute average/std of identical predictions over all hue shifts d with -30 <= d <= 30, d != 0
        equal_preds = {}
        for h in hue_shifts:
            target = csv_top1[h]
            equal = 0
            for t in zip(ref, target):
                if t[0] == t[1]:
                    equal += 1
            equal_preds[int(h)] = equal/nb_rows

        avg_equal_all_hues = self._compute_avg_and_std_over_dict_for_keys(equal_preds, list(range(10, 360, 10)))
        avg_equal_max_30 = self._compute_avg_and_std_over_dict_for_keys(equal_preds, [10, 20, 30, 330, 340, 350])
        avg_equal_not_max_30 = self._compute_avg_and_std_over_dict_for_keys(equal_preds, list(range(40, 330, 10)))

        # for k in sorted(top1_scores.keys()):
        #     print(f"{k}: {top1_scores[k]} --> {top1_scores[k] / nb_rows:.2f}")

        # Compute Top1/Top5 scores for each hue shift
        if b_imgnet_1k:
            top1_per_hue = self.in_scorer.compute_top1_score_for_csv(f, b_silent=True)
            ref_top1 = ('ref_top1', top1_per_hue[0])
            avg_top1_all_hues = self._compute_avg_and_std_over_dict_for_keys(top1_per_hue, list(range(10, 360, 10)))
            avg_top1_max_30 = self._compute_avg_and_std_over_dict_for_keys(top1_per_hue, [10, 20, 30, 330, 340, 350])
            avg_top1_not_max_30 = self._compute_avg_and_std_over_dict_for_keys(top1_per_hue, list(range(40, 330, 10)))

            top5_per_hue = self.in_scorer.compute_top5_score_for_csv(f_top5, b_silent=True)
            ref_top5 = ('ref_top5', top5_per_hue[0])
            avg_top5_all_hues = self._compute_avg_and_std_over_dict_for_keys(top5_per_hue, list(range(10, 360, 10)))
            avg_top5_max_30 = self._compute_avg_and_std_over_dict_for_keys(top5_per_hue, [10, 20, 30, 330, 340, 350])
            avg_top5_not_max_30 = self._compute_avg_and_std_over_dict_for_keys(top5_per_hue, list(range(40, 330, 10)))
        else:
            ref_top1 = ('-', '-')
            avg_top1_all_hues = ('-', '-')
            avg_top1_max_30 = ('-', '-')
            avg_top1_not_max_30 = ('-', '-')

            ref_top5 = ('-', '-')
            avg_top5_all_hues = ('-', '-')
            avg_top5_max_30 = ('-', '-')
            avg_top5_not_max_30 = ('-', '-')

        # Stats on what percentage of those predictions that originally were correct/wrong changed
        if b_imgnet_1k:
            acc_per_hue, overlap_ref_corr_per_col, overlap_ref_wrong_per_col =\
                self.in_scorer.compare_corr_vs_wrong_for_csv(f, b_silent=True)
            avg_overlap_ref_corr_all_hues = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_corr_per_col,
                                                                                         list(range(10, 360, 10)))
            avg_overlap_ref_corr_max_30 = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_corr_per_col,
                                                                                       [10, 20, 30, 330, 340, 350])
            avg_overlap_ref_corr_not_max_30 = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_corr_per_col,
                                                                                           list(range(40, 330, 10)))
            avg_overlap_ref_wrong_all_hues = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_wrong_per_col,
                                                                                          list(range(10, 360, 10)))
            avg_overlap_ref_wrong_max_30 = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_wrong_per_col,
                                                                                        [10, 20, 30, 330, 340, 350])
            avg_overlap_ref_wrong_not_max_30 = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_wrong_per_col,
                                                                                            list(range(40, 330, 10)))
        else:
            avg_overlap_ref_corr_all_hues = ('-', '-')
            avg_overlap_ref_corr_max_30 = ('-', '-')
            avg_overlap_ref_corr_not_max_30 = ('-', '-')
            avg_overlap_ref_wrong_all_hues = ('-', '-')
            avg_overlap_ref_wrong_max_30 = ('-', '-')
            avg_overlap_ref_wrong_not_max_30 = ('-', '-')


        # Stats on average distance of prediction from original prediction
        csv_pred_dist = pd.read_csv(f_pred_dist)
        avg_pred_dist_all_hues = self._compute_avg_std_pred_dist_for_hues(csv_pred_dist,
                                                                              list(range(10, 360, 10)))
        avg_pred_dist_max_30 = self._compute_avg_std_pred_dist_for_hues(csv_pred_dist,
                                                                            [10, 20, 30, 330, 340, 350])
        avg_pred_dist_not_max_30 = self._compute_avg_std_pred_dist_for_hues(csv_pred_dist,
                                                                                list(range(40, 330, 10)))

        return avg_equal_all_hues, avg_equal_max_30, avg_equal_not_max_30,\
            ref_top1,\
            avg_top1_all_hues, avg_overlap_ref_corr_all_hues, avg_overlap_ref_wrong_all_hues,\
            avg_top1_max_30, avg_overlap_ref_corr_max_30, avg_overlap_ref_wrong_max_30,\
            avg_top1_not_max_30, avg_overlap_ref_corr_not_max_30, avg_overlap_ref_wrong_not_max_30,\
            avg_pred_dist_all_hues, avg_pred_dist_max_30, avg_pred_dist_not_max_30, \
            ref_top5,\
            avg_top5_all_hues, avg_top5_max_30, avg_top5_not_max_30

    def analyze_saturation_results(self, b_imgnet_1k: bool, f: str):
        """
        Analyze results of saturation experiment.

        :param b_imgnet_1k: compute Top1/Top5 stats for model, or not?
        :param f: path to the CSV file containing the saturation experiment results.
        :return:
        """
        csv_top1 = pd.read_csv(f)
        f_top5 = f[:-4] + '_top5.csv'
        # csv_top5 = pd.read_csv(f_top5)
        f_pred_dist = f[:-4] + '_pred_dist.csv'

        nb_rows = csv_top1.shape[0]

        ref = csv_top1["1.00"]
        sat_shifts = list(csv_top1.columns)[1:]

        # Number of classifications identical to ref, in pc., per saturation shift
        # At the same time:
        # -Compute average/std of identical predictions over saturation shifts with g \in [0.5, 1.5]\1.
        # -Compute average/std of identical predictions over saturation shifts with g \in [0, 0.5] U [1.5, 2].
        equal_preds = {}
        for g in sat_shifts:
            target = csv_top1[g]
            equal = 0
            for t in zip(ref, target):
                if t[0] == t[1]:
                    equal += 1
            equal_preds[g] = equal/nb_rows

        avg_equal_all_gains = self._compute_avg_and_std_over_dict_for_keys(equal_preds,
                                                                          [f'{x:.2f}' for x in
                                                                          list(np.arange(0.0, 1.0, 0.05)) +
                                                                          list(np.arange(1.05, 2.01, 0.05))])
        avg_equal_1std = self._compute_avg_and_std_over_dict_for_keys(equal_preds,
                                                                      [f'{x:.2f}' for x in
                                                                       list(np.arange(0.5, 1.0, 0.05)) +
                                                                       list(np.arange(1.05, 1.51, 0.05))])
        avg_equal_gt_1st = self._compute_avg_and_std_over_dict_for_keys(equal_preds,
                                                                        [f'{x:.2f}' for x in
                                                                         list(np.arange(0.0, 0.5, 0.05)) +
                                                                         list(np.arange(1.55, 2.01, 0.05))])

        # for k in sorted(top1_scores.keys()):
        #     print(f"{k}: {top1_scores[k]} --> {top1_scores[k] / nb_rows:.2f}")

        # Compute Top1/Top5 scores for each hue shift
        if b_imgnet_1k:
            top1_per_sat = self.in_scorer.compute_top1_score_for_csv(f, b_silent=True)
            ref_top1 = ('ref_top1', top1_per_sat[1.])
            avg_top1_all_gains = self._compute_avg_and_std_over_dict_for_keys(top1_per_sat,
                                                                             [float(f'{x:.2f}') for x in
                                                                              list(np.arange(0.0, 1.0, 0.05)) +
                                                                              list(np.arange(1.05, 2.01, 0.05))])
            avg_top1_1std = self._compute_avg_and_std_over_dict_for_keys(top1_per_sat,
                                                                         [float(f'{x:.2f}') for x in
                                                                          list(np.arange(0.5, 1.0, 0.05)) +
                                                                          list(np.arange(1.05, 1.51, 0.05))])
            avg_top1_gt_1std = self._compute_avg_and_std_over_dict_for_keys(top1_per_sat,
                                                                           [float(f'{x:.2f}') for x in
                                                                            list(np.arange(0.0, 0.5, 0.05)) +
                                                                            list(np.arange(1.55, 2.01, 0.05))])

            top5_per_sat = self.in_scorer.compute_top5_score_for_csv(f_top5, b_silent=True)
            ref_top5 = ('ref_top5', top5_per_sat[1.])
            avg_top5_all_gains = self._compute_avg_and_std_over_dict_for_keys(top5_per_sat,
                                                                             [float(f'{x:.2f}') for x in
                                                                              list(np.arange(0.0, 1.0, 0.05)) +
                                                                              list(np.arange(1.05, 2.01, 0.05))])
            avg_top5_1std = self._compute_avg_and_std_over_dict_for_keys(top5_per_sat,
                                                                         [float(f'{x:.2f}') for x in
                                                                          list(np.arange(0.5, 1.0, 0.05)) +
                                                                          list(np.arange(1.05, 1.51, 0.05))])
            avg_top5_gt_1std = self._compute_avg_and_std_over_dict_for_keys(top5_per_sat,
                                                                            [float(f'{x:.2f}') for x in
                                                                             list(np.arange(0.0, 0.5, 0.05)) +
                                                                             list(np.arange(1.55, 2.01, 0.05))])
        else:
            ref_top1 = ('-', '-')
            avg_top1_all_gains = ('-', '-')
            avg_top1_1std = ('-', '-')
            avg_top1_gt_1std = ('-', '-')

            ref_top5 = ('-', '-')
            avg_top5_all_gains = ('-', '-')
            avg_top5_1std = ('-', '-')
            avg_top5_gt_1std = ('-', '-')

        # Stats on what percentage of those predictions that originally were correct/wrong changed
        if b_imgnet_1k:
            acc_per_hue, overlap_ref_corr_per_col, overlap_ref_wrong_per_col =\
                self.in_scorer.compare_corr_vs_wrong_for_csv(f, b_silent=True)
            avg_overlap_ref_corr_all_gains = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_corr_per_col,
                                                                                         [float(f'{x:.2f}') for x in
                                                                                          list(np.arange(0.0, 1.0, 0.05)) +
                                                                                          list(np.arange(1.05, 2.01, 0.05))])
            avg_overlap_ref_corr_1std = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_corr_per_col,
                                                                                       [float(f'{x:.2f}') for x in
                                                                                        list(np.arange(0.5, 1.0, 0.05)) +
                                                                                        list(np.arange(1.05, 1.51, 0.05))])
            avg_overlap_ref_corr_gt_1std = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_corr_per_col,
                                                                                           [float(f'{x:.2f}') for x in
                                                                                            list(np.arange(0.0, 0.5, 0.05)) +
                                                                                            list(np.arange(1.55, 2.01, 0.05))])
            avg_overlap_ref_wrong_all_gains = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_wrong_per_col,
                                                                                          [float(f'{x:.2f}') for x in
                                                                                           list(np.arange(0.0, 1.0, 0.05)) +
                                                                                           list(np.arange(1.05, 2.01, 0.05))])
            avg_overlap_ref_wrong_1std = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_wrong_per_col,
                                                                                        [float(f'{x:.2f}') for x in
                                                                                         list(np.arange(0.5, 1.0, 0.05)) +
                                                                                         list(np.arange(1.05, 1.51, 0.05))])
            avg_overlap_ref_wrong_gt_1std = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_wrong_per_col,
                                                                                            [float(f'{x:.2f}') for x in
                                                                                             list(np.arange(0.0, 0.5, 0.05)) +
                                                                                             list(np.arange(1.55, 2.01, 0.05))])
        else:
            avg_overlap_ref_corr_all_gains = ('-', '-')
            avg_overlap_ref_corr_1std = ('-', '-')
            avg_overlap_ref_corr_gt_1std = ('-', '-')
            avg_overlap_ref_wrong_all_gains = ('-', '-')
            avg_overlap_ref_wrong_1std = ('-', '-')
            avg_overlap_ref_wrong_gt_1std = ('-', '-')


        # Stats on average distance of prediction from original prediction
        csv_pred_dist = pd.read_csv(f_pred_dist)
        avg_pred_dist_all_gains = self._compute_avg_std_pred_dist_for_hues(csv_pred_dist,
                                                                          [f'{x:.2f}' for x in
                                                                           list(np.arange(0.0, 1.0, 0.05)) +
                                                                           list(np.arange(1.05, 2.01, 0.05))])
        avg_pred_dist_1std = self._compute_avg_std_pred_dist_for_hues(csv_pred_dist,
                                                                      [f'{x:.2f}' for x in
                                                                       list(np.arange(0.5, 1.0, 0.05)) +
                                                                       list(np.arange(1.05, 1.51, 0.05))])
        avg_pred_dist_gt_1std = self._compute_avg_std_pred_dist_for_hues(csv_pred_dist,
                                                                         [f'{x:.2f}' for x in
                                                                          list(np.arange(0.0, 0.5, 0.05)) +
                                                                          list(np.arange(1.55, 2.01, 0.05))])

        return avg_equal_all_gains, avg_equal_1std, avg_equal_gt_1st,\
            ref_top1,\
            avg_top1_all_gains, avg_overlap_ref_corr_all_gains, avg_overlap_ref_wrong_all_gains,\
            avg_top1_1std, avg_overlap_ref_corr_1std, avg_overlap_ref_wrong_1std,\
            avg_top1_gt_1std, avg_overlap_ref_corr_gt_1std, avg_overlap_ref_wrong_gt_1std,\
            avg_pred_dist_all_gains, avg_pred_dist_1std, avg_pred_dist_gt_1std, \
            ref_top5,\
            avg_top5_all_gains, avg_top5_1std, avg_top5_gt_1std

    def analyze_hue_sat_results(self, b_imgnet_1k: bool, f: str):
        """
        Analyze results of hue and saturation experiments.

        :param b_imgnet_1k: compute Top1/Top5 stats for model, or not?
        :param f: path to the CSV file containing the hue experiment results; the saturation experiment results file is\
        expected to be the same as the hue file, with '_hue.csv' replaced by '_saturation.csv'
        :return:
        """
        # --------------------------------------------------
        # First, do all hue related analyses
        # --------------------------------------------------
        csv_hue_top1 = pd.read_csv(f)
        # f_hue_top5 = f[:-4] + '_top5.csv'
        f_hue_pred_dist = f[:-4] + '_pred_dist.csv'
        nb_hue_rows = csv_hue_top1.shape[0]
        ref_hue = csv_hue_top1["0"]
        hue_shifts = list(csv_hue_top1.columns)[1:]

        # Number of classifications identical to ref_hue, in pc., per hue shift
        # At the same time:
        # -Compute average/std of identical predictions over all non-0 hue shifts
        # -Compute average/std of identical predictions over all hue shifts d with -30 <= d <= 30, d != 0
        equal_preds_hue = {}
        for h in hue_shifts:
            target = csv_hue_top1[h]
            equal = 0
            for t in zip(ref_hue, target):
                if t[0] == t[1]:
                    equal += 1
            equal_preds_hue[int(h)] = equal/nb_hue_rows

        avg_equal_all_hues = self._compute_avg_and_std_over_dict_for_keys(equal_preds_hue, list(range(10, 360, 10)))
        avg_equal_max_30 = self._compute_avg_and_std_over_dict_for_keys(equal_preds_hue, [10, 20, 30, 330, 340, 350])
        # avg_equal_not_max_30 = self._compute_avg_and_std_over_dict_for_keys(equal_preds_hue, list(range(40, 330, 10)))

        # Compute Top1/Top5 scores for each hue shift
        if b_imgnet_1k:
            top1_per_hue = self.in_scorer.compute_top1_score_for_csv(f, b_silent=True)
            ref_top1 = ('ref_top1', top1_per_hue[0])
            avg_top1_all_hues = self._compute_avg_and_std_over_dict_for_keys(top1_per_hue, list(range(10, 360, 10)))
            avg_top1_max_30 = self._compute_avg_and_std_over_dict_for_keys(top1_per_hue, [10, 20, 30, 330, 340, 350])
            # avg_top1_not_max_30 = self._compute_avg_and_std_over_dict_for_keys(top1_per_hue, list(range(40, 330, 10)))

            # top5_per_hue = self.in_scorer.compute_top5_score_for_csv(f_hue_top5, b_silent=True)
            # ref_top5 = ('ref_top5', top5_per_hue[0])
            # avg_top5_all_hues = self._compute_avg_and_std_over_dict_for_keys(top5_per_hue, list(range(10, 360, 10)))
            # avg_top5_max_30 = self._compute_avg_and_std_over_dict_for_keys(top5_per_hue, [10, 20, 30, 330, 340, 350])
            # avg_top5_not_max_30 = self._compute_avg_and_std_over_dict_for_keys(top5_per_hue, list(range(40, 330, 10)))
        else:
            ref_top1 = ('-', '-')
            avg_top1_all_hues = ('-', '-')
            avg_top1_max_30 = ('-', '-')
            # avg_top1_not_max_30 = ('-', '-')

            # ref_top5 = ('-', '-')
            # avg_top5_all_hues = ('-', '-')
            # avg_top5_max_30 = ('-', '-')
            # avg_top5_not_max_30 = ('-', '-')

        # Stats on what percentage of those predictions that originally were correct/wrong changed
        if b_imgnet_1k:
            acc_per_hue, overlap_ref_corr_per_col, overlap_ref_wrong_per_col =\
                self.in_scorer.compare_corr_vs_wrong_for_csv(f, b_silent=True)
            avg_overlap_ref_corr_all_hues = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_corr_per_col,
                                                                                         list(range(10, 360, 10)))
            avg_overlap_ref_corr_max_30 = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_corr_per_col,
                                                                                       [10, 20, 30, 330, 340, 350])
            # avg_overlap_ref_corr_not_max_30 = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_corr_per_col,
            #                                                                                list(range(40, 330, 10)))
            avg_overlap_ref_wrong_all_hues = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_wrong_per_col,
                                                                                          list(range(10, 360, 10)))
            avg_overlap_ref_wrong_max_30 = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_wrong_per_col,
                                                                                        [10, 20, 30, 330, 340, 350])
            # avg_overlap_ref_wrong_not_max_30 = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_wrong_per_col,
            #                                                                                 list(range(40, 330, 10)))
        else:
            avg_overlap_ref_corr_all_hues = ('-', '-')
            avg_overlap_ref_corr_max_30 = ('-', '-')
            # avg_overlap_ref_corr_not_max_30 = ('-', '-')
            avg_overlap_ref_wrong_all_hues = ('-', '-')
            avg_overlap_ref_wrong_max_30 = ('-', '-')
            # avg_overlap_ref_wrong_not_max_30 = ('-', '-')


        # Stats on average distance of prediction from original prediction
        csv_pred_dist = pd.read_csv(f_hue_pred_dist)
        avg_pred_dist_all_hues = self._compute_avg_std_pred_dist_for_hues(csv_pred_dist,
                                                                              list(range(10, 360, 10)))
        avg_pred_dist_max_30 = self._compute_avg_std_pred_dist_for_hues(csv_pred_dist,
                                                                            [10, 20, 30, 330, 340, 350])
        # avg_pred_dist_not_max_30 = self._compute_avg_std_pred_dist_for_hues(csv_pred_dist,
        #                                                                         list(range(40, 330, 10)))

        # --------------------------------------------------
        # Second, do all saturation related analyses
        # --------------------------------------------------
        f_sat = f.replace('_hue.csv', '_saturation.csv')
        csv_sat_top1 = pd.read_csv(f_sat)
        # f_sat_top5 = f_sat[:-4] + '_top5.csv'
        # csv_sat_top5 = pd.read_csv(f_sat_top5)
        f_sat_pred_dist = f_sat[:-4] + '_pred_dist.csv'
        nb_sat_row = csv_sat_top1.shape[0]

        ref = csv_sat_top1["1.00"]
        sat_shifts = list(csv_sat_top1.columns)[1:]

        # Number of classifications identical to ref, in pc., per saturation shift
        # At the same time:
        # -Compute average/std of identical predictions over saturation shifts with g \in [0.5, 1.5]\1.
        # -Compute average/std of identical predictions over saturation shifts with g \in [0, 0.5] U [1.5, 2].
        equal_preds_sat = {}
        for g in sat_shifts:
            target = csv_sat_top1[g]
            equal = 0
            for t in zip(ref, target):
                if t[0] == t[1]:
                    equal += 1
            equal_preds_sat[g] = equal/nb_sat_row

        avg_equal_all_gains = self._compute_avg_and_std_over_dict_for_keys(equal_preds_sat,
                                                                          [f'{x:.2f}' for x in
                                                                          list(np.arange(0.0, 1.0, 0.05)) +
                                                                          list(np.arange(1.05, 2.01, 0.05))])
        avg_equal_1std = self._compute_avg_and_std_over_dict_for_keys(equal_preds_sat,
                                                                      [f'{x:.2f}' for x in
                                                                       list(np.arange(0.5, 1.0, 0.05)) +
                                                                       list(np.arange(1.05, 1.51, 0.05))])
        # avg_equal_gt_1st = self._compute_avg_and_std_over_dict_for_keys(equal_preds_sat,
        #                                                                 [f'{x:.2f}' for x in
        #                                                                  list(np.arange(0.0, 0.5, 0.05)) +
        #                                                                  list(np.arange(1.55, 2.01, 0.05))])

        # Compute Top1/Top5 scores for each hue shift
        if b_imgnet_1k:
            top1_per_sat = self.in_scorer.compute_top1_score_for_csv(f_sat, b_silent=True)
            # ref_top1 = ('ref_top1', top1_per_sat[1.])
            avg_top1_all_gains = self._compute_avg_and_std_over_dict_for_keys(top1_per_sat,
                                                                             [float(f'{x:.2f}') for x in
                                                                              list(np.arange(0.0, 1.0, 0.05)) +
                                                                              list(np.arange(1.05, 2.01, 0.05))])
            avg_top1_1std = self._compute_avg_and_std_over_dict_for_keys(top1_per_sat,
                                                                         [float(f'{x:.2f}') for x in
                                                                          list(np.arange(0.5, 1.0, 0.05)) +
                                                                          list(np.arange(1.05, 1.51, 0.05))])
            # avg_top1_gt_1std = self._compute_avg_and_std_over_dict_for_keys(top1_per_sat,
            #                                                                [float(f'{x:.2f}') for x in
            #                                                                 list(np.arange(0.0, 0.5, 0.05)) +
            #                                                                 list(np.arange(1.55, 2.01, 0.05))])

            # top5_per_sat = self.in_scorer.compute_top5_score_for_csv(f_sat_top5, b_silent=True)
            # ref_top5 = ('ref_top5', top5_per_sat[1.])
            # avg_top5_all_gains = self._compute_avg_and_std_over_dict_for_keys(top5_per_sat,
            #                                                                  [float(f'{x:.2f}') for x in
            #                                                                   list(np.arange(0.0, 1.0, 0.05)) +
            #                                                                   list(np.arange(1.05, 2.01, 0.05))])
            # avg_top5_1std = self._compute_avg_and_std_over_dict_for_keys(top5_per_sat,
            #                                                              [float(f'{x:.2f}') for x in
            #                                                               list(np.arange(0.5, 1.0, 0.05)) +
            #                                                               list(np.arange(1.05, 1.51, 0.05))])
            # avg_top5_gt_1std = self._compute_avg_and_std_over_dict_for_keys(top5_per_sat,
            #                                                                 [float(f'{x:.2f}') for x in
            #                                                                  list(np.arange(0.0, 0.5, 0.05)) +
            #                                                                  list(np.arange(1.55, 2.01, 0.05))])
        else:
            ref_top1 = ('-', '-')
            avg_top1_all_gains = ('-', '-')
            avg_top1_1std = ('-', '-')
            # avg_top1_gt_1std = ('-', '-')

            # ref_top5 = ('-', '-')
            # avg_top5_all_gains = ('-', '-')
            # avg_top5_1std = ('-', '-')
            # avg_top5_gt_1std = ('-', '-')

        # Stats on what percentage of those predictions that originally were correct/wrong changed
        if b_imgnet_1k:
            acc_per_hue, overlap_ref_corr_per_col, overlap_ref_wrong_per_col =\
                self.in_scorer.compare_corr_vs_wrong_for_csv(f_sat, b_silent=True)
            avg_overlap_ref_corr_all_gains = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_corr_per_col,
                                                                                         [float(f'{x:.2f}') for x in
                                                                                          list(np.arange(0.0, 1.0, 0.05)) +
                                                                                          list(np.arange(1.05, 2.01, 0.05))])
            avg_overlap_ref_corr_1std = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_corr_per_col,
                                                                                       [float(f'{x:.2f}') for x in
                                                                                        list(np.arange(0.5, 1.0, 0.05)) +
                                                                                        list(np.arange(1.05, 1.51, 0.05))])
            # avg_overlap_ref_corr_gt_1std = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_corr_per_col,
            #                                                                                [float(f'{x:.2f}') for x in
            #                                                                                 list(np.arange(0.0, 0.5, 0.05)) +
            #                                                                                 list(np.arange(1.55, 2.01, 0.05))])
            avg_overlap_ref_wrong_all_gains = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_wrong_per_col,
                                                                                          [float(f'{x:.2f}') for x in
                                                                                           list(np.arange(0.0, 1.0, 0.05)) +
                                                                                           list(np.arange(1.05, 2.01, 0.05))])
            avg_overlap_ref_wrong_1std = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_wrong_per_col,
                                                                                        [float(f'{x:.2f}') for x in
                                                                                         list(np.arange(0.5, 1.0, 0.05)) +
                                                                                         list(np.arange(1.05, 1.51, 0.05))])
            # avg_overlap_ref_wrong_gt_1std = self._compute_avg_and_std_over_dict_for_keys(overlap_ref_wrong_per_col,
            #                                                                                 [float(f'{x:.2f}') for x in
            #                                                                                  list(np.arange(0.0, 0.5, 0.05)) +
            #                                                                                  list(np.arange(1.55, 2.01, 0.05))])
        else:
            avg_overlap_ref_corr_all_gains = ('-', '-')
            avg_overlap_ref_corr_1std = ('-', '-')
            # avg_overlap_ref_corr_gt_1std = ('-', '-')
            avg_overlap_ref_wrong_all_gains = ('-', '-')
            avg_overlap_ref_wrong_1std = ('-', '-')
            # avg_overlap_ref_wrong_gt_1std = ('-', '-')


        # Stats on average distance of prediction from original prediction
        csv_pred_dist = pd.read_csv(f_sat_pred_dist)
        avg_pred_dist_all_gains = self._compute_avg_std_pred_dist_for_hues(csv_pred_dist,
                                                                          [f'{x:.2f}' for x in
                                                                           list(np.arange(0.0, 1.0, 0.05)) +
                                                                           list(np.arange(1.05, 2.01, 0.05))])
        avg_pred_dist_1std = self._compute_avg_std_pred_dist_for_hues(csv_pred_dist,
                                                                      [f'{x:.2f}' for x in
                                                                       list(np.arange(0.5, 1.0, 0.05)) +
                                                                       list(np.arange(1.05, 1.51, 0.05))])
        # avg_pred_dist_gt_1std = self._compute_avg_std_pred_dist_for_hues(csv_pred_dist,
        #                                                                  [f'{x:.2f}' for x in
        #                                                                   list(np.arange(0.0, 0.5, 0.05)) +
        #                                                                   list(np.arange(1.55, 2.01, 0.05))])

        return avg_equal_all_hues, avg_equal_max_30,\
            avg_equal_all_gains, avg_equal_1std,\
            ref_top1,\
            avg_top1_all_hues, avg_overlap_ref_corr_all_hues, avg_overlap_ref_wrong_all_hues,\
            avg_top1_all_gains, avg_overlap_ref_corr_all_gains, avg_overlap_ref_wrong_all_gains,\
            avg_top1_max_30, avg_overlap_ref_corr_max_30, avg_overlap_ref_wrong_max_30,\
            avg_top1_1std, avg_overlap_ref_corr_1std, avg_overlap_ref_wrong_1std,\
            avg_pred_dist_all_hues, avg_pred_dist_max_30,\
            avg_pred_dist_all_gains, avg_pred_dist_1std

    @staticmethod
    def _compute_avg_and_std_over_dict_for_keys(d: dict, keys):
        """
        Computer average and standard deviation values over whatever is stored in the provided dictionary,
        using "keys" as keys to select those values to computer the stats over.

        :param d:
        :param keys:
        :return:
        """
        vals = []
        for k in keys:
            vals.append(d[k])
        avg = np.average(vals)
        std = np.std(vals)

        return avg, std

    @staticmethod
    def _compute_avg_and_std_over_avgs_and_stds(avgs, stds, sample_sizes, hues):
        """
        Computer average of averages and average of standard deviations

        :param avgs:
        :param stds:
        :param sample_sizes:
        :param hues:
        :return:
        """
        vals_avg, vals_std = [], []
        sum_sample_sizes = 0
        for h in hues:
            vals_avg.append(avgs[h]*sample_sizes[h])
            vals_std.append((sample_sizes[h]-1) * (stds[h]**2))
            sum_sample_sizes += sample_sizes[h]
        avg = np.sum(vals_avg)/sum_sample_sizes
        std = np.sqrt(np.sum(vals_std)/(sum_sample_sizes - len(vals_std)))

        return avg, std

    @staticmethod
    def _compute_avg_std_pred_dist_for_hues(df_pred_dist, cols):
        """
        Computer average and standard deviation over prediction distances for provided column names.

        :param df_pred_dist: DataFrame containing prediction distances per hue
        :param cols: the columns over which to compute the average prediction distance
        :return:
        """
        vals = df_pred_dist[[str(c) for c in cols]].values
        vals = vals[vals.nonzero()]
        avg = np.average(vals)
        std = np.std(vals)

        return avg, std


if __name__ == '__main__':
    csv_sat = os.path.join(Config.DIR_LOGS, 'densenet161_val_stability_saturation.csv')
    csv_hue = os.path.join(Config.DIR_LOGS, 'emonet_val_stability_hue.csv')

    # ColorStabilityAnalysis.analyze_saturation(csv_sat, model_name='AlexNet')
    # ColorStabilityAnalysis.analyze_hue(csv_hue, model_name='EmoNet')

    creator = CreateLaTeXTables()
    if False:
        creator.create_hue_results_table([
            (True, 'AlexNet', os.path.join(Config.DIR_LOGS, 'alexnet_val_stability_hue.csv')),
            # (True, 'AlexNet SIN', os.path.join(Config.DIR_LOGS, 'alexnet_sin_stability_hue.csv')),
            # (False, 'AlexNet P365', os.path.join(Config.DIR_LOGS, 'alexnet_places365_stability_hue.csv')),
            # (False, 'EmoNet', os.path.join(Config.DIR_LOGS, 'emonet_val_stability_hue.csv')),
            (True, 'AlexNet class.+h', os.path.join(Config.DIR_LOGS, 'alexnet_classifier_tl_hue_val_stability_hue.csv')),
            (True, 'Alexnet+h', os.path.join(Config.DIR_LOGS, 'alexnet_full_tl_hue_val_stability_hue.csv')),
            (True, 'AlexNet+hs', os.path.join(Config.DIR_LOGS, 'alexnet_full_tl_hue+sat_val_stability_hue.csv')),
            (True, 'VGG16', os.path.join(Config.DIR_LOGS, 'vgg16_val_stability_hue.csv')),
            # (True, 'VGG16 SIN', os.path.join(Config.DIR_LOGS, 'vgg16_sin_stability_hue.csv')),
            (True, 'VGG16+h', os.path.join(Config.DIR_LOGS, 'vgg16_full_tl_hue_val_stability_hue.csv')),
            # (True, 'vgg19', os.path.join(Config.DIR_LOGS, 'vgg19_val_stability_hue.csv')),
            (True, 'ResNet18', os.path.join(Config.DIR_LOGS, 'resnet18_val_stability_hue.csv')),
            # (False, 'ResNet18 P365', os.path.join(Config.DIR_LOGS, 'resnet18_places365_stability_hue.csv')),
            (True, 'ResNet18+h', os.path.join(Config.DIR_LOGS, 'resnet18_full_tl_hue_val_stability_hue.csv')),
            (True, 'ResNet18+hs', os.path.join(Config.DIR_LOGS, 'resnet18_full_tl_hue+sat_val_stability_hue.csv')),
            # (True, 'ResNet50', os.path.join(Config.DIR_LOGS, 'resnet50_val_stability_hue.csv')),
            # (True, 'ResNet50 SIN', os.path.join(Config.DIR_LOGS, 'resnet50_sin_stability_hue.csv')),
            # (True, 'DenseNet161', os.path.join(Config.DIR_LOGS, 'densenet161_val_stability_hue.csv')),
            # (False, 'DenseNet161 P365', os.path.join(Config.DIR_LOGS, 'densenet161_places365_stability_hue.csv'))
        ],
        label="tb:trained_model_stats")

    if False:
        creator.create_sat_results_table([
            (True, 'AlexNet', os.path.join(Config.DIR_LOGS, 'alexnet_val_stability_saturation.csv')),
            (True, 'AlexNet SIN', os.path.join(Config.DIR_LOGS, 'alexnet_sin_stability_saturation.csv')),
            (False, 'AlexNet P365', os.path.join(Config.DIR_LOGS, 'alexnet_places365_stability_saturation.csv')),
            (False, 'EmoNet', os.path.join(Config.DIR_LOGS, 'emonet_val_stability_saturation.csv')),
            # (True, 'AlexNet class.+h', os.path.join(Config.DIR_LOGS, 'alexnet_classifier_tl_saturation_val_stability_saturation.csv')),
            # (True, 'Alexnet+h', os.path.join(Config.DIR_LOGS, 'alexnet_full_tl_saturation_val_stability_saturation.csv')),
            # (True, 'AlexNet+hs', os.path.join(Config.DIR_LOGS, 'alexnet_full_tl_saturation+sat_val_stability_saturation.csv')),
            (True, 'VGG16', os.path.join(Config.DIR_LOGS, 'vgg16_val_stability_saturation.csv')),
            (True, 'VGG16 SIN', os.path.join(Config.DIR_LOGS, 'vgg16_sin_stability_saturation.csv')),
            # (True, 'VGG16+h', os.path.join(Config.DIR_LOGS, 'vgg16_full_tl_saturation_val_stability_saturation.csv')),
            # (True, 'vgg19', os.path.join(Config.DIR_LOGS, 'vgg19_val_stability_saturation.csv')),
            (True, 'ResNet18', os.path.join(Config.DIR_LOGS, 'resnet18_val_stability_saturation.csv')),
            (False, 'ResNet18 P365', os.path.join(Config.DIR_LOGS, 'resnet18_places365_stability_saturation.csv')),
            # (True, 'ResNet18+h', os.path.join(Config.DIR_LOGS, 'resnet18_full_tl_saturation_val_stability_saturation.csv')),
            # (True, 'ResNet18+hs', os.path.join(Config.DIR_LOGS, 'resnet18_full_tl_hue+sat_val_stability_saturation.csv')),
            (True, 'ResNet50', os.path.join(Config.DIR_LOGS, 'resnet50_val_stability_saturation.csv')),
            (True, 'ResNet50 SIN', os.path.join(Config.DIR_LOGS, 'resnet50_sin_stability_saturation.csv')),
            # (True, 'DenseNet161', os.path.join(Config.DIR_LOGS, 'densenet161_val_stability_saturation.csv')),
            (False, 'DenseNet161 P365', os.path.join(Config.DIR_LOGS, 'densenet161_places365_stability_saturation.csv'))
        ],
        label="tb:retrained_model_stats")

    if True:
        creator.create_hue_sat_results_table([
            (True, 'AlexNet', os.path.join(Config.DIR_LOGS, 'alexnet_val_stability_hue.csv')),
            # (True, 'AlexNet SIN', os.path.join(Config.DIR_LOGS, 'alexnet_sin_stability_hue.csv')),
            # (False, 'AlexNet P365', os.path.join(Config.DIR_LOGS, 'alexnet_places365_stability_hue.csv')),
            # (False, 'EmoNet', os.path.join(Config.DIR_LOGS, 'emonet_val_stability_hue.csv')),
            (True, 'AlexNet class.+h', os.path.join(Config.DIR_LOGS, 'alexnet_classifier_tl_hue_val_stability_hue.csv')),
            (True, 'AlexNet class.+hs', os.path.join(Config.DIR_LOGS, 'alexnet_classifier_tl_hue+sat_val_stability_hue.csv')),
            (True, 'Alexnet +h', os.path.join(Config.DIR_LOGS, 'alexnet_full_tl_hue_val_stability_hue.csv')),
            (True, 'AlexNet +hs', os.path.join(Config.DIR_LOGS, 'alexnet_full_tl_hue+sat_val_stability_hue.csv')),
            (True, 'VGG16', os.path.join(Config.DIR_LOGS, 'vgg16_val_stability_hue.csv')),
            # (True, 'VGG16 SIN', os.path.join(Config.DIR_LOGS, 'vgg16_sin_stability_hue.csv')),
            (True, 'VGG16 +h', os.path.join(Config.DIR_LOGS, 'vgg16_full_tl_hue_val_stability_hue.csv')),
            (True, 'VGG16 +hs', os.path.join(Config.DIR_LOGS, 'vgg16_full_tl_hue+sat_val_stability_hue.csv')),
            # (True, 'vgg19', os.path.join(Config.DIR_LOGS, 'vgg19_val_stability_hue.csv')),
            (True, 'ResNet18', os.path.join(Config.DIR_LOGS, 'resnet18_val_stability_hue.csv')),
            # (False, 'ResNet18 P365', os.path.join(Config.DIR_LOGS, 'resnet18_places365_stability_hue.csv')),
            (True, 'ResNet18 +h', os.path.join(Config.DIR_LOGS, 'resnet18_full_tl_hue_val_stability_hue.csv')),
            (True, 'ResNet18 +hs', os.path.join(Config.DIR_LOGS, 'resnet18_full_tl_hue+sat_val_stability_hue.csv')),
            # (True, 'ResNet50', os.path.join(Config.DIR_LOGS, 'resnet50_val_stability_hue.csv')),
            # (True, 'ResNet50 SIN', os.path.join(Config.DIR_LOGS, 'resnet50_sin_stability_hue.csv')),
            # (True, 'DenseNet161', os.path.join(Config.DIR_LOGS, 'densenet161_val_stability_hue.csv')),
            # (False, 'DenseNet161 P365', os.path.join(Config.DIR_LOGS, 'densenet161_places365_stability_hue.csv'))
        ],
        label="tb:retrained_model_hue_sat_stats")
