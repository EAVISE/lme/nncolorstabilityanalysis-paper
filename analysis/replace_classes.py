"""
Replace prediction index with ImageNet class for given CSV. Allows to visually check to what extent\\
the label changes induced by hue/saturation changes are "reasonable" or completely unrelated semantically.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import pandas as pd

from imagenet_scoring.imagenet_classes import ImageNetClasses
from config import Config


class ReplaceClasses:
    @staticmethod
    def process_csv(file_csv: str):
        """

        :param file_csv: CSV file containing label prediction
        :return:
        """
        inc = ImageNetClasses()

        csv = pd.read_csv(file_csv)
        for i, row in csv.iterrows():
            if (i+1) % 50 == 0:
                print(f"\rAt row {i+1} of {len(csv)} [{100*(i+1)/len(csv):5.1f}%]...", end='', flush=True)
            for j in range(1, len(row)):
                csv.at[i, csv.columns[j]] = inc.get_class_name(int(row[j]))
        print(f"\rAt row {len(csv)} of {len(csv)} [100.0%]...")

        csv.to_csv(file_csv.replace('.csv', '_classnames.csv'), index=False)


if __name__ == '__main__':
    ReplaceClasses.process_csv(os.path.join(Config.DIR_LOGS, 'vgg16_stability_hue.csv'))
