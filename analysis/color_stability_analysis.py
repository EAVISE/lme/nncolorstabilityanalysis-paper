"""
Create plots comparing model performance.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import numpy as np
from matplotlib import pyplot as plt
import os
import pandas as pd

from config import Config
import seaborn as sb


class ColorStabilityAnalysis:
    @staticmethod
    def analyze_saturation(f: str, model_name=None):
        """
        Analyze results of saturation experiment, generated using emonet_stability.py.

        :param f: path to the CSV file containing the saturation experiment results.
        :param model_name:
        :return:
        """
        if model_name is None:
            model_name = 'Not specified'
        csv = pd.read_csv(f)

        nb_rows = csv.shape[0]

        ref = csv["1.00"]
        keys = list(csv.columns)[1:]

        # Number of classifications identical to ref, in pc., per hue shift
        scores = {}
        for k in keys:
            target = csv[k]
            equal = 0
            for t in zip(ref, target):
                if t[0] == t[1]:
                    equal += 1
            scores[k] = equal

        for k in sorted(scores.keys()):
            print(f"{k}: {scores[k]} --> {scores[k]/nb_rows:.2f}")

        plt_vals = [scores[k]/nb_rows for k in sorted(scores.keys())]

        fig = plt.figure()
        plt.title(f'Model: {model_name}\nFraction of identical classification to ref (g=1.00) per color gain')
        plt.bar(keys, plt_vals)
        plt.xlabel('Color gain')
        plt.ylabel('Identical (fraction)')
        plt.show()

        # Confusion matrix over all color scales
        conf_matrix = np.zeros((20, 20))
        for row_idx, row in csv.iterrows():
            ref = row['1.00']
            for k in keys:
                if k == '1.00':
                    continue
                conf_matrix[ref, row[k]] += 1

        # Row-normalize
        conf_matrix_norm = conf_matrix.copy()
        for row_idx in range(20):
            row_sum = sum(conf_matrix_norm[row_idx, :])
            conf_matrix_norm[row_idx, :] /= row_sum

        fig = plt.figure()
        plt.title('Confusion matrix over all color gains')
        ax = sb.heatmap(conf_matrix_norm)
        plt.xlabel('Classified as')
        plt.ylabel('Reference')
        plt.tight_layout()
        plt.show()

        # Per color gain
        ref = csv['1.00']
        for k in keys:
            if k == '1.00':
                continue
            target = csv[k]

            conf_matrix = np.zeros((20, 20))
            for t in zip(ref, target):
                conf_matrix[t[0], t[1]] += 1

            # Row-normalize
            conf_matrix_norm = conf_matrix.copy()
            for row_idx in range(20):
                row_sum = sum(conf_matrix_norm[row_idx, :])
                conf_matrix_norm[row_idx, :] /= row_sum

            fig = plt.figure()
            plt.title(f'Color scale: {k}')
            ax = sb.heatmap(conf_matrix_norm)
            plt.xlabel('Classified as')
            plt.ylabel('Reference')
            plt.tight_layout()
            plt.show()

    @staticmethod
    def analyze_hue(f: str, model_name=None):
        """
        Analyze results of hue experiment, generated using emonet_stability.py.

        :param f: path to the CSV file containing the hue experiment results.
        :param model_name:
        :return:
        """
        if model_name is None:
            model_name = 'Not specified'
        csv = pd.read_csv(f)

        nb_rows = csv.shape[0]

        ref = csv["0"]
        keys = list(csv.columns)[1:]

        # Number of classifications identical to ref, in pc., per hue shift
        scores = {}
        for k in keys:
            target = csv[k]
            equal = 0
            for t in zip(ref, target):
                if t[0] == t[1]:
                    equal += 1
            scores[int(k)] = equal

        for k in sorted(scores.keys()):
            print(f"{k}: {scores[k]} --> {scores[k] / nb_rows:.2f}")

        plt_vals = [scores[k] / nb_rows for k in sorted(scores.keys())]

        fig = plt.figure()
        plt.title(f'Model: {model_name}\nFraction of identical classification to reference (d=0) per hue shift')
        plt.bar(keys, plt_vals)
        plt.xlabel('Hue shift (degrees)')
        plt.ylabel('Identical (fraction)')
        plt.show()

        # Confusion matrix over all color scales
        conf_matrix = np.zeros((20, 20))
        for row_idx, row in csv.iterrows():
            ref = row['0']
            for k in keys:
                if k == '0':
                    continue
                conf_matrix[ref, row[k]] += 1

        # Row-normalize
        conf_matrix_norm = conf_matrix.copy()
        for row_idx in range(20):
            row_sum = sum(conf_matrix_norm[row_idx, :])
            conf_matrix_norm[row_idx, :] /= row_sum

        fig = plt.figure()
        plt.title('Confusion matrix over all hue shifts')
        ax = sb.heatmap(conf_matrix_norm)
        plt.xlabel('Classified as')
        plt.ylabel('Reference')
        plt.tight_layout()
        plt.show()

        # Per hue shift
        ref = csv['0']
        for k in keys:
            if k == '0':
                continue
            target = csv[k]

            conf_matrix = np.zeros((20, 20))
            for t in zip(ref, target):
                conf_matrix[t[0], t[1]] += 1

            # Row-normalize
            conf_matrix_norm = conf_matrix.copy()
            for row_idx in range(20):
                row_sum = sum(conf_matrix_norm[row_idx, :])
                conf_matrix_norm[row_idx, :] /= row_sum

            fig = plt.figure()
            plt.title(f'Hue shift: {k}')
            ax = sb.heatmap(conf_matrix_norm)
            plt.xlabel('Classified as')
            plt.ylabel('Reference')
            plt.tight_layout()
            plt.show()

    @classmethod
    def plot_together_hue(cls, files_hue: list, model_names=None, gaps_after: set=None):
        """
        Make a combined bar plot from multiple result files.

        :param files_hue:
        :param model_names: list containing the model names. Default = None, in which case the names will be taken from the\
        filenames.
        :param gaps_after: introduce gaps between bars after bars corresponding to the indices in this list; is None, no gaps\
        are introduced.
        :return:
        """
        hue_keys, hue_datapoints = cls._get_hue_datapoints(files_hue)

        # Adapted from https://matplotlib.org/stable/gallery/units/bar_unit_demo.html?highlight=barchart
        N = len(files_hue)
        fig, ax = plt.subplots(figsize=(16, 9))
        # plt.rcParams.update({'font.size': 16})
        plt.title("Fraction of identical classification to reference (d=0) per hue shift", fontsize=20)

        ind = np.arange(len(hue_keys))  # the x locations for the groups
        width = 0.75/N  # the width of the bars
        if gaps_after is None:
            gaps_after = set()
        gap_width = 0.06
        add_offset = 0.
        left_edge = -((N-1+(gap_width*len(gaps_after)))/2)*width
        for i, e in enumerate(hue_datapoints):
            if i in gaps_after:
                add_offset += gap_width
            ax.bar(ind + left_edge + i*width + add_offset, e, width, bottom=0, label=model_names[i])

        plt.xticks(ind, labels=hue_keys, rotation=60, fontsize=12)
        plt.yticks(fontsize=12)
        plt.ylim(0., 1.05)
        plt.xlabel('Hue shift (degrees)', fontsize=16)
        plt.ylabel('Identical (fraction)', fontsize=16)

        ax.legend(fontsize=16, loc='best')
        # ax.yaxis.set_units(inch)
        # ax.autoscale_view()
        plt.margins(0.05, 0.2)

        plt.tight_layout()
        plt.show()

    @classmethod
    def plot_together_saturation(cls, files_saturation: list, model_names=None, gaps_after: set=None):
        """
        Make a combined bar plot from multiple result files.

        :param files_saturation: list of data files to process.
        :param model_names: list containing the model names. Default = None, in which case the names will be taken from the\
        filenames.
        :param gaps_after: introduce gaps between bars after bars corresponding to the indices in this list; is None, no gaps\
        are introduced.
        :return:
        """
        sat_keys, sat_datapoints = cls._get_saturation_datapoints(files_saturation)

        # Adapted from https://matplotlib.org/stable/gallery/units/bar_unit_demo.html?highlight=barchart
        N = len(files_saturation)
        fig, ax = plt.subplots(figsize=(16, 9))
        # plt.figure()
        plt.title("Fraction of identical classification to reference (g=1.00) per saturation shift", fontsize=24)

        ind = np.arange(len(sat_keys))  # the x locations for the groups
        width = 0.80/N  # the width of the bars
        if gaps_after is None:
            gaps_after = set()
        gap_width = 0.06
        add_offset = 0.
        left_edge = -((N-1+(gap_width*len(gaps_after)))/2)*width
        for i, e in enumerate(sat_datapoints):
            if i in gaps_after:
                add_offset += gap_width
            ax.bar(ind + left_edge + i*width + add_offset, e, width, bottom=0, label=model_names[i])

        plt.xticks(ind, labels=sat_keys, rotation=60, fontsize=14)
        plt.yticks(fontsize=14)
        plt.ylim(0., 1.05)
        plt.xlabel('Saturation (PIL.ImageEnhance.Enhance parameter)', fontsize=18)
        plt.ylabel('Identical (fraction)', fontsize=18)

        ax.legend(fontsize=20)
        # ax.yaxis.set_units(inch)
        # ax.autoscale_view()
        plt.margins(0.05, 0.2)

        plt.tight_layout()
        plt.show()

    @classmethod
    def plot_together_hue_and_sat(cls, files_hue: list, model_names=None, gaps_after: set=None, out_file=None):
        """
        Make a combined bar plot from multiple result files, plotting both hue and saturation evolution of model stats.

        :param files_hue: the saturation files are expected to be able to be obtained by replacing "_hue.csv" with\
        "_saturation.csv".
        :param model_names: list containing the model names. Default = None, in which case the names will be taken from the\
        filenames.
        :param gaps_after: introduce gaps between bars after bars corresponding to the indices in this list; is None, no gaps\
        are introduced.
        :param out_file: save plot to this file; if None, will show() instead.
        :return:
        """
        hue_keys, hue_datapoints = cls._get_hue_datapoints(files_hue=files_hue)
        sat_keys, sat_datapoints = cls._get_saturation_datapoints(
            files_saturation=[x.replace('_hue.csv', '_saturation.csv') for x in files_hue])

        N = len(files_hue)
        width = 0.80/N  # the width of the bars
        tick_size = 14
        label_size = 18
        title_size = 24
        # ############################################################
        # Hue plot
        # ############################################################
        fig, axes = plt.subplots(2, 1, figsize=(16, 12), dpi=300)
        ax_hue, ax_sat = axes[0], axes[1]

        ax_hue.set_title("Fraction of identical classification to reference (d=0) per hue shift", fontsize=title_size)
        ind_hue = np.arange(len(hue_keys))  # the x locations for the groups
        if gaps_after is None:
            gaps_after = set()
        gap_width = 0.06
        add_offset = 0.
        left_edge = -((N-1+(gap_width*len(gaps_after)))/2)*width
        for i, e in enumerate(hue_datapoints):
            if i in gaps_after:
                add_offset += gap_width
            ax_hue.bar(ind_hue + left_edge + i*width + add_offset, e, width, bottom=0, label=model_names[i])

        ax_hue.set_xticks(ind_hue, labels=hue_keys, rotation=60, fontsize=tick_size)
        ax_hue.set_yticks(np.arange(0., 1.05, 0.2), fontsize=title_size)
        ax_hue.set_ylim(.3, 1.05)
        ax_hue.set_xlabel('Hue shift (degrees)', fontsize=label_size)
        ax_hue.set_ylabel('Identical (fraction)', fontsize=label_size)

        ax_hue.legend(fontsize=label_size, loc='best')

        # ############################################################
        # Saturation plot
        # ############################################################
        ax_sat.set_title("Fraction of identical classification to reference (g=1.00) per saturation shift", fontsize=title_size)

        ind_sat = np.arange(len(sat_keys))  # the x locations for the groups
        width = 0.80 / N  # the width of the bars
        if gaps_after is None:
            gaps_after = set()
        gap_width = 0.06
        add_offset = 0.
        left_edge = -((N - 1 + (gap_width * len(gaps_after))) / 2) * width
        for i, e in enumerate(sat_datapoints):
            if i in gaps_after:
                add_offset += gap_width
            ax_sat.bar(ind_sat + left_edge + i * width + add_offset, e, width, bottom=0, label=model_names[i])

        ax_sat.set_xticks(ind_sat, labels=sat_keys, rotation=60, fontsize=tick_size)
        ax_sat.set_yticks(np.arange(0., 1.05, 0.2), fontsize=tick_size)
        ax_sat.set_ylim(.3, 1.05)
        ax_sat.set_xlabel('Saturation (PIL.ImageEnhance.Enhance parameter)', fontsize=label_size)
        ax_sat.set_ylabel('Identical (fraction)', fontsize=label_size)


        plt.margins(0.05, 0.2)

        plt.tight_layout()
        plt.subplots_adjust(hspace=.35)
        if out_file is None:
            plt.show()
        else:
            plt.savefig(out_file)

    @classmethod
    def _get_hue_datapoints(cls, files_hue: list):
        hue_datapoints = []

        b_first = True
        for f in files_hue:
            csv = pd.read_csv(f)
            if b_first:
                b_first = False
                keys = list(csv.columns)[1:]
                keys = sorted([int(x) for x in keys])
            nb_rows = csv.shape[0]
            ref = csv["0"]

            # Number of classifications identical to ref, in pc., per hue shift
            _datapoints = []
            # Using 'sorted' shouldn't be necessary, but better be safe than sorry.
            for k in keys:
                target = csv[str(k)]
                equal = 0
                for t in zip(ref, target):
                    if t[0] == t[1]:
                        equal += 1
                _datapoints.append(equal/nb_rows)

            hue_datapoints.append(_datapoints)

        return keys, hue_datapoints

    @classmethod
    def _get_saturation_datapoints(cls, files_saturation: list):
        sat_datapoints = []

        b_first = True
        for f in files_saturation:
            csv = pd.read_csv(f)
            if b_first:
                b_first = False
                keys = list(csv.columns)[1:]
                keys = sorted(keys)
            nb_rows = csv.shape[0]
            ref = csv["1.00"]

            # Number of classifications identical to ref, in pc., per hue shift
            _datapoints = []
            # Using 'sorted' shouldn't be necessary, but better be safe than sorry.
            for k in keys:
                target = csv[str(k)]
                equal = 0
                for t in zip(ref, target):
                    if t[0] == t[1]:
                        equal += 1
                _datapoints.append(equal/nb_rows)

            sat_datapoints.append(_datapoints)

        return keys, sat_datapoints


if __name__ == '__main__':
    # csv_sat = os.path.join(Config.DIR_LOGS, 'densenet161_val_stability_saturation.csv')
    # csv_hue = os.path.join(Config.DIR_LOGS, 'emonet_val_stability_hue.csv')

    # ColorStabilityAnalysis.analyze_saturation(csv_sat, model_name='AlexNet')
    # ColorStabilityAnalysis.analyze_hue(csv_hue, model_name='EmoNet')

    if True:
        ColorStabilityAnalysis.plot_together_hue(
            files_hue=[
                os.path.join(Config.DIR_LOGS, 'alexnet_val_stability_hue.csv'),
                os.path.join(Config.DIR_LOGS, 'alexnet_sin_stability_hue.csv'),
                os.path.join(Config.DIR_LOGS, 'alexnet_places365_stability_hue.csv'),
                # os.path.join(Config.DIR_LOGS, 'alexnet_classifier_tl_hue_val_stability_hue.csv'),
                # os.path.join(Config.DIR_LOGS, 'alexnet_full_tl_hue_val_stability_hue.csv'),
                # os.path.join(Config.DIR_LOGS, 'alexnet_full_tl_hue+sat_val_stability_hue.csv'),
                os.path.join(Config.DIR_LOGS, 'emonet_val_stability_hue.csv'),
                os.path.join(Config.DIR_LOGS, 'vgg16_val_stability_hue.csv'),
                os.path.join(Config.DIR_LOGS, 'vgg16_sin_stability_hue.csv'),
                # os.path.join(Config.DIR_LOGS, 'vgg16_full_tl_hue_val_stability_hue.csv'),
                # os.path.join(Config.DIR_LOGS, 'vgg16_full_tl_hue+sat_val_stability_hue.csv'),
                # os.path.join(Config.DIR_LOGS, 'vgg16_tl_cc_val_stability_hue.csv'),
                os.path.join(Config.DIR_LOGS, 'vgg19_val_stability_hue.csv'),
                os.path.join(Config.DIR_LOGS, 'resnet18_val_stability_hue.csv'),
                os.path.join(Config.DIR_LOGS, 'resnet18_places365_stability_hue.csv'),
                # os.path.join(Config.DIR_LOGS, 'resnet18_full_tl_hue_val_stability_hue.csv'),
                # os.path.join(Config.DIR_LOGS, 'resnet18_full_tl_hue+sat_val_stability_hue.csv'),
                os.path.join(Config.DIR_LOGS, 'resnet50_val_stability_hue.csv'),
                os.path.join(Config.DIR_LOGS, 'resnet50_sin_stability_hue.csv'),
                os.path.join(Config.DIR_LOGS, 'densenet161_val_stability_hue.csv')
            ],
            model_names=[
                'AlexNet',
                'AlexNet SIN',
                'AlexNet P365',
                # 'AlexNet class. +h',
                # 'AlexNet +h',
                # 'AlexNet +hs',
                'EmoNet',
                'VGG16',
                'VGG16 SIN',
                # 'VGG16 +h',
                # 'VGG16 +hs',
                'vgg19',
                'ResNet18',
                'ResNet18 P365',
                # 'ResNet18 +h',
                # 'ResNet18 +hs',
                'ResNet50',
                'ResNet50 SIN',
                'DenseNet161'
            ],
            gaps_after=None
        )

    if False:
        ColorStabilityAnalysis.plot_together_saturation(
            files_saturation=[
                os.path.join(Config.DIR_LOGS, 'alexnet_val_stability_saturation.csv'),
                os.path.join(Config.DIR_LOGS, 'alexnet_sin_stability_saturation.csv'),
                os.path.join(Config.DIR_LOGS, 'alexnet_places365_stability_saturation.csv'),
                # os.path.join(Config.DIR_LOGS, 'alexnet_classifier_tl_hue_val_stability_saturation.csv'),
                # os.path.join(Config.DIR_LOGS, 'alexnet_full_tl_hue_val_stability_saturation.csv'),
                # os.path.join(Config.DIR_LOGS, 'alexnet_full_tl_hue+sat_val_stability_saturation.csv'),
                os.path.join(Config.DIR_LOGS, 'emonet_val_stability_saturation.csv'),
                # os.path.join(Config.DIR_LOGS, 'vgg16_val_stability_saturation.csv'),
                # os.path.join(Config.DIR_LOGS, 'vgg16_sin_stability_saturation.csv'),
                # os.path.join(Config.DIR_LOGS, 'vgg16_full_tl_hue_val_stability_saturation.csv'),
                # os.path.join(Config.DIR_LOGS, 'vgg16_full_tl_hue+sat_val_stability_saturation.csv'),
                # os.path.join(Config.DIR_LOGS, 'vgg16_tl_cc_val_stability_saturation.csv'),
                # os.path.join(Config.DIR_LOGS, 'vgg19_val_stability_saturation.csv'),
                # os.path.join(Config.DIR_LOGS, 'resnet18_val_stability_saturation.csv'),
                # os.path.join(Config.DIR_LOGS, 'resnet18_places365_stability_saturation.csv'),
                # os.path.join(Config.DIR_LOGS, 'resnet18_full_tl_hue_val_stability_saturation.csv'),
                # os.path.join(Config.DIR_LOGS, 'resnet18_full_tl_hue+sat_val_stability_saturation.csv'),
                # os.path.join(Config.DIR_LOGS, 'resnet50_val_stability_saturation.csv'),
                # os.path.join(Config.DIR_LOGS, 'resnet50_sin_stability_saturation.csv'),
                # os.path.join(Config.DIR_LOGS, 'densenet161_val_stability_saturation.csv')
                ],
            model_names=[
                'AlexNet',
                'AlexNet SIN',
                'AlexNet P365',
                # 'AlexNet class. +h',
                # 'AlexNet +h',
                # 'AlexNet +hs',
                'EmoNet',
                # 'VGG16',
                # 'VGG16 SIN'
                # 'VGG16 +h',
                # 'VGG16 +hs',
                # 'vgg19',
                # 'ResNet18',
                # 'ResNet18 P365',
                # 'ResNet18 +h',
                # 'ResNet18 +hs',
                # 'ResNet50',
                # 'ResNet50 SIN',
                # 'DenseNet161'
            ],
            gaps_after=None
        )

    if True:
        ColorStabilityAnalysis.plot_together_hue_and_sat(
            files_hue=[
                os.path.join(Config.DIR_LOGS, 'alexnet_val_stability_hue.csv'),
                os.path.join(Config.DIR_LOGS, 'alexnet_sin_stability_hue.csv'),
                os.path.join(Config.DIR_LOGS, 'alexnet_places365_stability_hue.csv'),
                # os.path.join(Config.DIR_LOGS, 'alexnet_classifier_tl_hue_val_stability_hue.csv'),
                # os.path.join(Config.DIR_LOGS, 'alexnet_full_tl_hue_val_stability_hue.csv'),
                # os.path.join(Config.DIR_LOGS, 'alexnet_full_tl_hue+sat_val_stability_hue.csv'),
                os.path.join(Config.DIR_LOGS, 'emonet_val_stability_hue.csv'),
                os.path.join(Config.DIR_LOGS, 'vgg16_val_stability_hue.csv'),
                os.path.join(Config.DIR_LOGS, 'vgg16_sin_stability_hue.csv'),
                # os.path.join(Config.DIR_LOGS, 'vgg16_full_tl_hue_val_stability_hue.csv'),
                # os.path.join(Config.DIR_LOGS, 'vgg16_full_tl_hue+sat_val_stability_hue.csv'),
                # os.path.join(Config.DIR_LOGS, 'vgg16_tl_cc_val_stability_hue.csv'),
                # os.path.join(Config.DIR_LOGS, 'vgg19_val_stability_hue.csv'),
                # os.path.join(Config.DIR_LOGS, 'resnet18_val_stability_hue.csv'),
                # os.path.join(Config.DIR_LOGS, 'resnet18_places365_stability_hue.csv'),
                # os.path.join(Config.DIR_LOGS, 'resnet18_full_tl_hue_val_stability_hue.csv'),
                # os.path.join(Config.DIR_LOGS, 'resnet18_full_tl_hue+sat_val_stability_hue.csv'),
                # os.path.join(Config.DIR_LOGS, 'resnet50_val_stability_hue.csv'),
                # os.path.join(Config.DIR_LOGS, 'resnet50_sin_stability_hue.csv'),
                # os.path.join(Config.DIR_LOGS, 'densenet161_val_stability_hue.csv')
            ],
            model_names=[
                'AlexNet',
                'AlexNet SIN',
                'AlexNet P365',
                # 'AlexNet class. +h',
                # 'AlexNet +h',
                # 'AlexNet +hs',
                'EmoNet',
                'VGG16',
                'VGG16 SIN'
                # 'VGG16 +h',
                # 'VGG16 +hs',
                # 'vgg19',
                # 'ResNet18',
                # 'ResNet18 P365',
                # 'ResNet18 +h',
                # 'ResNet18 +hs',
                # 'ResNet50',
                # 'ResNet50 SIN',
                # 'DenseNet161'
            ],
            gaps_after={4},
            out_file=os.path.join(Config.HOME_DIR, 'Work', 'Papers', 'ColorPaper2023', 'equal_graph_hue+sat_AlexNet+VGG16.png')
        )
