import os

import torch
import torchvision
from torch.nn import CrossEntropyLoss

from config import Config
from datasets.imagenet_preproc import ImageNetPreProcess
from datasets.imagenet_train_dataset import ImageNetTrainDatasetFactory
from imagenet_training.data_config import DataConfig
from imagenet_training.imagenet_trainer import ImageNetTrainer
from imagenet_training.net_config import NetConfig, StopCriterion


if __name__ == '__main__':
    net_config = NetConfig()
    net_config.device = torch.device('cuda:0')
    net_config.b_allow_parallel = False
    net_config.max_epochs = 250
    net_config.dropout = 0.25
    net_config.hold_for_epochs = 2
    net_config.patience = 6
    net_config.batch_size = 256
    net_config.learning_rate = .00001
    net_config.log_interval = 5
    net_config.burn_in = 0
    net_config.stop_criterion = StopCriterion.TEST

    net_config.pretrained = True
    net_config.network = torchvision.models.alexnet
    net_config.network_params = {'weights': torchvision.models.AlexNet_Weights.DEFAULT, 'progress': True}
    net_config.b_freeze = False
    net_config.b_freeze_classifier = False

    net_config.add_random_hue = False
    net_config.add_random_saturation = True

    net_config.optimizer_class = torch.optim.Adam
    net_config.loss = CrossEntropyLoss
    net_config.loss_params = {}

    net_config.model_name = net_config.network.__name__

    data_config = DataConfig()
    data_config.dataset_factory = ImageNetTrainDatasetFactory
    # !!! The random hue shift is applied during the training loop, so that we can process
    # an entire batch at once. This saves quite some time. !!!
    # data_config.dataset_factory_params['train_preproc'] = ImageNetHueShiftPreProcess(mu=0., sigma=30., device=torch.device('cpu'))
    data_config.dataset_factory_params['train_preproc'] = ImageNetPreProcess(ImageNetPreProcess.RANDOM)
    data_config.dataset_factory_params['max_samples'] = -1

    model_name = os.path.join(Config.DIR_IMGNET, 'Models', 'alexnet_full_hue+sat_tl')

    # Logger params
    data_config.logger_params['logfile'] = None  # model_name + '_training_log.txt'
    data_config.logger_params['b_overwrite'] = False

    net_config.save_every = 1
    trainer = ImageNetTrainer(net_config, data_config, b_set_loss_weights=True, b_default_init=True)
    trainer.train(b_save_model=False, model_name=model_name)
