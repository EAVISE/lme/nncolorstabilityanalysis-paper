"""

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""


class DataConfig:
    def __init__(self):
        # Dataset factory to be used
        self.dataset_factory = None
        self.dataset_factory_params = {
            'max_samples': -1
        }

        # Parameters to be passed along to Logger instance.
        self.logger_params = {}

        self.file_results = None
        self.file_c_mtxs = None
        self.file_l1_weights = None
