"""

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os
import random

import PIL
import torch
import torchvision.transforms

from config import Config
from tools.pil_hue_changer import PILHueShifter
from datasets.imagenet_preproc import ImageNetPreProcess


class RandomHueShift(torch.nn.Module):
    def __init__(self, mu=0., sigma=30, device=torch.device('cuda')):
        super().__init__()
        self.mu = mu
        self.sigma = sigma
        self.device = device

        # Define random number generator
        self.rng = random.SystemRandom()

    def forward(self, img: PIL.Image or torch.Tensor) -> PIL.Image or torch.Tensor:
        """

        :param img:
        :return: same type as input img
        """
        if isinstance(img, torch.Tensor) and img.dim() == 4:
            rnd_hue_shift = [int(self.rng.gauss(mu=self.mu, sigma=self.sigma))%360 for _ in range(img.shape[0])]
        else:
            rnd_hue_shift = int(self.rng.gauss(mu=self.mu, sigma=self.sigma))%360
        shifted_img = PILHueShifter.shift_hue_8bit_tensor(image=img, hue_shift=rnd_hue_shift, device=self.device)
        return shifted_img


if __name__ == '__main__':
    test_img = os.path.join(Config.DIR_IMGNET_TRAIN, 'n03110669', 'n03110669_1005.JPEG')
    test_img2 = os.path.join(Config.DIR_IMGNET_TRAIN, 'n03110669', 'n03110669_455.JPEG')

    shifter = RandomHueShift()
    with open(test_img, 'rb') as f:
        img = PIL.Image.open(f).convert('RGB')

    # test with PIL.Image
    if False:
        shifter(img).show()

    # test with torch.Tensor
    img = torchvision.transforms.ToTensor()(img)
    if False:
        res = torchvision.transforms.ToPILImage()(shifter(img))
        res.show()

    # test with batch of torch.Tensors
    if True:
        norm = ImageNetPreProcess(chain_type=ImageNetPreProcess.NORMALIZE)
        mean = torch.tensor([0.485, 0.456, 0.406]).view((3,1,1)).cuda()
        std = torch.tensor([0.229, 0.224, 0.225]).view((3,1,1)).cuda()
        resize = torchvision.transforms.Resize((256, 256))
        with open(test_img2, 'rb') as f:
            img2 = PIL.Image.open(f).convert('RGB')
        img2 = torchvision.transforms.ToTensor()(img2)

        imgs = torch.stack([resize(img), resize(img2)])
        # imgs = norm(imgs)
        res = shifter(imgs)
        # res *= std
        # res += mean
        for i in range(res.shape[0]):
            temp = torchvision.transforms.ToPILImage()(shifter(res[i]))
            temp.show()
