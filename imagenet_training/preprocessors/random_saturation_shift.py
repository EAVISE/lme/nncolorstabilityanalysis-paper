"""

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os
import random

import PIL
import torch
import torchvision.transforms

from config import Config


class RandomSaturationShift(torch.nn.Module):
    def __init__(self, mu=1., sigma=0.50, device=torch.device('cuda')):
        super().__init__()
        self.mu = mu
        self.sigma = sigma
        self.device = device

        # Define random number generator
        self.rng = random.SystemRandom()

        self.to_pil = torchvision.transforms.ToPILImage()
        self.to_tensor = torchvision.transforms.ToTensor()

    def forward(self, img: PIL.Image or torch.Tensor) -> PIL.Image or torch.Tensor:
        """

        :param img:
        :return: same type as input img
        """
        if isinstance(img, torch.Tensor):
            if img.dim() == 4:
                imgs = []
                for i in range(img.shape[0]):
                    imgs.append(self._process_tensor(img[i]))
                return torch.stack(imgs).to(self.device)
            elif img.dim() == 3:
                return self._process_tensor(img).to(self.device)
            else:
                raise ValueError("Provided tensor has unsupported dimensionality. Should be 3 or 4.")
        else:
            return self._shift_img(img)

    def _process_tensor(self, img: torch.Tensor):
        temp = (self.to_pil(img))
        temp = self._shift_img(temp)
        return self.to_tensor(temp)

    def _shift_img(self, img: PIL.Image):
        converter = PIL.ImageEnhance.Color(img)
        return converter.enhance(self.rng.gauss(mu=self.mu, sigma=self.sigma)%2)

if __name__ == '__main__':
    test_img = os.path.join(Config.DIR_IMGNET_TRAIN, 'n03110669', 'n03110669_1005.JPEG')
    test_img2 = os.path.join(Config.DIR_IMGNET_TRAIN, 'n03110669', 'n03110669_455.JPEG')

    shifter = RandomSaturationShift()
    with open(test_img, 'rb') as f:
        img = PIL.Image.open(f).convert('RGB')
    img.show()

    # test with PIL.Image
    if True:
        shifter(img).show()

    # test with torch.Tensor
    img = torchvision.transforms.ToTensor()(img)
    if False:
        res = torchvision.transforms.ToPILImage()(shifter(img))
        res.show()

    # test with batch of torch.Tensors
    if True:
        resize = torchvision.transforms.Resize((256, 256))
        with open(test_img2, 'rb') as f:
            img2 = PIL.Image.open(f).convert('RGB')
        img2 = torchvision.transforms.ToTensor()(img2)

        imgs = torch.stack([resize(img), resize(img2)])
        res = shifter(imgs)
        for i in range(res.shape[0]):
            temp = torchvision.transforms.ToPILImage()(shifter(res[i]))
            temp.show()
