"""
Configuration file containing all the necessary parameter values relating to a neural net.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import math
from enum import Enum

import torch


class StopCriterion(Enum):
    TRAIN = 'train'
    TEST = 'test'


class NetConfig:
    def __init__(self):
        """

        """
        self.nb_outputs = 1000

        self.device = torch.device('cuda')
        self.b_allow_parallel = True  # Allow running process over multiple GPUs, if possible
        self.b_use_manual_seed = False
        self.manual_seed = 1

        self.start_epoch = 1
        self.max_epochs = 400
        self.save_every = 0
        self.batch_size = 10

        self.model_name = None  # Name you want to give to the model you are training; will default to self.network.__name__
        self.network_params = {}
        self.pretrained = False  # When using torchvision.models models, use pretrained version

        self.b_freeze = False  # Freeze layers of pretrained models
        self.b_freeze_classifier = False  # b_freeze_classifier to pass to TrainerTools.freeze_layers if self.freeze = True
        # self.change_last_layer = False  # Change last layer of network, as used by BaseTrainer and BaseDoubleTrainer
        self.imagenet_freeze_depth = 3  # When freezing classifier for ImageNet

        self.loss = torch.nn.CrossEntropyLoss
        self.loss_params = {}
        self.load_weights = None  # Path to pth file to be loaded, if any; None to train from scratch

        self.dropout = 0.25
        self.learning_rate = 0.01
        self.optimizer = None
        self.optimizer_class = torch.optim.Adam
        self.optimizer_params = {}

        self.lr_scheduler = None
        self.hold_for_epochs = 5
        self.patience = 16
        self.burn_in = 0  # Number of burn in epochs

        self.log_interval = 100

        self.add_random_hue = False
        self.add_random_saturation = False
        self.add_rabdon_blur = False

        self.stop_criterion = StopCriterion.TRAIN

    def set_optimizer(self, model, lr=None, weight_decay=1e-6):
        if lr is None:
            lr = self.learning_rate
        self.optimizer = self.optimizer_class(model.parameters(),
                                              lr=lr,
                                              weight_decay=weight_decay,
                                              **self.optimizer_params)

    def set_lr_scheduler(self, hold_for_epochs=5):
        """
        Initialize the learning rate scheduler.

        :param hold_for_epochs: nb of epochs to keep the learning rate fixed
        :return:
        """
        if self.optimizer is None:
            raise ValueError("Optimizer needs to be initialized first.")
        # self.lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(self.optimizer, 'min',
        #                                                                patience=self.patience)
        self.lr_scheduler = torch.optim.lr_scheduler.\
            LambdaLR(self.optimizer,
                     lr_lambda=[lambda epoch: 1/math.sqrt(epoch//hold_for_epochs + 1)])
        # self.lr_scheduler = torch.optim.lr_scheduler.\
        #     LambdaLR(self.optimizer,
        #              lr_lambda=[lambda epoch: (0.95**epoch)])
