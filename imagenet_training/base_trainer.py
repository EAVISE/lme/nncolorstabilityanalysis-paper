"""
Base trainer class to be used as is or inherited from.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
from collections import Counter

import torch
import torch.nn
import torch.optim.lr_scheduler
import torch.utils.data
from tqdm import tqdm

from config import Config
from imagenet_training.base_trainer_global import BaseTrainerGlobal
from imagenet_training.class_weights import ClassWeights
from tools.trainer_tools import TrainerTools


class BaseTrainer(BaseTrainerGlobal):
    def __init__(self, net_config=None, data_config=None, b_set_loss_weights=True, b_default_init=False):
        """

        :param net_config:
        :param data_config:
        :param b_set_loss_weights: if 'True', will determine the appropriate class weights to be given to the loss function
        :param b_default_init: use default initialization
        """
        super().__init__(net_config=net_config, data_config=data_config,
                         b_set_loss_weights=b_set_loss_weights, b_default_init=b_default_init)
        self.logger.print(f"Executing {__file__.replace(Config.DIR_CODE, '')}")

    def default_init(self):
        # Load data
        if hasattr(self.data_config, 'datasets'):
            train_data, val_data = self.data_config.datasets
        else:
            train_data, val_data = self.data_config.dataset_factory.get_train_val_datasets(**self.data_config.dataset_factory_params)

        self.logger.print(f"train_data size: {len(train_data)}")
        self.logger.print(f"val_data size  : {len(val_data)}")

        self.train_loader = torch.utils.data.DataLoader(train_data, batch_size=self.net_config.batch_size,
                                                        shuffle=True)
        self.val_loader = torch.utils.data.DataLoader(val_data, batch_size=self.net_config.batch_size,
                                                      shuffle=False)

        # Initialize loss
        # Determine weights for loss function
        if self.b_set_loss_weights:
            self.logger.print("Using Classical Weights...")
            target_weights = ClassWeights.classical_weights(data_train=self.train_loader.dataset,
                                                            nb_classes=self.net_config.nb_outputs)
            self.net_config.loss_params = {'weight': target_weights.to(self.device)}
            self.logger.print(f"Target weights: {target_weights}")

        self.net_config.loss = self.net_config.loss(**self.net_config.loss_params)

        # Initialize model
        model = self.net_config.network
        self.model = model(**self.net_config.network_params).to(self.device)

        if self.net_config.b_allow_parallel and torch.cuda.device_count() > 1:
            self.b_parallel = True
            print(f"Using {torch.cuda.device_count()} GPUs...")
            self.model = torch.nn.DataParallel(self.model)
            self.model.to(self.net_config.device)
            self.net_config.set_optimizer(self.model.module)
        else:
            self.net_config.set_optimizer(self.model)

        self.net_config.set_lr_scheduler(hold_for_epochs=self.net_config.hold_for_epochs)
        # Set learning rate to correct starting rate if resuming training
        for _ in range(1, self.net_config.start_epoch):
            self.net_config.lr_scheduler.step()

        self.logger.print(f"Training model of class: {self.model.__class__.__name__}")

        self.logger.print_line(length=90)
        self.logger.print(f"StopCriterion   : {self.net_config.stop_criterion}")
        self.logger.print(f"Dropout         : {self.net_config.dropout}")
        self.logger.print(f"Loss            : {self.net_config.loss.__class__.__name__}")
        self.logger.print(f"Loss params     : {self.net_config.loss_params}")
        self.logger.print(f"Optimizer       : {self.net_config.optimizer_class.__name__}")
        self.logger.print(f"Optimizer params:")
        for k, v in self.net_config.optimizer.defaults.items():
            self.logger.print(f"\t{k:12s}: {v}")

        self.logger.print(f"\nTraining model of class: {self.model.__class__.__name__}")
        self.logger.print_line(length=90)

    def test_step(self, epoch: int, loader: torch.utils.data.DataLoader):
        """
        A single epoch in testing.

        :param epoch: The current epoch.
        :param loader: The dataloader to use for testing.
        """
        self.model.eval()
        outputs = []
        targets = []
        test_loss = 0.

        nb_correct, nb_total = 0, 0
        counts_per_label = Counter()
        preds_per_label = Counter()
        correct_per_label = Counter()
        total_batches = len(loader.dataset)/self.net_config.batch_size
        with torch.no_grad():
            for batch_index, (data, target) in tqdm(enumerate(loader),
                                                    total=len(loader),
                                                    desc=f'Testing epoch {epoch}:', disable=True):
                data, target = data.to(self.device), target.to(self.device)
                with torch.no_grad():
                    output = self.model(data)

                _t = target.cpu().numpy()
                _p = output.argmax(dim=1).cpu().numpy()
                counts_per_label.update(_t)
                preds_per_label.update(_p)
                for t in zip(_t, _p):
                    nb_total += 1
                    targets.append(t[0])
                    outputs.append(t[1])
                    if t[0] == t[1]:
                        nb_correct += 1
                        correct_per_label[t[0]] += 1
                test_loss += self.net_config.loss(output, target).item()

                if (batch_index + 1) % self.net_config.log_interval == 0:
                    self.logger.print(f'\rTEST :: Current loss = {test_loss}'
                                      f' at batch = {batch_index + 1}/{total_batches}, epoch = {epoch}, accuracy: {nb_correct}/{nb_total} [{100*nb_correct/nb_total:5.1f}].',
                                      end=None, flush=True)
            self.logger.print(f'\rTEST :: Current loss = {test_loss}'
                              f' at batch = {batch_index + 1}/{total_batches}, epoch = {epoch}, accuracy: {nb_correct}/{nb_total} [{100*nb_correct/nb_total:5.1f}].', end='\n')

        test_loss /= len(self.val_loader)

        self.logger.print(f'Testing loss at epoch {epoch}: {test_loss}')
        prec_per_label, rec_per_label, f1_per_label, acc, weighted_acc, macro_f1, weighted_f1 =\
            TrainerTools.compute_class_scores(nb_classes=self.net_config.nb_outputs, counts_per_label=counts_per_label,
                                              preds_per_label=preds_per_label, correct_per_label=correct_per_label)
        self.logger.print("Test stats:")
        self.logger.print(f'Accuracy         : {acc:>6.4f} [{nb_correct}/{nb_total}]')
        self.logger.print(f'Weighted accuracy: {weighted_acc:>6.4f}')
        self.logger.print(f'Macro F1         : {macro_f1:>6.4f}')
        self.logger.print(f'Weighted F1      : {weighted_f1:>6.4f}')

        return test_loss, prec_per_label, rec_per_label, f1_per_label, acc, weighted_acc, macro_f1, weighted_f1

    def train_step(self, epoch):
        """
        A single epoch in training.

        :param epoch: The current epoch.
        """
        self.model.train()
        epoch_size = len(self.train_loader)

        outputs, targets = [], []
        total_since_update, corr_since_update = 0, 0
        total_loss = 0.
        nb_batches, nb_correct, nb_total = 0, 0, 0
        counts_per_label = Counter()
        preds_per_label = Counter()
        correct_per_label = Counter()
        total_batches = len(self.train_loader.dataset)//self.net_config.batch_size
        for batch_index, (data, target) in tqdm(enumerate(self.train_loader),
                                                total=epoch_size,
                                                desc=f'Training epoch {epoch}:', disable=True):
            nb_batches += 1
            data = data.to(self.device)
            target = target.to(self.device)
            # self.logger.print(f"target.shape: {target.shape}")
            self.net_config.optimizer.zero_grad()
            output = self.model(data)
            # self.logger.print(f"output.shape: {output.shape}")
            loss = self.net_config.loss(output, target)
            loss.backward()
            self.net_config.optimizer.step()

            total_loss += loss.item()

            if 'b_one_hot_target' in self.data_config.dataset_factory_params and \
                    self.data_config.other_params['b_one_hot_target']:
                _t = target.argmax(dim=1).tolist()
            else:
                _t = target.tolist()
            _p = output.argmax(dim=1).tolist()
            outputs += _p
            targets += _t
            counts_per_label.update(_t)
            preds_per_label.update(_p)
            for t in zip(_t, _p):
                nb_total += 1
                total_since_update += 1
                if t[0] == t[1]:
                    nb_correct += 1
                    corr_since_update += 1
                    correct_per_label[t[0]] += 1

            if (batch_index+1) % self.net_config.log_interval == 0:
                self.logger.print(f'LR = {self.net_config.optimizer.param_groups[0]["lr"]:.5f}, Current avg. loss = {total_loss/(batch_index+1):.3f}'
                      f' at batch = {batch_index+1}/{total_batches}, epoch = {epoch}, accuracy: {nb_correct}/{nb_total} [{100*nb_correct/nb_total:5.1f}] -- '
                                  f'update acc.: {corr_since_update}/{total_since_update} [{100*corr_since_update/total_since_update:5.1f}]')
                total_since_update, corr_since_update = 0, 0

        avg_loss = total_loss / nb_batches
        self.net_config.lr_scheduler.step()

        self.logger.print(f'Training loss at epoch {epoch}: {avg_loss}')
        prec_per_label, rec_per_label, f1_per_label, acc, weighted_acc, macro_f1, weighted_f1 =\
            TrainerTools.compute_class_scores(nb_classes=self.net_config.nb_outputs, counts_per_label=counts_per_label,
                                              preds_per_label=preds_per_label, correct_per_label=correct_per_label)
        self.logger.print("Train stats:")

        self.logger.print(f'Accuracy         : {acc:>6.4f} [{nb_correct}/{nb_total}]')
        self.logger.print(f'Weighted accuracy: {weighted_acc:>6.4f}')
        self.logger.print(f'Macro F1         : {macro_f1:>6.4f}')
        self.logger.print(f'Weighted F1      : {weighted_f1:>6.4f}')
        self.logger.print()

        return avg_loss, prec_per_label, rec_per_label, f1_per_label, acc, weighted_acc, macro_f1, weighted_f1
