"""
Class that holds several different methods for obtaining class weights when one wants to train a network
using imbalanced data.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
from collections import Counter

import torch


class ClassWeights:
    @staticmethod
    def laurent_weights(data_train: torch.utils.data.Dataset, nb_classes: int, weight_smooth=0.):
        """

        :param data_train: the dataset containing the training dataset
        :param nb_classes: total number of classes to be considered
        :param weight_smooth: a smoothing coefficient
        :return:
        """
        target_cnts = Counter(data_train.ys)
        sum_targets = sum(target_cnts.values())
        target_weights = torch.zeros(nb_classes)
        for k, v in target_cnts.items():
            target_weights[k] = ((1 + weight_smooth)*sum_targets-v)/((1 + weight_smooth)*sum_targets)
        target_weights /= max(target_weights).item()

        return target_weights

    @staticmethod
    def classical_weights(data_train: torch.utils.data.Dataset, nb_classes: int) -> torch.Tensor:
        """
        More conventional way of computing class weights.
        See, e.g., https://www.analyticsvidhya.com/blog/2020/10/improve-class-imbalance-class-weights/

        :param data_train: the dataset containing the training dataset
        :param nb_classes: total number of classes to be considered
        :return:
        """
        target_cnts = Counter(data_train.ys)
        sum_targets = sum(target_cnts.values())
        target_weights = torch.zeros(nb_classes)
        for k, v in target_cnts.items():
            target_weights[k] = (sum_targets) / (nb_classes * v)
        target_weights /= max(target_weights).item()

        return target_weights
