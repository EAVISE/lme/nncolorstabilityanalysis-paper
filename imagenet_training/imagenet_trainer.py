"""

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os
from collections import Counter

import torch
import torchvision
from torch.nn import CrossEntropyLoss
from tqdm import tqdm

from config import Config
from datasets.imagenet_preproc import ImageNetPreProcess
from datasets.imagenet_train_dataset import ImageNetTrainDatasetFactory
from imagenet_training.base_trainer import BaseTrainer
from imagenet_training.data_config import DataConfig
from imagenet_training.net_config import NetConfig, StopCriterion
from imagenet_training.preprocessors.random_hue_shift import RandomHueShift
from imagenet_training.preprocessors.random_saturation_shift import RandomSaturationShift
from tools.trainer_tools import TrainerTools


class ImageNetTrainer(BaseTrainer):
    def __init__(self, net_config=None, data_config=None, b_set_loss_weights=True, b_default_init=True):
        """

        :param net_config:
        :param data_config:
        :param b_set_loss_weights: if 'True', will determine the appropriate class weights to be given to the loss function
        :param b_default_init: use default initialization
        """
        super().__init__(net_config=net_config, data_config=data_config,
                         b_set_loss_weights=b_set_loss_weights, b_default_init=b_default_init)

        if net_config.add_random_hue:
            print(">>>Adding random hue shift to train images.")
            self.random_hue_shift = RandomHueShift(device=self.net_config.device)
        elif net_config.add_random_saturation:
            print(">>>Adding random saturation shift to train images.")
            self.random_saturation_shift = RandomSaturationShift(device=self.net_config.device)
        if net_config.load_weights is not None:
            print(f">>>Continuing from previously trained model: {net_config.load_weights}")
            self.model.load_state_dict(torch.load(net_config.load_weights))

        self.normalize = ImageNetPreProcess(chain_type=ImageNetPreProcess.NORMALIZE)

        # Freeze convolution weights for param.requires_grad = False
        if self.net_config.b_freeze:
            TrainerTools.freeze_layers(model=self.model,
                                       logger=self.logger,
                                       b_freeze_classifier=self.net_config.b_freeze_classifier,
                                       depth=self.net_config.imagenet_freeze_depth)

    def train_step(self, epoch):
        """
        A single epoch in training.

        :param epoch: The current epoch.
        """
        self.model.train()
        epoch_size = len(self.train_loader)

        outputs, targets = [], []
        total_since_update, corr_since_update = 0, 0
        total_loss = 0.
        nb_batches, nb_correct, nb_total = 0, 0, 0
        counts_per_label = Counter()
        preds_per_label = Counter()
        correct_per_label = Counter()
        total_batches = len(self.train_loader.dataset)//self.net_config.batch_size
        for batch_index, (data, target) in tqdm(enumerate(self.train_loader),
                                                total=epoch_size,
                                                desc=f'Training epoch {epoch}:', disable=True):
            nb_batches += 1
            data = data.to(self.device)
            if self.net_config.add_random_hue:
                data = self.random_hue_shift(data)
            if self.net_config.add_random_saturation:
                data = self.random_saturation_shift(data)
            data = self.normalize(data)
            # self.logger.print(f"data.shape: {data.shape}")
            target = target.to(self.device)
            # self.logger.print(f"target.shape: {target.shape}")
            self.net_config.optimizer.zero_grad()
            output = self.model(data)
            # self.logger.print(f"output.shape: {output.shape}")
            loss = self.net_config.loss(output, target)
            loss.backward()
            self.net_config.optimizer.step()

            total_loss += loss.item()

            if 'b_one_hot_target' in self.data_config.dataset_factory_params and \
                    self.data_config.other_params['b_one_hot_target']:
                _t = target.argmax(dim=1).tolist()
            else:
                _t = target.tolist()
            _p = output.argmax(dim=1).tolist()
            outputs += _p
            targets += _t
            counts_per_label.update(_t)
            preds_per_label.update(_p)
            for t in zip(_t, _p):
                nb_total += 1
                total_since_update += 1
                if t[0] == t[1]:
                    nb_correct += 1
                    corr_since_update += 1
                    correct_per_label[t[0]] += 1

            if (batch_index+1) % self.net_config.log_interval == 0:
                self.logger.print(f'LR = {self.net_config.optimizer.param_groups[0]["lr"]:.8f}, Current avg. loss = {total_loss/(batch_index+1):.3f}'
                      f' at batch = {batch_index+1}/{total_batches}, epoch = {epoch}, accuracy: {nb_correct}/{nb_total} [{100*nb_correct/nb_total:5.1f}] -- '
                                  f'update acc.: {corr_since_update}/{total_since_update} [{100*corr_since_update/total_since_update:5.1f}]')
                total_since_update, corr_since_update = 0, 0

        avg_loss = total_loss / nb_batches
        self.net_config.lr_scheduler.step()

        self.logger.print(f'Training loss at epoch {epoch}: {avg_loss}')
        prec_per_label, rec_per_label, f1_per_label, acc, weighted_acc, macro_f1, weighted_f1 =\
            TrainerTools.compute_class_scores(nb_classes=self.net_config.nb_outputs, counts_per_label=counts_per_label,
                                              preds_per_label=preds_per_label, correct_per_label=correct_per_label)
        self.logger.print("Train stats:")

        self.logger.print(f'Accuracy         : {acc:>6.4f} [{nb_correct}/{nb_total}]')
        self.logger.print(f'Weighted accuracy: {weighted_acc:>6.4f}')
        self.logger.print(f'Macro F1         : {macro_f1:>6.4f}')
        self.logger.print(f'Weighted F1      : {weighted_f1:>6.4f}')
        self.logger.print()

        return avg_loss, prec_per_label, rec_per_label, f1_per_label, acc, weighted_acc, macro_f1, weighted_f1


if __name__ == '__main__':
    net_config = NetConfig()
    net_config.device = torch.device('cuda:1')
    net_config.b_allow_parallel = False
    net_config.max_epochs = 250
    net_config.dropout = 0.25
    net_config.hold_for_epochs = 2
    net_config.patience = 6
    net_config.batch_size = 64
    net_config.learning_rate = .00001
    net_config.log_interval = 5
    net_config.burn_in = 0
    net_config.stop_criterion = StopCriterion.TEST

    net_config.network = torchvision.models.resnet18
    net_config.network_params = {'weights': torchvision.models.ResNet18_Weights.DEFAULT, 'progress': True}
    net_config.b_freeze = False
    net_config.b_freeze_classifier = False

    net_config.add_random_hue = True
    net_config.add_random_saturation = False

    net_config.optimizer_class = torch.optim.Adam
    net_config.loss = CrossEntropyLoss
    net_config.loss_params = {}

    net_config.model_name = net_config.network.__name__

    gen_file_name = f"ilsvrc_{net_config.model_name}" +\
                    f"_lr={net_config.learning_rate}_loss={net_config.loss.__name__}_sc={net_config.stop_criterion.value}"

    data_config = DataConfig()
    data_config.dataset_factory = ImageNetTrainDatasetFactory
    # !!! The random hue shift is applied during the training loop, so that we can process
    # an entire batch at once. This saves quite some time. !!!
    # data_config.dataset_factory_params['train_preproc'] = ImageNetHueShiftPreProcess(mu=0., sigma=30., device=torch.device('cpu'))
    data_config.dataset_factory_params['train_preproc'] = ImageNetPreProcess(ImageNetPreProcess.RANDOM)
    data_config.dataset_factory_params['max_samples'] = -1

    model_name = os.path.join(Config.DIR_IMGNET, 'Models', 'vgg16_full_tl')

    # Logger params
    data_config.logger_params['logfile'] = model_name + '_training_log.txt'
    data_config.logger_params['b_overwrite'] = False

    net_config.load_weights = os.path.join(Config.DIR_IMGNET, 'Models', 'resnet18_full_tl_epoch19_cuda:1.pth')
    net_config.start_epoch = 20

    net_config.save_every = 1
    trainer = ImageNetTrainer(net_config, data_config, b_set_loss_weights=True, b_default_init=True)
    trainer.train(b_save_model=True, model_name=model_name)
