"""
Dataset containing all ImageNet test images, to be loaded using ImageNet preprocessing.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import torch
from torchvision import transforms

from config import Config
from datasets.imagenet_preproc import ImageNetPreProcess


class ImageNetTestDatasetFactory:
    """
    Create PyTorch dataset containing ImageNet test images that uses ImageNet preprocessing.
    """
    @classmethod
    def get_dataset(cls,
                    base_dir=None,
                    max_samples=-1):
        """

        :param base_dir: parent directory containing images to load
        :param max_samples: maximum number of samples to load
        :return: dataset
        """
        print(f"ImageNetTestDatasetFactory: Reading data...")
        if base_dir is None:
            base_dir = Config.DIR_IMGNET_TEST

        data_files = os.listdir(base_dir)
        if 0 < max_samples < len(data_files):
            data_files = data_files[:max_samples]
        data_files = sorted(data_files)

        transform = transforms.Compose([ImageNetPreProcess()])

        dataset = ImageNetTestDataset(xs=data_files, transform=transform, base_dir=base_dir)

        return dataset


class ImageNetTestDataset(torch.utils.data.Dataset):
    def __init__(self, xs, base_dir=None, transform=None):
        """

        :param xs: array containing the paths to the images to use
        :param base_dir: base dir from which the image paths will be taken
        :param transform: transform to be applied to images
        """
        if base_dir is None:
            self.base_dir = Config.DIR_IMGNET_TEST
        else:
            self.base_dir = base_dir

        self.transform = transform
        self.xs = xs

    def __getitem__(self, item: int) -> torch.Tensor:
        """
        Retrieves an item from dataset.

        :param item: Item index.
        :return: loaded and preprocessed image.
        """
        img_path = self.xs[item]
        full_path = os.path.join(self.base_dir, *img_path.split('/'))
        return self.transform(full_path)

    def __len__(self) -> int:
        """
        Return the length of the dataset.

        :return: The length of the dataset.
        """
        return len(self.xs)


if __name__ == '__main__':
    dataset = ImageNetTestDatasetFactory.get_dataset(max_samples=10)

    for i in range(10):
        print(dataset.__getitem__(i))
