"""
Load and preprocess an image for use with ImageNet network. Besides the original data augmentation,
this class also adds a random hue shift.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import PIL
import torch
from torchvision import transforms

from config import Config
from imagenet_training.preprocessors.random_hue_shift import RandomHueShift


class ImageNetHueShiftPreProcess(torch.nn.Module):
    """
    Load and preprocess image for use with ImageNet network.
    """
    def __init__(self, mu=0., sigma=30., device=torch.device('cuda')):
        """

        :param mu: mu for Gaussian random number generation
        :param sigma: sigma for Gaussian random number generation
        """
        super().__init__()
        print(f"Initializing ImageNetHueShiftPreProcess...")
        self.preprocess = transforms.Compose([
            transforms.Resize(256),
            transforms.RandomCrop(224),
            transforms.RandomHorizontalFlip(p=0.3),
            transforms.ToTensor(),
            RandomHueShift(mu=mu, sigma=sigma, device=device),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ])

    def forward(self, img: str or PIL.Image) -> torch.Tensor:
        """
        Resize image.

        :param img: Input image
        :return: the resized image, as torch.Tensor.
        """
        if isinstance(img, str):
            with open(img, 'rb') as f:
                img = PIL.Image.open(f).convert('RGB')
        img = self.preprocess(img)

        return img


if __name__ == '__main__':
    test_img = os.path.join(Config.DIR_IMGNET_TRAIN, 'n03110669', 'n03110669_1005.JPEG')
    test_img2 = os.path.join(Config.DIR_IMGNET_TRAIN, 'n03110669', 'n03110669_455.JPEG')

    pp = ImageNetHueShiftPreProcess()
    for _ in range(10):
        hihi = pp(test_img)
        img = transforms.ToPILImage()(hihi)
        img.show()
        img.close()
