"""
A script to compare the time it takes to:
-load a batch of 1000 images using ImageNetHueShiftPreProcess
-load a batch of 1000 images using ImageNetPreProcess("random"), followed by sending this batch through RandomHueShift

Spoiler alert: 2nd option is faster.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os
from datetime import datetime

import torch

from config import Config
from datasets.imagenet_hueshift_preproc import ImageNetHueShiftPreProcess
from datasets.imagenet_preproc import ImageNetPreProcess
from imagenet_training.preprocessors.random_hue_shift import RandomHueShift

# Get path of 1000 images
max_files = 750
path_imgs = os.path.join(Config.DIR_IMGNET_TRAIN, 'n03110669')
files_imgs = os.listdir(path_imgs)[:max_files]

# 1. Use ImageNetHueShiftPreProcess
print("Executing scenario 1...")
pp = ImageNetHueShiftPreProcess()
t_start = datetime.now()
batch = []
for f in files_imgs:
    batch.append(pp(os.path.join(path_imgs, f)).cpu())
batch = torch.stack(batch)
t_end = datetime.now()
t_delta = t_end - t_start
print(f"Done! Time taken: {t_delta.total_seconds():.3f}s")

# 1. Use ImageNetPreProcess("random") followed
print("\nExecuting scenario 2...")
pp = ImageNetPreProcess(ImageNetPreProcess.RANDOM)
pp2 = RandomHueShift(device=torch.device('cuda'))
t_start = datetime.now()
batch = []
for f in files_imgs:
    batch.append(pp(os.path.join(path_imgs, f)).cpu())
batch = torch.stack(batch)
batch = pp2(batch)
t_end = datetime.now()
t_delta = t_end - t_start
print(f"Done! Time taken: {t_delta.total_seconds():.2f}s")
