"""
Dataset containing all ImageNet train/validation images, to be loaded using ImageNet preprocessing.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import torch
from torchvision import transforms

from config import Config
from datasets.ilsvrc_labels_map import ILSVRCLabelsMap
from datasets.imagenet_preproc import ImageNetPreProcess
from tools.dataset_tools import DatasetTools


class ImageNetTrainDatasetFactory:
    """
    Create PyTorch datasets, one containing ImageNet train images, the other using ImageNet val images.
    Both use the original ImageNet preprocessing.
    """
    @classmethod
    def get_train_val_datasets(cls,
                               train_dir=None,
                               val_dir=None,
                               val_sol_file=None,
                               train_preproc=None,
                               max_samples=-1):
        """

        :param train_dir: parent directory containing train images to load
        :param val_dir: parent directory containing validation images to load
        :param val_sol_file: CSV file containing target validation labels
        :param train_preproc: preprocessing to be used for train images
        :param max_samples: maximum number of samples to load
        :return: dataset
        """
        print(f"ImageNetTrainDatasetFactory: Reading data...")
        if train_dir is None:
            train_dir = Config.DIR_IMGNET_TRAIN
        if val_dir is None:
            val_dir = Config.DIR_IMGNET_VAL
        if val_sol_file is None:
            val_sol_file = Config.FILE_IMGNET_VAL_SOL

        # Load train files and labels
        train_subdirs = os.listdir(train_dir)
        train_files = []
        train_labels = []
        label_map = ILSVRCLabelsMap()
        for e in train_subdirs:
            full_dir = os.path.join(train_dir, e)
            dir_label = int(label_map.label_to_idx[e])
            if not os.path.isdir(full_dir):
                raise ValueError(f"Training directory seems to contain stuff it isn't supposed to: {full_dir}")
            file_names = [f'{os.path.join(e,x)}' for x in os.listdir(full_dir)]
            train_files += file_names
            train_labels += len(file_names)*[dir_label]

        # Shuffle data and check against max_samples
        train_files, train_labels = DatasetTools.shuffle_data(train_files, train_labels)
        if max_samples > 0:
            train_files = train_files[:max_samples]
            train_labels = train_labels[:max_samples]

        # Load validation files and labels
        val_files = os.listdir(val_dir)
        b_first = True
        val_sol = {}
        with open(val_sol_file, 'r') as fin:
            for line in fin:
                if b_first:
                    b_first = False
                    continue
                line = line.strip()
                parts = line.split(',')
                img_name = parts[0].strip()
                # Some images have more than 1 bbox annotation,
                # but all bboxes have the same label
                img_label = parts[1].split(' ')[0]
                img_label = int(label_map.label_to_idx[img_label])
                val_sol[img_name] = img_label
        val_labels = [val_sol[x[:-5]] for x in val_files]

        # Shuffle data and check against max_samples
        val_files, val_labels = DatasetTools.shuffle_data(val_files, val_labels)
        if max_samples > 0:
            val_files = val_files[:max_samples]
            val_labels = val_labels[:max_samples]

        if train_preproc is None:
            train_transform = transforms.Compose([ImageNetPreProcess(chain_type=ImageNetPreProcess.RANDOM)])
        else:
            train_transform = transforms.Compose([train_preproc])
        test_transform = transforms.Compose([ImageNetPreProcess(chain_type=ImageNetPreProcess.FULL)])

        train_dataset = ImageNetDataset(xs=train_files, ys=train_labels, transform=train_transform, base_dir=train_dir)
        val_dataset = ImageNetDataset(xs=val_files, ys=val_labels, transform=test_transform, base_dir=val_dir)

        return train_dataset, val_dataset


class ImageNetDataset(torch.utils.data.Dataset):
    def __init__(self, xs, ys, base_dir=None, transform=None):
        """

        :param xs: array containing the paths to the images to use
        :param base_dir: base dir from which the image paths will be taken
        :param transform: transform to be applied to images
        """
        if base_dir is None:
            self.base_dir = Config.DIR_IMGNET_TEST
        else:
            self.base_dir = base_dir

        self.transform = transform
        self.target_transform = transforms.Compose([torch.as_tensor])

        self.xs = xs
        self.ys = ys

    def __getitem__(self, item: int) -> (torch.Tensor, int):
        """
        Retrieves an item from dataset.

        :param item: Item index.
        :return: loaded and preprocessed image.
        """
        img_path = self.xs[item]
        full_path = os.path.join(self.base_dir, *img_path.split('/'))
        return self.transform(full_path), self.target_transform(self.ys[item])

    def __len__(self) -> int:
        """
        Return the length of the dataset.

        :return: The length of the dataset.
        """
        return len(self.xs)


if __name__ == '__main__':
    train_data, val_data = ImageNetTrainDatasetFactory.get_train_val_datasets(max_samples=10)

    for i in range(10):
        print(train_data.__getitem__(i))
        print(val_data.__getitem__(i))
