"""
Create dictionary mapping ImageNet ILSVRC class labels to numeric ID.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
from config import Config
import json

class ILSVRCLabelsMap:
    def __init__(self):
        self.idx_to_label = json.load(open(Config.FILE_IMGNET_CLASSES, 'r'))
        self.label_to_idx = {v[0]: k for k, v in self.idx_to_label.items()}


if __name__ == '__main__':
    m = ILSVRCLabelsMap()
    print(m.idx_to_label)
