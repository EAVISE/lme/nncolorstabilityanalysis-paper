"""

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import torch


class NNTools:
    @staticmethod
    def change_last_layer(model, nb_outputs):
        """
        Use this method to change the last layer of a pretrained ImageNet network with a linear layer with nb_outputs nodes.
        Changes are applied in place.

        :param model: the model to alter.
        :param nb_outputs: the number of nodes in the new layer.
        :return:
        """
        # Change the number of output classes, i.e., the last network layer
        if model.__class__.__name__ == 'SqueezeNet':
            num_features = model.classifier[1].in_channels
            features = list(model.classifier)[:-3]  # Remove last 3 layers
            features.extend([torch.nn.Conv2d(num_features, nb_outputs, kernel_size=(1, 1))])  # Add
            features.extend([torch.nn.ReLU(inplace=True)])  # Add
            features.extend([torch.nn.AdaptiveAvgPool2d(output_size=(1, 1))])  # Add
            model.classifier = torch.nn.Sequential(*features)  # Replace the model classifier
        elif model.__class__.__name__ in {'ResNet', 'GoogLeNet', 'ShuffleNetV2'}:
            num_features = model.fc.in_features
            model.fc = torch.nn.Linear(num_features, nb_outputs)  # Resnet18
            # elif net_config.model_name == 'resnet50' or net_config.model_name == 'resnext50_32x4d' \
            #         or net_config.model_name == 'wide_resnet50_2' \
            #         or net_config.model_name == 'wide_resnet50_2':
            #     model.fc = torch.nn.Linear(2048, net_config.nb_outputs).to(device) # ResNet50
        elif model.__class__.__name__ == 'Inception3':
            num_features = model.fc.in_features
            model.fc = torch.nn.Linear(num_features, nb_outputs)
            aux_features = model.AuxLogits.fc.in_features
            model.AuxLogits.fc = torch.nn.Linear(aux_features, nb_outputs)
            # elif net_config.model_name == 'googlenet' or net_config.model_name == 'shufflenet_v2_x1_0':
            #     model.fc = torch.nn.Linear(1024, net_config.nb_outputs).to(device) # GoogLeNet
        elif model.__class__.__name__ in {'MobileNetV2', 'EfficientNet', 'MNASNet'}:
            num_features = model.classifier[1].in_features
            model.classifier[1] = torch.nn.Linear(num_features, nb_outputs)  # MobileNet V2
        elif model.__class__.__name__ == 'MobileNetV3':
            num_features = model.classifier[3].in_features
            model.classifier[3] = torch.nn.Linear(num_features, nb_outputs)  # MobileNet V3 Large
        elif model.__class__.__name__ == 'DenseNet':
            num_features = model.classifier.in_features
            model.classifier = torch.nn.Linear(num_features, nb_outputs)  # DenseNet-121
            # elif net_config.model_name == 'densenet161':
            #     model.classifier = torch.nn.Linear(2208, net_config.nb_outputs).to(device) # DenseNet-161
        elif model.__class__.__name__ in {'AlexNet', 'VGG'}:
            num_features = model.classifier[6].in_features
            model.classifier[6] = torch.nn.Linear(num_features, nb_outputs)
        else:
            msg = f"Don't know what to do with model of class: {model.__class__.__name__}."
            raise ValueError(msg)
