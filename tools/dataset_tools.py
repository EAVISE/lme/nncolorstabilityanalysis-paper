"""

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import random


class DatasetTools:
    @staticmethod
    def shuffle_data(data_files, labels):
        rng = random.SystemRandom()
        temp = list(zip(data_files, labels))
        rng.shuffle(temp)
        return zip(*temp)
