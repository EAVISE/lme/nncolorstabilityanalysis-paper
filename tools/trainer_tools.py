"""
Class containing tools that are potentially useful for (initializing the) training (of) networks.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
from collections import Counter, defaultdict

import numpy as np

from tools.logger import Logger


class Depth:
    """
    Depth options when freezing classifier layer for VGG or AlexNet.
    """
    DEPTH_1 = 1
    DEPTH_2 = 2
    DEPTH_3 = 3


class TrainerTools:
    @staticmethod
    def compute_class_scores(nb_classes: int, counts_per_label: Counter, preds_per_label: Counter, correct_per_label: Counter):
        """

        :param nb_classes:
        :param counts_per_label:
        :param preds_per_label:
        :param correct_per_label:
        :return: (accuracy, weighted accuracy, macro f1, weighted f1)
        """
        nb_elems = sum(counts_per_label.values())
        prec_per_label = {i: correct_per_label[i]/preds_per_label[i] if preds_per_label[i] > 0 else
            (0 if counts_per_label[i] > 0 else 1) for i in range(nb_classes)}
        rec_per_label = {i: correct_per_label[i]/counts_per_label[i] if counts_per_label[i] > 0 else 1 for i in range(nb_classes)}
        f1_per_label = {i: 2*prec_per_label[i]*rec_per_label[i]/(prec_per_label[i]+rec_per_label[i]) if
                        prec_per_label[i]+rec_per_label[i] > 0 else 0 for i in range(nb_classes)}
        acc = sum(correct_per_label.values())/nb_elems
        weighted_acc = sum([prec_per_label[i]*counts_per_label[i] for i in range(nb_classes)])/nb_elems
        macro_f1 = sum(f1_per_label.values())/nb_classes
        weighted_f1 = sum([f1_per_label[i]*counts_per_label[i] for i in range(nb_classes)])/nb_elems

        return prec_per_label, rec_per_label, f1_per_label, acc, weighted_acc, macro_f1, weighted_f1

    @staticmethod
    def compute_reg_scores(preds: [], targets: []):
        """

        :param preds:
        :param targets:
        :return: (avg_per_target, std_per_target)
        """
        per_target = defaultdict(list)
        for (p, t) in zip(preds, targets):
            per_target[t].append(p)
        avg_per_target = {t: np.average(p) for t, p in per_target.items()}
        std_per_target = {t: np.std(p) for t, p in per_target.items()}

        return avg_per_target, std_per_target

    @staticmethod
    def freeze_layers(model, logger: Logger = None, b_freeze_classifier=False, depth: int = Depth.DEPTH_3):
        """
        Use this method to freeze the layers of pretrained networks.

        :param model: the model whose layers need to be frozen.
        :param logger: Logger instance to be used to print messages; if None, revert to standard print().
        :param b_freeze_classifier: by default, for SqueezeNet, AlexNet and VGG, only the Conv-layers are frozen, not the linear layers\
        that constitute the "classifier" following the "features" (i.e., conv) part. If this boolean is set to true, also the\
        classifier layers will be frozen.
        :param depth: for VGG and Alexnet, number of linear layers to freeze in case b_freeze_classifier = True. Min = 1, max = 3.
        :return:
        """
        if logger is None:
            logger = Logger(logfile=None)

        # AlexNet, VGG, SqueezeNet: only disable CNN part, not the classifier
        if model.__module__ in {'torchvision.models.squeezenet', 'torchvision.models.vgg', 'torchvision.models.alexnet'}:
            logger.print('Freeze convolution layers...')
            for param in model.features.parameters():
                param.requires_grad = False
            if b_freeze_classifier:
                logger.print('Also freezing classifier layers...')
                if model.__module__ == 'torchvision.models.vgg':
                    logger.print(f'\tVGG: Freezing {depth} layers...')
                    if depth > 0:
                        TrainerTools._freeze_layer(model.classifier[0])
                    if depth > 1:
                        TrainerTools._freeze_layer(model.classifier[3])
                    if depth > 2:
                        TrainerTools._freeze_layer(model.classifier[6])
                elif model.__module__ == 'torchvision.models.alexnet':
                    logger.print(f'\tAlexNet: Freezing {depth} layers...')
                    if depth > 0:
                        TrainerTools._freeze_layer(model.classifier[1])
                    if depth > 1:
                        TrainerTools._freeze_layer(model.classifier[4])
                    if depth > 2:
                        TrainerTools._freeze_layer(model.classifier[6])
                else:
                    for param in model.classifier.parameters():
                        param.requires_grad = False
        else:
            logger.print('Freeze all layers...')
            for param in model.parameters():
                param.requires_grad = False

    @staticmethod
    def _freeze_layer(layer):
        for param in layer.parameters():
            param.requires_grad = False
