"""
Logger class. Implements a "print" method that prints to the console, as well as writes to a user-specified log file.
If no log file is specified, will just print to console.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os.path
from datetime import datetime

import torch.nn

from tools.print_tools import PrintTools


class Logger:
    """
    Logger class that permits to print output to the console, whilst also writing it away to a log file.
    It also provides methods to write training results and confusion matrices to separate files.

    """
    def __init__(self, logfile=None, b_overwrite=False):
        """

        :param logfile: path to file to write to; default = None, in which case text will only be printed to console,
        i.e., behaviour reverts to the default print() method.
        :param b_overwrite: if 'True' and logfile already exists, will overwrite the existing file; else will create a
        new file with an appended index; e.g., say "logfile"="brol.txt", and "brol.txt" already exists, then "brol_2.txt"
        will be used.
        """
        self.logfile = logfile

        # Store this in a boolean, so as not to always have to preform the check
        self.b_log = (logfile is not None)

        if self.b_log and os.path.isfile(logfile):
            if b_overwrite:
                os.remove(logfile)
            else:
                # Get directory for logfile
                dir_file = os.path.dirname(logfile)
                # Split extension from rest of filename
                filename, ext = os.path.splitext(logfile)
                filename = filename.replace(dir_file, '')[1:]
                # Get index to be used, i.e., check how many index versions already exist
                idx = 2  # Starting index, in case "logfile" already exists, but no other alternatives
                for f in os.listdir(dir_file):
                    if f.startswith(filename + '_'):
                        f_name, f_ext = os.path.splitext(f)
                        # Check f_name equals form "filename_x", with x an integer
                        f_name_diff = f_name.replace(filename + '_', '')
                        if f_name_diff.isdigit():
                            idx += 1
                # Define new "logfile"
                self.logfile = os.path.join(dir_file, f"{filename}_{idx}{ext}")
                # raise FileExistsError(f'Specified log file already exists: {logfile}')

    def print(self, s: str = '', **params):
        print(s, **params)
        self.write(s)

    def print_line(self, length=60, **params):
        """
        Print a line consisting of 'length' '-' characters

        :param length: number of consecutive '-' characters to print
        :return:
        """
        s = '-'*length
        self.print(s, **params)
        self.write(s)

    def print_dbl_line(self, length=60, **params):
        """
        Print a line consisting of 'length' '=' characters

        :param length: number of consecutive '=' characters to print
        :return:
        """
        s = '=' * length
        self.print(s, **params)
        self.write(s)

    def print_star_line(self, length=60, **params):
        """
        Print a line consisting of 'length' '*' characters

        :param length: number of consecutive '*' characters to print
        :return:
        """
        s = '*' * length
        self.print(s, **params)
        self.write(s)

    def write(self, s: str = ''):
        if self.b_log:
            with open(self.logfile, 'a') as fout:
                fout.write(s + '\n')

    # Write results from training
    @staticmethod
    def open_results_csv(csv_file: str, b_overwrite=False):
        """
        Create results CSV file if it does not yet exist, and write header (i.e., column titles).
        If the file already exists, it is left untouched, unless you explicitly want it overwritten.

        :param csv_file: path to file
        :param b_overwrite: overwrite (i.e., replace) file if it already exists.
        :return:
        """
        if csv_file is not None:
            if b_overwrite or (not os.path.exists(csv_file)):
                file_dir = os.path.dirname(csv_file)
                if not os.path.isdir(file_dir):
                    os.makedirs(file_dir)
                with open(csv_file, 'w') as fout:
                    fout.write(f"datetime,model,loss,optimizer,lr,best_epoch,"
                               f"#train_samples,train_loss,train_acc,train_weighted_acc,train_macro_f1,train_weighted_f1,"
                               f"#test_samples,test_loss,test_acc,test_weighted_acc,test_macro_f1,test_weighted_f1,output_file\n")

    @staticmethod
    def write_result_to_csv(best_stats: dict, csv_file: str):
        """
        Append provided result to provided CSV file.

        :param best_stats: dictionary containing the result to be appended to the csv_file
        :param csv_file: path to file
        :return:
        """
        if csv_file is not None:
            with open(csv_file, 'a') as fout:
                fout.write(f"{best_stats['datetime']},{best_stats['model_name']},{best_stats['loss']},{best_stats['optimizer']},"
                           f"{best_stats['lr']},{best_stats['epoch']},"
                           f"{best_stats['#train_samples']},{best_stats['train_avg']:},"
                           f"{best_stats['train_acc']:.5f},{best_stats['train_weighted_acc']:.5f},"
                           f"{best_stats['train_macro_f1']:.5f},{best_stats['train_weighted_f1']:.5f},"
                           f"{best_stats['#test_samples']},{best_stats['test_avg']},"
                           f"{best_stats['test_acc']:.5f},{best_stats['test_weighted_acc']:.5f},"
                           f"{best_stats['test_macro_f1']:.5f},{best_stats['test_weighted_f1']:.5f},"
                           f"{best_stats['output_file']}\n")

    # Write confusion matrices and metrics per class table
    @staticmethod
    def open_mtx_file(mtx_file: str, b_overwrite=False):
        """
        Create confusion matrix text file if it does not yet exist.
        If the file already exists, it is left untouched, unless you explicitely want it overwritten.

        :param mtx_file: path to file
        :param b_overwrite: overwrite (i.e., replace) file if it already exists
        :return:
        """
        if mtx_file is not None:
            if b_overwrite or (not os.path.exists(mtx_file)):
                with open(mtx_file, 'w') as fout:
                    fout.write("Confusion matrices pertaining to best epochs per model.\n")

    @staticmethod
    def get_nb_files(out_dir, gen_name):
        # Get index to be used, i.e., check how many index versions already exist
        idx = 1  # Starting index, in case "logfile" already exists, but no other alternatives
        for f in sorted(os.listdir(out_dir)):
            if f == f"{gen_name}.txt":
                idx += 1
            elif f.startswith(gen_name + '_'):
                f_name, f_ext = os.path.splitext(f)
                # Check f_name equals form "filename_x", with x an integer
                f_name_diff = f_name.replace(gen_name + '_', '')
                if f_name_diff.isdigit():
                    idx += 1
        return idx
