"""
Script to change the hue of an image using PIL, or directly on tensor representations of these images.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
from copy import deepcopy

import PIL.ImageQt
import torch
import torchvision.transforms
from PIL import Image
import numpy as np

from pytorch_rgb2hsv.color_torch import rgb2hsv_torch, hsv2rgb_torch


class PILHueShifter:
    @staticmethod
    def shift_hue_8bit(image: PIL.Image.Image, hue_shift: int or list):
        """
        Shift the hue value of an image by hue_shift amount, where hue_shift is an angle between -360 and +360.

        :param image: the image to be processed.
        :param hue_shift: the hue shift, in degrees, to be applied; hue_shift = 0 will return a copy of the image.\
        If a list is provided, a list of images will be returned corresponding one-on-one to each hue shift in the list.
        :return: shifted PIL.Image object, or list of shifted PIL.Image objects if hue_shift is a list.
        """
        b_int = True
        if isinstance(hue_shift, int):
            if hue_shift == 0:
                return deepcopy(image)
            elif hue_shift < -360 or hue_shift > 360:
                raise ValueError(f"Hue shift should be within range [-360, 360], got {hue_shift}.")

            hue_shift = int(255 * hue_shift/360) % 255
        elif isinstance(hue_shift, list):
            b_int = False
            hue_shift = [int(255 * x/360) % 255 for x in hue_shift]
        else:
            raise TypeError(f"Parameter hue_shift should be of type int or list, got {hue_shift.__class__} instead.")

        img_hsv = image.convert('HSV')
        np_img = np.asarray(img_hsv)

        # Since we are working with unsigned integers, there is no need to mod the output of the sum operation.
        if b_int:
            np_img[:, :, 0] += hue_shift
            res = Image.fromarray(np_img, 'HSV').convert('RGB')
        else:
            res = []
            # Copy image required number of times
            # np_img = np.repeat(np_img[None, :], len(hue_shift), axis=0)
            # hue_shift = np.asarray(hue_shift, dtype=np.uint8)
            # np_img[:, :, :, 0] += hue_shift[:, None, None]
            # for i in range(np_img.shape[0]):
            #     new_img = Image.fromarray(np_img[i], 'HSV').convert('RGB')
            #     res.append(new_img)

            # Old code; i expected the new code above to be faster, but this does not appear to be the case.
            # Mainly, i suspect, because the conversion from HSV to RGB still needs to be done on a per-image basis.
            for h in hue_shift:
                new_img = np_img.copy()
                new_img[:, :, 0] += h
                new_img = Image.fromarray(new_img, 'HSV').convert('RGB')
                res.append(new_img)

        return res

    @staticmethod
    def shift_hue_8bit_tensor(image: PIL.Image.Image or torch.Tensor, hue_shift: int or list, device=torch.device('cuda')):
        """
        Shift the hue value of an image by hue_shift amount, where hue_shift is an angle between -360 and +360.\
        Performs the transformation using PyTorch instead of Numpy, which is used by shift_hue_8bit(). This allows\
        to vectorize the transformation.

        If a Tensor is provided, it is expected to have dimensions (3, H, W) or (B, 3, H, W).\
        In the first case, if hue_shift is a list, the resulting output Tensor will have shape (len(hue_shift, H, W),
        if it is an int the output Tensor will have the same shape as the input Tensor.\
        In the second case, it is expected that len(hue_shift) == B.

        :param image: the image to be processed.
        :param hue_shift: the hue shift, in degrees, to be applied; hue_shift = 0 will return a copy of the image.\
        If a list is provided, a list of images will be returned corresponding one-on-one to each hue shift in the list.
        :param device: CPU or Cuda; what device will the computations be performed on?
        :return: shifted PIL.Image/pytorch.Tensor object, or list of shifted objects if hue_shift is a list.
        """
        b_int = True
        if isinstance(hue_shift, int):
            if hue_shift == 0:
                return deepcopy(image)
            elif hue_shift < -360 or hue_shift > 360:
                raise ValueError(f"Hue shift should be within range [-360, 360], got {hue_shift}.")

            hue_shift = hue_shift/360.
        elif isinstance(hue_shift, list):
            b_int = False
            hue_shift = [x/360. for x in hue_shift]
        else:
            raise TypeError(f"Parameter hue_shift should be of type int or list, got {hue_shift.__class__} instead.")

        b_PIL = isinstance(image, PIL.Image.Image)
        b_batch = False
        if b_PIL:
            # ToTensor flips the x and y axes compared to the original image, which is why we permute
            image = torchvision.transforms.ToTensor()(image).permute((0, 2, 1))
        else:
            if not isinstance(image, torch.Tensor):
                raise TypeError(f"Provided image object is of invalid type {image.__class__}, expected PIL.Image or torch.Tensor.")
            elif image.dim() == 4:
                b_batch = True
            elif image.dim() != 3:
                raise ValueError(f"Provided image tensor has invalid shape. image.dim() should be 3 (single image) or 4 (batch).")

        if not b_batch:
            image = image.unsqueeze(0)

        res = None
        if b_int:
            # t_img = PILHueShifter._img_to_tensor(image).cuda()
            # Since we are working with unsigned integers, there is no need to mod the output of the sum operation.
            t_img = image.to(device)
            t_img = rgb2hsv_torch(t_img)
            t_img[:, 0, :, :] += hue_shift
            t_img[:, 0, :, :] = torch.where(t_img[:, 0, :, :] > 1., t_img[:, 0, :, :] % 1., t_img[:, 0, :, :])
            t_img = hsv2rgb_torch(t_img)

            # res = PILHueShifter._tensor_to_img(t_img.cpu())
            if not b_batch:
                res = t_img.squeeze()
            if b_PIL:
                res = torchvision.transforms.ToPILImage()(res.permute(0,2,1))
        else:
            # Check tensor batch has same size as hue_shift list, if applicable
            if b_batch and not b_int:
                if image.shape[0] != len(hue_shift):
                    raise ValueError("In case a tensor batch and hue_shift list is provided, both should be of equal dimension.\n"
                                     f"Got batch dim {image.shape[0]} and list dim {len(hue_shift)} instead.")

            # t_img = PILHueShifter._img_to_tensor(image).cuda()
            t_img = image.to(device)
            t_img = rgb2hsv_torch(t_img)
            # Copy image required number of times
            if not b_batch:
                t_img = torch.repeat_interleave(t_img, len(hue_shift), 0)

            t_hue_shift = torch.tensor(hue_shift).to(device)
            t_img[:, 0, :, :] += t_hue_shift[:, None, None]
            t_img[:, 0, :, :] = torch.where(t_img[:, 0, :, :] > 1., t_img[:, 0, :, :] % 1., t_img[:, 0, :, :])
            # Back to RGB
            t_img = hsv2rgb_torch(t_img)
            # Back to PIL.Image
            if b_PIL:
                res = []
                for i in range(t_img.shape[0]):
                    # res.append(PILHueShifter._tensor_to_img(t_img[i].cpu()))
                    _res = t_img[i].squeeze()
                    if b_PIL:
                        _res = torchvision.transforms.ToPILImage()(_res.permute(0, 2, 1))
                    res.append(_res)
            else:
                res = t_img

        return res

    # ############################################################
    # HERE BE DRAGONS
    # ############################################################
    @staticmethod
    def _img_to_tensor(img):
        """
        Convert a PIL.Image to a PyTorch tensor.

        :param img:
        :return:
        """
        # This is somewhat convoluted, but actually the most practical i could find
        t_img = torch.from_numpy(np.asarray(img)).to(torch.float)
        # Standardize
        t_img /= 255.
        # Permute
        t_img = torch.permute(t_img, (2, 1, 0))
        # Add batch dimension
        t_img = t_img.unsqueeze(0)

        return t_img

    @staticmethod
    def _tensor_to_img(t: torch.Tensor):
        t = deepcopy(t)
        # Squeeze, if necessary; if not, this statement will have no effect
        t = t.squeeze(0)
        # Permute
        t = torch.permute(t, (2, 1, 0))
        # Rescale back to 0-255
        t *= 255.
        # Cast to uint
        t = t.to(torch.uint8)
        # Back to numpy
        n = t.numpy()

        img = PIL.Image.fromarray(n, 'RGB')

        return img
